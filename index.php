<?php

$yii=dirname(__FILE__).'/framework/yii.php';

if (substr($_SERVER['HTTP_HOST'], -10) == 'kyberia.sk') {
    $config=dirname(__FILE__).'/protected/config/production.php';
}
else {
    defined('YII_DEBUG') or define('YII_DEBUG',true);
    // specify how many levels of call stack should be shown in each log message
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

    $config=dirname(__FILE__).'/protected/config/dev.php';
}

require_once($yii);
//!TODO zbavit sa brm-extensions
require_once(dirname(__FILE__).'/protected/brm-extensions.php');
Yii::createWebApplication($config)->run();
