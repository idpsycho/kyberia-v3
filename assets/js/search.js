function hideSearch() { $('header .search-window').fadeOut(); }
function updateSearch() {
	var q = $('header input[name=search]').val();
	var w = $('header .search-window');
	w.find('q').each(function() {
		var t = $(this);

		if (!t.data('orig')) t.data('orig', t.text());
		var val = q ? q : t.data('orig');
		t.text( val );

		var opt = t.parents('a.opt:eq(0)');
		var filter = opt.data('filter');
		var search = opt.data('search');
		if (filter)
			opt.attr('href', window.location.pathname+'?'+filter+'='+val)
		if (search)
			opt.attr('href', '/search?'+search+'='+val)
	});
	var opts = $('.search-window a.opt');
	if (!opts.filter('.active').length)
		opts.eq(0).addClass('active');

	w.fadeIn();
}
function moveSearch(dir) {
	var rows = $('.search-window .opt');
	var a = rows.filter('.active').removeClass('active');
	if (dir == 'up') {
		a = a.prevAll('.opt:eq(0)');
		if (!a.length) a = rows.eq(-1)
	}
	if (dir == 'down') {
		a = a.nextAll('.opt:eq(0)');
		if (!a.length) a = rows.eq(0);
	}
	a.addClass('active');
}
$(function() {
	$('input[name=search]').blur(hideSearch);
	$('input[name=search]').focus(updateSearch);
	$('input[name=search]').keyup(updateSearch);
	$('input[name=search]').keydown(function(e) {
		if (e.which != 13) return;
		window.location = $('.search-window .opt.active').attr('href');
		//$('.empty').text('yep');
	});
	$('.search-window .opt').mouseenter(function() {
		$(this).addClass('active').siblings().removeClass('active');
	});

	$('input[name=search]').keydown(function(e) {
		if (e.which == 38) return moveSearch('up');
		else if (e.which == 40) return moveSearch('down');
		else updateSearch();
	});
});
