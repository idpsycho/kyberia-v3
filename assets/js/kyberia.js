
function mysqlToJsDate(s)
{
	if (!s || s=='0000-00-00 00:00:00') return;

	var a=s.split(" ");
	if (a.length != 2) return;

	var d=a[0].split("-");
	var t=a[1].split(":");
	if (d.length!=3 || t.length!=3) return;

	return new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);
}
function prettyDate(s)
{
	var date = mysqlToJsDate(s);
	if (!date) return s;

	var diff = ((new Date()).getTime() - date.getTime()) / 1000;
	var days = Math.floor(diff / 86400);

	if (isNaN(days) || days < 0) return s;

	if (diff < 60)				return 'a moment ago';
	if (diff < 2*60)			return '1 minute ago';
	if (diff < 60*60)			return Math.floor( diff / 60 ) + ' minutes ago';
	if (diff < 2*60*60)			return '1 hour ago';
	if (diff < 24*60*60)		return Math.floor( diff / 3600 ) + ' hours ago';
	if (diff < 2*24*60*60)		return 'yesterday';

	if (days < 7)				return days + ' days ago';
	if (days < 31)				return Math.floor(days/7) + ' weeks ago';
	if (days < 365)				return Math.floor(days/30) + ' months ago';
	if (days < 20*365)			return Math.floor(days/365) + ' years ago';
	return s;
}

/////////////////////////////////////////////////////////////




function onResize()
{
	$('.js-min-height-fill-body').each(function() {
		var e = $(this);
		var minh = $(window).height() - e.offset().top;
		e.css( 'min-height', minh+'px' );
	});
}

$(function() {
	$(window).resize(onResize);

	$(document).on('keydown', 'textarea', function(e) {
		if (e.which == 13 && e.ctrlKey)
			$(this).parents('form:eq(0)').submit();
	});

	$(document).on('keyup focus', 'textarea', function(e) {
		var ta = $(this);
		var sh = ta[0].scrollHeight;
		var h = ta.height();
		if (sh > h+5)
			ta.not(':animated').animate({height: parseInt(sh+100)+'px'}, 'slow');
	});
	onResize();

	setTimeout(checkMailEveryMinute, 60*1000);

	$('.date').each(function() {
		var s = $(this).text();
		var ago = prettyDate( s );
		$(this).text( ago ).attr('title', s);
	});

	/////////
	// DEBUG STUFF
	$('br.render-keypoint').each(function(i, e) {
		var title = $(e).data('title');
		var ticks = $(e).data('ticks');
		if (!title || !title.length)
			return;

		var item = $(e).prevAll(':not(br,hr)').eq(0);
		if (!item.length) item = $(e).parent();
		item.css({'outline': ticks+'px solid red'});
		item.attr('title', title);
	});
	/////////

});

function checkMailEveryMinute()
{
	$.getJSON('/ajax-checkMail', function(json) {
		if (json.unread)
		{
			$('#ajax-mail-notification').addClass('unread');
			$('#ajax-mail-notification .num-unread').text( json.unread );
			document.title = '('+json.unread+') '+document.title.replace(/^\([0-9]+\)/, '');
		}
		else
		{
			$('#ajax-mail-notification').removeClass('unread');
			$('#ajax-mail-notification .num-unread').text('');
			$('#ajax-mail-notification .unread-senders').text('');
			document.title = document.title.replace(/^\([0-9]+\)/, '');
		}
	}, function() {	// error
		$('#ajax-mail-notification .num-unread').text('?');
	});

	setTimeout(checkMailEveryMinute, 60*1000);
}
