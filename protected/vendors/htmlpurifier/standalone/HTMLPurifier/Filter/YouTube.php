<?php
class HTMLPurifier_Filter_YouTube extends HTMLPurifier_Filter
{
    public $name = 'YouTube';

    public function preFilter($html, $config, $context) {
        $pre_replace = array (
            '#<object[^>]+>.+?http://bandcamp.com/EmbeddedPlayer.swf/((track|album)\=[0-9]+\/size\=grande/bgcol\=[A-Za-z0-9]+\/linkcol\=[A-Za-z0-9]+\/).+?</object>#s' => '<span class="bandcamp_grande-embed">\1</span>', // bandcamp grande
            '#<object[^>]+>.+?http://bandcamp.com/EmbeddedPlayer.swf/((track|album)\=[0-9]+\/size\=short/bgcol\=[A-Za-z0-9]+\/linkcol\=[A-Za-z0-9]+\/).+?</object>#s' => '<span class="bandcamp_short-embed">\1</span>', // bandcamp short
            '#<object[^>]+>.+?http://bandcamp.com/EmbeddedPlayer.swf/((track|album)\=[0-9]+\/size\=tall/bgcol\=[A-Za-z0-9]+\/linkcol\=[A-Za-z0-9]+\/).+?</object>#s' => '<span class="bandcamp_tall-embed">\1</span>', // bandcamp tall
            '#<object[^>]+>.+?http://bandcamp.com/EmbeddedPlayer.swf/((track|album)\=[0-9]+\/size\=venti/bgcol\=[A-Za-z0-9]+\/linkcol\=[A-Za-z0-9]+\/).+?</object>#s' => '<span class="bandcamp_venti-embed">\1</span>', // bandcamp venti
	    // ^^^^ daj to to jedneho kontajnera pls, vyzera to jak keby si prvy krat kodil :)
            '#<iframe[^>]+http://maps.cloudmade.com/iframe\?(lat=[0-9\.]+&lng=[0-9\.]+&zoom=[0-9]+&styleId=[0-9]+)[^>]*></iframe>#s' => '<span class="cloudmade-embed">\1</span>', // cloudmade
            '#<object[^>]+>.+?http://www.dailymotion.com/swf/video/([A-Za-z0-9\-_]+).+?</object>#s' => '<span class="dailymotion-embed">\1</span>', // dailymotion
            '#<iframe[^>]+http://www.dailymotion.com/embed/video/([A-Za-z0-9\-_]+)[^>]*></iframe>#s' => '<span class="dailymotion-iframe-embed">\1</span>', // dailymotion-iframe
            '#<embed[^>]+http://www.demoscene.tv/mediaplayer.swf\?id=([_A-Za-z0-9]+)[^>]*>#s' => '<span class="demoscene-embed">\1</span>', //demoscene
	    '#<iframe[^>]+(?:https?:)?//www\.facebook\.com/video/embed\?video_id\=([0-9]+)" width="([0-9]+)" height="([0-9]+)"[^>]+></iframe>#s' => '<span class="facebook-video-embed">\1|\2|\3</span>',
            '#<object[^>]+>.+?http://www.flickr.com/apps/video/stewart.swf(?:\?v=[0-9]+)*.+?photo_id=([0-9]+).+?</object>#s' => '<span class="flickr-embed">\1</span>', // flickr
            '#<iframe[^>]+http://embedded.freemap.sk/\?(lon=[0-9\.]+&amp;lat=[0-9\.]+&amp;zoom=[0-9]+&amp;marker=[0-9]+)[^>]*></iframe>#s' => '<span class="freemap-embed">\1</span>', // freemap
            '#<iframe[^>]+http://([_A-Za-z0-9]+.huba.sk/\?[%\._A-Za-z0-9\/\&;:=-]+).+</iframe>#s' => '<span class="huba-embed">\1</span>', // huba.sk
            '#<iframe[^>]+http://www.ipernity.com/share/audio/([A-Za-z0-9\.]+)[^>]*></iframe>#s' => '<span class="ipernity-audio-embed">\1</span>', // ipernity-audio
            '#<object[^>]+>.+?http://www.ipernity.com/mp/([A-Za-z0-9\._]+\.mp3\.swf).+?</object>#s' => '<span class="ipernity-audio-old-embed">\1</span>', // ipernity-audio-old
            '#<iframe[^>]+http://www.ipernity.com/share/video/([A-Za-z0-9\.]+)[^>]*></iframe>#s' => '<span class="ipernity-video-embed">\1</span>', // ipernity-video
            '#<object[^>]+>.+?http://www.ipernity.com/mp/([A-Za-z0-9\._]+\.flv\.swf).+?</object>#s' => '<span class="ipernity-video-old-embed">\1</span>', // ipernity-video-old
            '#<object[^>]+>.+?http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf\?feed=([%\._A-Za-z0-9\-=]+&embed_uuid=[a-z0-9\-]+).+?</object>#s' => '<span class="mixcloud-embed">\1</span>', // mixcloud
            '#<object[^>]+>.+?http://www.mixcloud.com/api/1/cloudcast/([A-Za-z0-9\.\-_]+/[A-Za-z0-9\-_]+\.json&embed_uuid=[a-z0-9\-]+)&embed_type\=widget_standard.+?</object>#s' => '<span class="mixcloud-cloudcast-embed">\1</span>', // mixcloud-cloudcast
            '#<iframe[^>]+//www.mixcloud.com/widget/iframe/\?feed=http%3A%2F%2Fwww.mixcloud.com%2F([A-Za-z0-9\.\-_%]+&embed_uuid=[a-z0-9\-]+).+?</iframe>#s' => '<span class="mixcloud-iframe-embed">\1</span>', // mixcloud-iframe
            '#<iframe[^>]+http://www.openstreetmap.org/export/embed.html\?(bbox=[0-9\.\,]+&layer=[A-Za-z0-9\-_]+)[^>]*></iframe>#s' => '<span class="openstreetmap-embed">\1</span>', // openstreetmap
            '#<iframe[^>]+http://www.sme.sk/vp/([0-9]+)/[^>]*></iframe>#s' => '<span class="sme-embed">\1</span>', // sme
            '#<iframe[^>]+http://www.snotr.com/embed/([0-9]+)[^>]*></iframe>#s' => '<span class="snotr-embed">\1</span>', // snotr
            '#<object[^>]+>.+?(?:https?:)?//player.soundcloud.com/player.swf\?url=([%\._A-Za-z0-9\-]+).+?</object>#s' => '<span class="soundcloud-embed">\1</span>', // soundcloud
            '#<iframe[^>]+(?:https?:)?//w.soundcloud.com/player/\?url=([%\._A-Za-z0-9\-&=;]+)[^>]*></iframe>#s' => '<span class="soundcloud-iframe-embed">\1</span>', // soundcloud-iframe
            '#<iframe[^>]+http://www.ta3.com/embed/([A-Za-z0-9\-]+).html[^>]*>(?:</iframe>)?#s' => '<span class="ta3-embed">\1</span>', // ta3
            '#<object[^>]+>.+?vu=http://video.ted.com/talks/embed/([A-Za-z0-9\-_]+\.flv).+?</object>#s' => '<span class="ted-embed">\1</span>', //ted
            '#<object[^>]+>.+?vu=http://video.ted.com/talk/stream/([A-Za-z0-9\-_\.\/&=:;\+]+).+?</object>#s' => '<span class="ted-stream-embed">\1</span>', //ted-stream
            '#<object[^>]+>.+?http://vimeo.com/moogaloop.swf\?clip_id=([0-9\-_]+).+?</object>#s' => '<span class="vimeo-embed">\1</span>', //vimeo
            '#<iframe[^>]+http://player.vimeo.com/video/([0-9]+).+?</iframe>#s' => '<span class="vimeo-iframe-embed">\1</span>', // vimeo-iframe
            '#<iframe[^>]+(?:https?:)?//vine.co/v/([A-Za-z0-9]+)/card.+?</iframe>#s' => '<span class="vine-embed">\1</span>', // vine
            '#<object[^>]+>.+?(?:https?:)?//www.youtube.com/v/([A-Za-z0-9\-_\?=&;]+).+?</object>#s' => '<span class="youtube-embed">\1</span>', // youtube
            '#<iframe[^>]+(?:https?:)?//www.youtube.com/embed/([A-Za-z0-9\-_\?=&]+).+?</iframe>#s' => '<span class="youtube-iframe-embed">\1</span>', // youtube-iframe
        );
        return preg_replace(array_keys($pre_replace), array_values($pre_replace), $html);
    }

    public function postFilter($html, $config, $context) {
        $post_regex = array (
    	    '#<span class="(bandcamp_grande)-embed">((track|album)\=[0-9]+\/size\=grande/bgcol\=[A-Za-z0-9]+\/linkcol\=[A-Za-z0-9]+\/)</span>#',
    	    '#<span class="(bandcamp_short)-embed">((track|album)\=[0-9]+\/size\=short/bgcol\=[A-Za-z0-9]+\/linkcol\=[A-Za-z0-9]+\/)</span>#',
    	    '#<span class="(bandcamp_tall)-embed">((track|album)\=[0-9]+\/size\=tall/bgcol\=[A-Za-z0-9]+\/linkcol\=[A-Za-z0-9]+\/)</span>#',
    	    '#<span class="(bandcamp_venti)-embed">((track|album)\=[0-9]+\/size\=venti/bgcol\=[A-Za-z0-9]+\/linkcol\=[A-Za-z0-9]+\/)</span>#',
            '#<span class="(cloudmade)-embed">(lat=[0-9\.]+&amp;lng=[0-9\.]+&amp;zoom=[0-9]+&amp;styleId=[0-9]+)</span>#',
            '#<span class="(dailymotion)-embed">([A-Za-z0-9\-_]+)</span>#',
            '#<span class="(dailymotion-iframe)-embed">([A-Za-z0-9\-_]+)</span>#',
            '#<span class="(demoscene)-embed">([_A-Za-z0-9]+)</span>#',
	    '#<span class="(facebook-video)-embed">([0-9]+\|[0-9]+\|[0-9]+)</span>#',
            '#<span class="(flickr)-embed">([0-9]+)</span>#',
            '#<span class="(freemap)-embed">(lon=[0-9\.]+&amp;lat=[0-9\.]+&amp;zoom=[0-9]+&amp;marker=[0-9]+)</span>#',
            '#<span class="(huba)-embed">([_A-Za-z0-9]+.huba.sk/\?[%\._A-Za-z0-9\/\&;:=-]+)</span>#',
            '#<span class="(ipernity-audio)-embed">([A-Za-z0-9\.]+)</span>#',
            '#<span class="(ipernity-audio-old)-embed">([A-Za-z0-9\._]+\.mp3\.swf)</span>#',
            '#<span class="(ipernity-video)-embed">([A-Za-z0-9\.]+)</span>#',
            '#<span class="(ipernity-video-old)-embed">([A-Za-z0-9\._]+\.flv\.swf)</span>#',
            '#<span class="(mixcloud)-embed">([%\._A-Za-z0-9\-=]+&(?:amp;)?embed_uuid=[a-z0-9\-]+)</span>#',
            '#<span class="(mixcloud-cloudcast)-embed">([A-Za-z0-9\.\-_]+/[A-Za-z0-9\-_]+\.json&(?:amp;)?embed_uuid=[a-z0-9\-]+)</span>#',
            '#<span class="(mixcloud-iframe)-embed">([A-Za-z0-9\.\-_%]+&(?:amp;)?embed_uuid=[a-z0-9\-]+)</span>#',
            '#<span class="(openstreetmap)-embed">(bbox=[0-9\.\,]+&amp;layer=[A-Za-z0-9\-_]+)</span>#',
            '#<span class="(sme)-embed">([0-9]+)</span>#',
            '#<span class="(snotr)-embed">([0-9]+)</span>#',
            '#<span class="(soundcloud)-embed">([%\._A-Za-z0-9\-]+)</span>#',
            '#<span class="(soundcloud-iframe)-embed">([%\._A-Za-z0-9\-&=;]+)</span>#',
            '#<span class="(ta3)-embed">([A-Za-z0-9\-]+)</span>#',
            '#<span class="(ted)-embed">([A-Za-z0-9\-_]+\.flv)</span>#',
            '#<span class="(ted-stream)-embed">([A-Za-z0-9\-_\.\/&=:;\+]+)</span>#',
            '#<span class="(vimeo)-embed">([0-9\-_]+)</span>#',
            '#<span class="(vimeo-iframe)-embed">([0-9]+)</span>#',
            '#<span class="(vine)-embed">([A-Za-z0-9]+)</span>#',
            '#<span class="(youtube)-embed">([A-Za-z0-9\-_\?=&;]+)</span>#',
            '#<span class="(youtube-iframe)-embed">([A-Za-z0-9\-_\?=&]+)</span>#',
        );
        return preg_replace_callback($post_regex, array($this, 'postFilterCallback'), $html);
    }

    protected function armorUrl($url) {
        return str_replace('--', '-&#45;', $url);
    }

    protected function postFilterCallback($matches) {
        $url = $this->armorUrl($matches[2]);
        if($matches[1] == 'bandcamp_grande')
            return '<object width="300" height="100" type="application/x-shockwave-flash" '.
                'data="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'">'.
                '<param name="movie" value="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<param name="allowFullScreen" value="true"></param>'.
                '<!--[if IE]>'.
                '<embed src="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="300" height="100" />'.
                '<![endif]-->'.
                '</object>';
        elseif($matches[1] == 'bandcamp_short')
            return '<object width="150" height="270" type="application/x-shockwave-flash" '.
                'data="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'">'.
                '<param name="movie" value="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<param name="allowFullScreen" value="true"></param>'.
                '<!--[if IE]>'.
                '<embed src="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="150" height="270" />'.
                '<![endif]-->'.
                '</object>';
        elseif($matches[1] == 'bandcamp_tall')
            return '<object width="150" height="270" type="application/x-shockwave-flash" '.
                'data="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'">'.
                '<param name="movie" value="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<param name="allowFullScreen" value="true"></param>'.
                '<!--[if IE]>'.
                '<embed src="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="150" height="270" />'.
                '<![endif]-->'.
                '</object>';
        elseif($matches[1] == 'bandcamp_venti')
            return '<object width="400" height="100" type="application/x-shockwave-flash" '.
                'data="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'">'.
                '<param name="movie" value="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<param name="allowFullScreen" value="true"></param>'.
                '<!--[if IE]>'.
                '<embed src="http://bandcamp.com/EmbeddedPlayer.swf/'.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="400" height="100" />'.
                '<![endif]-->'.
                '</object>';
        elseif($matches[1] == 'cloudmade')
            return '<iframe width="460" height="350" scrolling="no" '.
                'style="padding:0px; margin:0px; border: 0px;" '.
                'src="http://maps.cloudmade.com/iframe?'.$url.'"></iframe>';
        elseif($matches[1] == 'dailymotion')
            return '<object width="480" height="365" type="application/x-shockwave-flash" '.
                'data="http://www.dailymotion.com/swf/video/'.$url.'">'.
                '<param name="movie" value="http://www.dailymotion.com/swf/video/'.$url.'"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<param name="allowFullScreen" value="true"></param>'.
                '<!--[if IE]>'.
                '<embed src="http://www.dailymotion.com/swf/video/'.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="480" height="365" />'.
                '<![endif]-->'.
                '</object>';
        elseif($matches[1] == 'dailymotion-iframe')
            return '<iframe frameborder="0" width="480" height="360" '.
                'src="http://www.dailymotion.com/embed/video/'.$url.'"></iframe>';
        elseif($matches[1] == 'demoscene')
            return '<object width="512" height="404" type="application/x-shockwave-flash" '.
                'data="http://www.demoscene.tv/mediaplayer.swf?id='.$url.'">'.
                '<param name="movie" value="http://www.demoscene.tv/mediaplayer.swf?id='.$url.'"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<param name="allowFullScreen" value="true"></param>'.
                '<!--[if IE]>'.
                '<embed src="http://www.demoscene.tv/mediaplayer.swf?id='.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="512" height="404" />'.
                '<![endif]-->'.
                '</object>';
	elseif($matches[1] == 'facebook-video') {
	    $params = explode('|', $url);
	    return '<iframe src="https://www.facebook.com/video/embed?video_id='.$params[0].'" width="'.$params[1].'" height="'.$params[2].'" frameborder="0"></iframe>';
	}
        elseif($matches[1] == 'flickr')
            return '<object type="application/x-shockwave-flash" width="480" height="360" data="http://www.flickr.com/apps/video/stewart.swf">'.
                '<param name="flashvars" value="intl_lang=en-us&photo_id='.$url.'"></param>'.
                '<param name="movie" value="http://www.flickr.com/apps/video/stewart.swf"></param>'.
                '<param name="bgcolor" value="#000000"></param> <param name="allowFullScreen" value="true"></param>'.
                '<!--[if IE]>'.
                '<embed type="application/x-shockwave-flash" src="http://www.flickr.com/apps/video/stewart.swf" bgcolor="#000000" allowfullscreen="true" flashvars="intl_lang=en-us&photo_id='.$url.'" height="360" width="480"></embed>'.
                '<![endif]--></object>';
        elseif($matches[1] == 'freemap')
            return '<iframe width="460" height="350" scrolling="no" '.
                'style="padding:0px; margin:0px; border: 0px;" '.
                'src="http://embedded.freemap.sk/?'.$url.'"></iframe>';
        elseif($matches[1] == 'huba')
            return '<iframe width="480" height="360" scrolling="no" '.
                'style="padding:0px; margin:0px; border: 0px;" '.
                'src="http://'.$url.'"></iframe>';
        elseif($matches[1] == 'ipernity-audio')
            return '<iframe src="http://www.ipernity.com/share/audio/'.$url.'" width="300" height="60" frameborder="0"></iframe>';
        elseif($matches[1] == 'ipernity-audio-old')
            return '<object width="300" height="60" type="application/x-shockwave-flash" '.
                'data="http://www.ipernity.com/mp/'.$url.'">'.
                '<param name="movie" value="http://www.ipernity.com/mp/'.$url.'"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<!--[if IE]>'.
                '<embed src="http://www.ipernity.com/mp/'.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="300" height="60" />'.
                '<![endif]-->'.
                '</object>';
        elseif($matches[1] == 'ipernity-video')
            return '<iframe src="http://www.ipernity.com/share/video/'.$url.'" width="480" height="320" frameborder="0"></iframe>';
        elseif($matches[1] == 'ipernity-video-old')
            return '<object width="480" height="352" type="application/x-shockwave-flash" '.
                'data="http://www.ipernity.com/mp/'.$url.'">'.
                '<param name="movie" value="http://www.ipernity.com/mp/'.$url.'"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<param name="allowFullScreen" value="true"></param>'.
                '<!--[if IE]>'.
                '<embed src="http://www.ipernity.com/mp/'.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="480" height="352" />'.
                '<![endif]-->'.
                '</object>';
        elseif($matches[1] == 'mixcloud')
            return '<object width="480" height="480">'.
                '<param name="movie" value="http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?feed='.$url.'&embed_type=widget_standard"></param>'.
                '<param name="allowFullScreen" value="true"></param>'.
                '<param name="wmode" value="opaque"></param>'.
                '<param name="allowscriptaccess" value="always"></param>'.
                '<embed src="http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?feed='.$url.'&embed_type=widget_standard" type="application/x-shockwave-flash" wmode="opaque" allowscriptaccess="always" allowfullscreen="true" width="480" height="480"></embed>'.
                '</object>';
        elseif($matches[1] == 'mixcloud-cloudcast')
            return '<object width="300" height="300">'.
                '<param name="movie" value="http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?v=106"></param>'.
                '<param name="allowFullScreen" value="true"></param>'.
                '<param name="allowscriptaccess" value="always"></param>'.
                '<param name="flashVars" value="feed=http://www.mixcloud.com/api/1/cloudcast/'.$url.'&embed_type=widget_standard"></param>'.
                '<embed src="http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?v=106" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" flashvars="feed=http://www.mixcloud.com/api/1/cloudcast/'.$url.'&embed_type=widget_standard" width="300" height="300"></embed>'.
                '</object>';
        elseif($matches[1] == 'mixcloud-iframe')
            return '<iframe width="480" height="480" '.
                'src="//www.mixcloud.com/widget/iframe/?feed=http%3A%2F%2Fwww.mixcloud.com%2F'.$url.'&stylecolor=&embed_type=widget_standard" frameborder="0"></iframe>';
        elseif($matches[1] == 'openstreetmap')
            return '<iframe width="460" height="350" scrolling="no" '.
                'style="padding:0px; margin:0px; border: 0px;" '.
                'src="http://www.openstreetmap.org/export/embed.html?'.$url.'"></iframe>';
        elseif($matches[1] == 'sme')
            return '<iframe width="482" height="300" scrolling="no" '.
                'style="padding:0px; margin:0px; border: 0px;" '.
                'src="http://www.sme.sk/vp/'.$url.'/"></iframe>';
        elseif($matches[1] == 'snotr')
            return '<iframe width="400" height="330" scrolling="no" '.
                'style="padding:0px; margin:0px; border: 0px;" '.
                'src="http://www.snotr.com/embed/'.$url.'"></iframe>';
        elseif($matches[1] == 'soundcloud')
            return '<object width="100%" height="81">'.
                '<param name="movie" value="//player.soundcloud.com/player.swf?url='.$url.'"></param>'.
                '<embed src="//player.soundcloud.com/player.swf?url='.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="100%" height="81" />'.
                '</object>';
        elseif($matches[1] == 'soundcloud-iframe')
            return '<iframe width="100%" height="166" scrolling="no" frameborder="no" '.
                'src="//w.soundcloud.com/player/?url='.$url.'"></iframe>';
        elseif($matches[1] == 'ta3')
            return '<iframe src="http://www.ta3.com/embed/'.$url.'.html" width="500" height="320" frameborder="0" scrolling="no"></iframe>';
        elseif($matches[1] == 'ted')
            return '<object width="446" height="326" type="application/x-shockwave-flash" '.
                'data="http://video.ted.com/assets/player/swf/EmbedPlayer.swf">'.
                '<param name="movie" value="http://video.ted.com/assets/player/swf/EmbedPlayer.swf"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<param name="bgColor" value="#ffffff"></param>'.
                '<param name="allowFullScreen" value="true"></param>'.
                '<param name="flashvars" value="vu=http://video.ted.com/talks/embed/'.$url.'&vw=432&vh=240&ap=0"></param>'.
                '<!--[if IE]>'.
                '<embed src="http://video.ted.com/assets/player/swf/EmbedPlayer.swf" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="446" height="326" allowFullScreen="true" '.
                'flashvars="vu=http://video.ted.com/talks/embed/'.$url.'&vw=432&vh=240&ap=0" />'.
                '<![endif]-->'.
                '</object>';
        elseif($matches[1] == 'ted-stream')
            return '<object width="526" height="374">'.
                '<param name="movie" value="http://video.ted.com/assets/player/swf/EmbedPlayer.swf"></param>'.
                '<param name="allowFullScreen" value="true" />'.
                '<param name="allowScriptAccess" value="always"/>'.
                '<param name="wmode" value="transparent"></param>'.
                '<param name="bgColor" value="#ffffff"></param>'.
                '<param name="flashvars" value="vu=http://video.ted.com/talk/stream/'.$url.'" />'.
                '<embed src="http://video.ted.com/assets/player/swf/EmbedPlayer.swf" type="application/x-shockwave-flash" wmode="transparent" '.
                'width="526" height="374" allowFullScreen="true" '.
                'flashvars="vu=http://video.ted.com/talk/stream/'.$url.'"></embed>'.
                '</object>';
        elseif($matches[1] == 'vimeo')
            return '<object width="640" height="390" type="application/x-shockwave-flash" '.
                'data="http://vimeo.com/moogaloop.swf?clip_id='.$url.'">'.
                '<param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id='.$url.'"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<!--[if IE]>'.
                '<embed src="http://vimeo.com/moogaloop.swf?clip_id='.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="640" height="390" />'.
                '<![endif]-->'.
                '</object>';
        elseif($matches[1] == 'vimeo-iframe')
    	    return '<iframe src="http://player.vimeo.com/video/'.$url.'" width="640" height="390" frameborder="0"></iframe>';
        elseif($matches[1] == 'vine')
            return '<iframe src="//vine.co/v/'.$url.'/card" width="435" height="435" frameborder="0"></iframe>';
        elseif($matches[1] == 'youtube')
            return '<object width="640" height="390" type="application/x-shockwave-flash" '.
                'data="//www.youtube.com/v/'.$url.'">'.
                '<param name="movie" value="//www.youtube.com/v/'.$url.'"></param>'.
                '<param name="wmode" value="transparent"></param>'.
                '<!--[if IE]>'.
                '<embed src="//www.youtube.com/v/'.$url.'" '.
                'type="application/x-shockwave-flash" '.
                'wmode="transparent" width="640" height="390" />'.
                '<![endif]-->'.
                '</object>';
        elseif($matches[1] == 'youtube-iframe')
    	    return '<iframe class="youtube-player" type="text/html" width="640" height="390" '.
    		'src="//www.youtube.com/embed/'.$url.'" frameborder="0" allowFullScreen></iframe>';
    }
}

// vim: et sw=4 sts=4
