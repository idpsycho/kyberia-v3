<?php

/**
 * Allows to use Yii beginContent() and endContent()
 *
 * Example:
 *  {begin_content view="activeForm" data=""}
 *      {$content}
 *  {/begin_content}
 *
 * @param array                    $params   parameters
 * @param string                   $content  contents of the block
 * @param Smarty_Internal_Template $template template object
 * @param boolean                  &$repeat  repeat flag
 * @return string
 * @throws CException
 * @author Marek Viger (marek.viger@gmail.com)
 */
function smarty_block_begin_content($params, $content, $template, &$repeat)
{
    /** @var $controller CController */
    $controller = $template->getTemplateVars('this');
    if (!is_object($controller) || !($controller instanceof CController)) {
        throw new CException("ERROR: Can't get controller object from template.");
    }

    if (!isset($params['view'])) {
        throw new CException("ERROR: Missing 'view' parameter.");
    }

    if ($repeat) { //tag opened
        if (!isset($params['data'])) {
            $params['data'] = array();
        }

        $controller->beginContent($params['view'], $params['data']);
    }
    else { //tag closed
        echo $content;

        $controller->endContent();
    }
}
