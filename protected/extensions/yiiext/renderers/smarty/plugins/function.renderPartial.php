<?php
/**
 * Renders a view
 *
 * @see CController::renderPartial().
 *
 * @param array $params
 * @param Smarty_Internal_Template $smarty
 * @return string Widget output
 * @throws CException
 */
function smarty_function_renderPartial($params, Smarty_Internal_Template &$smarty)
{
    // aliases
    $aliases = array(
        'assign' => 'return',
    );

    foreach ($aliases as $alias => $original) {
        if (array_key_exists($alias, $params) && !array_key_exists($original, $params)) {
            $params[$original] = &$params[$alias];
        }
    }

    /** @var $controller CController */
    $controller = $smarty->getTemplateVars('this');
    if (!is_object($controller) || !($controller instanceof CController)) {
        $controller = Yii::app()->controller;

        if (!is_object($controller) || !($controller instanceof CController)) {
            throw new CException("Can't get controller object from template. Error.");
        }
    }

    if (!isset($params['view'])) {
        throw new CException("'view' parameter should be specified.");
    }

    // transfer params to variables with default values
    $view = $params['view'];
    $data = empty($params['data']) ? array() : $params['data'];
    $assign = empty($params['assign']) ? NULL : $params['assign'];
    $processOutput = empty($params['processOutput']) ? FALSE : $params['processOutput'];

    unset($params);

    $output = $controller->renderPartial($view, $data, TRUE, $processOutput);
    if (!empty($assign)) {
        $smarty->assign($assign, $output);
    }
    else {
        return $output;
    }
}