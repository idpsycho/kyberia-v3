{extends file="layouts/node.tpl"}
{block content}
<section class="node-examples">

	<div style="kyberia-bordered">

		<style>
			table.examples { border-collapse: collapse; width: 100%; }
			table.examples>tbody>tr>td { border: 1px solid #6dae42; padding: 5px; }
		</style>

		<table class="examples"><tbody>
			<tr>
				<td>$node|tpl:btnK</td>
				<td>{$node|tpl:btnK}</td>
			</tr>
			<tr>
				<td>$node|tpl:btnFook</td>
				<td>{$node|tpl:btnFook}</td>
			</tr>
			<tr>
				<td>$node|tpl:btnBook</td>
				<td>{$node|tpl:btnBook}</td>
			</tr>
			<tr>
				<td>$user|tpl:formLogin</td>
				<td>{$user|tpl:formLogin}</td>
			</tr>
			<tr>
				<td>$node|tpl:bookedUsers</td>
				<td>{$node|tpl:bookedUsers}</td>
			</tr>
			<tr>
				<td>$node|tpl:kValue</td>
				<td>{$node|tpl:kValue}</td>
			</tr>
			<tr>
				<td>$node|tpl:currentVisitors</td>
				<td>{$node|tpl:currentVisitors}</td>
			</tr>
			<tr>
				<td>$node|tpl:reply</td>
				<td>{$node|tpl:reply}</td>
			</tr>
			<tr>
				<td>$node|tpl:viewNodeshell</td>
				<td>{$node|tpl:viewNodeshell}</td>
			</tr>
			<tr>
				<td>$node|tpl:viewTree</td>
				<td>{$node|tpl:viewTree}</td>
			</tr>
			<tr>
				<td>$node|tpl:ancestors</td>
				<td>{$node|tpl:ancestors}</td>
			</tr>

		</tbody></table>

	</div>

</section>
{/block}