<style>
	.user-sidebar .node-visitors { text-align: left; }
</style>

<div class="user-sidebar">
	{if !$user.user_id}

		Kyberia je komunita bytosti, ktore chcu byt stastne a slobodne,
		s&nbsp;vysokou mierou respektu voci ostatnym, a&nbsp;hladom po novych zazitkoch a informaciach.

	{else}

		<a href="{$user.link}"><img src="{$user.avatar}"></a>

		<h4>friends</h4>
		{$user|tpl:friendsOnline}

		{$user|tpl:booked}

	{/if}
</div>

