<div class="user-booked">

	<h4>bookmarks</h4>

	{foreach from=$u.bookedNodes item=na}
		{assign var=n value=$na.node}
		<div class="bookmark-node">
			:: <a class="node-name" href="{$n.link}">{if $n.name}{$n.name}{else}{$n.id}{/if}</a>
			{assign var="unread" value=$na.unread_children}
			<span class="node-new-children">{if $unread}{$unread} NEW{/if}</span>
			<span class="node-new-descs">{if $na.hasNewDescs}new{/if}</span>
		</div>
	{/foreach}

</div>

