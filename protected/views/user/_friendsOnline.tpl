<div class="friends-online">
	{foreach from=$user.friendsOnline item=friend}
		{$friend->tpl(people, [small=>1])}
	{/foreach}
</div>
