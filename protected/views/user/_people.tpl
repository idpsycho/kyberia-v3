
{if !$small}
	<div class="user-activity-box">
		<h4 class="user-name">{$u.login}</h4>
		{$u->tpl(avatar)}
		<div class="user-activity">
			<a class="user-location" href="{$u.location.link}">{$u.location.name}</a>
			<span class="user-ago">{$u.lastActiveAgo}</span>
		</div>
	</div>
{else}
	<div class="user-activity-line">
		{$u->tpl(avatar)}
		<div class="user-activity">
			<span class="user-ago">{$u.lastActiveAgo}</span>
			<a class="user-location" href="{$u.location.link}">{$u.location.name}</a>
		</div>
	</div>
{/if}
