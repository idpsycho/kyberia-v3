{extends file="layouts/node.tpl"}
{block content}
<section class="given-k">

	{assign var="nodeuser" value=$node.asUser}
	{if !$nodeuser}
		well, nodes dont give k, do they..
	{else}
		{foreach from=$nodeuser.givenK item=na}
			{if !$na.node} {continue} {/if}

			{$na.node->tpl(viewAsSubmission)}
		{/foreach}
	{/if}

</section>
{/block}
