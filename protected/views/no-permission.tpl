{extends "layouts/center.tpl"}
{block content}
<section class="no-permission">

	you don't have permission to view this node
	<br>
	<br>
	<br>owner: {$node.user.login}
	<br>access: {$node.access}

</section>
{/block}