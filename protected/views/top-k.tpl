{extends file="layouts/node.tpl"}
{block content}
<section class="top-k">

	{assign var="nodeuser" value=$node.asUser}
	{if $nodeuser}
		{assign var="nodes" value=$nodeuser.topK}
	{else}
		{assign var="nodes" value=$node.topK}
	{/if}

	{foreach from=$nodes item=n}
		{$n->tpl(viewAsSubmission)}
	{/foreach}

</section>
{/block}
