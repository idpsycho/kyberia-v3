<script src="/assets/js/search.js"></script>

<div class="search-window" style="display: none;">
	<div class="results">

		{if $node.name != 'search'}
			<div class="where">filter <span class="current_node">{$node.node_name}</span></div>
			<a href="#" class="opt" data-filter="search"><i class="icon-search "></i> find <q>text</q></a>
			{if $node.name == 'mail'}
				<a href="#" class="opt" data-filter="user"><i class="icon-user "></i> conversations with <q>user</q></a>
			{else}
				<a href="#" class="opt" data-filter="user"><i class="icon-user "></i> <q>user</q>'s submissions</a>
				{* <a href="#" class="opt" data-filter="k-by"><b class="k">k</b> by <q>user</q></a> *}
			{/if}
			<div class="empty">&nbsp;</div>
		{/if}

		<div class="where">search on kyberia</div>
		<a href="#" class="opt" data-search="search"><i class="icon-search"></i> find <q>text</q> on kyberia</a>
		<a href="#" class="opt" data-search="user"><i class="icon-user"></i> find <q>user</q> on kyberia</a>

	</div>
</div>