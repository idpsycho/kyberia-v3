{extends file="layouts/center.tpl"}
{block content}
<section class="register" style="width: 666px; margin: 10px auto; text-align: center;">

	<p class="kyberia_important">
		Pozor, ziadas o vstup do autonomnej zony!
	</p>
	<p>
		Kyberia je komunita ludi, ktori mozu byt roztruseni po celom svete a predsa vedia, ze su sucastou toho isteho naroda.
		<br><br>
		Nie naroda Cechov alebo Slovakov, ale naroda bytosti, ktore chcu byt stastne a slobodne,
		<br><b>s vysokou mierou respektu voci ostatnym</b>, a hladom po novych zazitkoch a informaciach.
	</p>

	<p style="font-weight: bold; color: #fff">
		Ak si myslis, ze si schopny rozhodnut sam za seba, co je dobre a co zle, a mas zaujem o vstup do nasej zony,<br>
		pokus sa do zeleneho ramceka o co najuprimnejsie splnenie tejto podmienky...
		<br><br>
		Ukaz nieco zo seba, co vies, co dokazes. Neodpovedaj na otazky, ale vyjadri sa svojim sposobom:<br>
		text, grafika, animacia, hudba ...
		<br><br>
		... a snad ti niekedy v buducnosti pride od protektorov syndikatu h-k sprava o tom, ze si bol zaregistrovany.
	</p>

	<br>
	<br>
	<form action="/register" method="POST" style="width: 100%;" autocomplete="off">
		<textarea style="width: 100%; height: 100px;" name="registration">{$smarty.post.registration}</textarea>
		<br>
		<br>
		A este zopar oficialit
		<br>
		<br>

		<label>
			<span>email:</span>
			<input type="text" name="email" value="{$smarty.post.email}">
		</label>
		<br>

		<label>
			<span>login:</span>
			<input type="text" name="login" value="{$smarty.post.login}">
		</label>
		<br>

		<label>
			<span>heslo:</span>
			<input type="password" name="password" value="{$smarty.post.password}">
		</label>
		<br>

		<label>
			<span>heslo znova:</span>
			<input type="password" name="password2" value="{$smarty.post.password2}">
		</label>
		<br>

		<p><i>
			A este jedna lahodka pre prudicov typu cinny organ alebo puritansky rodic.
			<br>V pripade, ze stlacis tlacitko "register", stavas sa automaticky
			<br><b>SPOLUZODPOVEDNYM ZA VSETKY PRISPEVKY</b>,
			<br>ktore boli a budu ulozene v databaze kyberie. Bez vynimky.
		</i></p>
		<br>
		<button>register</button>
	</form>

</section>
{/block}
