{extends file="layouts/node.tpl"}
{block content}
<section class="history">


	<style>
		.history-navigation { text-align: center; margin: 20px 0; }
		.history-navigation div { display: inline-block; padding: 5px 10px; width: 150px; }
	</style>

	{assign var=tiamat value=$node->getTiamat($smarty.get.revision)}

	<div class="history-navigation">
		{assign var=prev value=$tiamat.prev}
		{assign var=next value=$tiamat.next}
		<div style="text-align: right;">
			{if $next}
				<a href="{$next.link}">
					<i class="icon icon-chevron-left"></i>
					<span class="date">{$next.update_performed}</span>
				</a>
			{/if}
		</div>
		|
		<div style="text-align: left;">
			{if $prev}
				<a href="{$prev.link}">
					<span class="date">{$prev.update_performed}</span>
					<i class="icon icon-chevron-right"></i>
				</a>
			{/if}
		</div>
	</div>



	{$node->tpl(viewAsSubmission, ['tiamat'=>$tiamat.asNode])}

	<script>
		$(function() {
			$('.revision').click(function() {
				window.location = $(this).find('a').attr('href');
			});
			$('.revision a').click(function() {
				 event.stopPropagation();
			});
		});
	</script>

	<div class="revisions">
		<h2>History</h2>
			<div class="revision">
				<a href="{$node.link}/source" class="date">source</a> current
			</div>
		{foreach from=$node.tiamats item=rev}
			<div class="revision {if $tiamat.update_performed == $rev.update_performed}active{/if}">
				<a href="{$rev.link}" class="date">{$rev.update_performed}</a>
					replaced by {if $rev.destructor}{$rev.destructor->tpl(link)}
					{else}-{/if}
			</div>
		{/foreach}
	</div>

</section>
{/block}
