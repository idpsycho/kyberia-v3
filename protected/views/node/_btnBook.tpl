{if !$node}MISSING NODE!{/if}

{if $user->hasBooked($node)}
	<form class="node-button unbook" method="POST" action="{$node.link}/unbook">
		<button>unbook</button>
	</form>
{else}
	<form class="node-button book" method="POST" action="{$node.link}/book">
		<button>book</button>
	</form>
{/if}