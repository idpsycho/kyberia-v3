<div class="node-reply">
	<img class="avatar" src="{$user.avatar}">
	<form action="{$node.link}/add" method="POST">

		{if $user->canWrite_slow($node)}
		<div class="row">
			<textarea name="content">{$smarty.post.content}</textarea>
		</div>
		{/if}

		<div class="row">
			{if $user->canWrite($node)}
			<button>{if $addtext}{$addtext}{else}add{/if}</button>

			{assign var="defaultName" value=$node.newChildName}
			&nbsp; name ::
			<input type="text" name="name"
								value="{$smarty.post.name}"
								placeholder="{$defaultName|@addslashes}">

			<label>
				<input type="checkbox" name="no_html"> no html
			</label>
			{/if}

			<div class="hardlink">
				<button>put<span></span></button>
				<input type="text" name="id_put_into" placeholder="put into">
				<script>
				$(function() {
					$('.hardlink [name=id_put_into]').keyup(function(e) {
						var id = $(this).val().match(/(?:^|\/)([0-9]+)/);
						if (id) id = (id[1]+'').substr(0, 8);
						$(this).prev().find('span').text( !id ? '' : ' into '+id );
					});
				});
				</script>
			</div>
		</div>

		<div class="preview">
		</div>
	</form>
</div>
