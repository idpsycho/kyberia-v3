{if $node.asUser and $node.id != $user.id}
	{if $user->hasBlockedMail($node)}
		<form class="node-button unblock-mail" method="POST" action="{$node.link}/unblockMail">
			<button>unblock mail</button>
		</form>
	{else}
		<form class="node-button block-mail" method="POST" action="{$node.link}/blockMail">
			<button>block mail</button>
		</form>
	{/if}
{/if}

