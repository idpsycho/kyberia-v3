
<tr class="node view-as-nodeshell template-{$node.template.name}">

	<td><input type="checkbox"></td>

	<td><img class="avatar" src="{$node.userAvatar}"></td>

	<td><a href="{$node.user.link}" class="user">{$node.user.login}</a></td>

	<td>
		<a href="{$node.link}" class="name">
				{if $node.name}	{$node.name}
				{else}			{$node.id}
				{/if}
		</a>
	</td>

	<td>
		<span class="template">
				{if $node.template.name}	{$node.template.name}
				{else}						{$node.id_template}
				{/if}
		</span>
	</td>

	<td>
		{assign var="ch" value=$node.children_count}
		<span class="child-count count-{$ch}">{if $ch}{$ch}{else}no{/if} children</span>
	</td>

	<td><span class="date">{$node.created}</span></td>

	<td>{$node->tpl(kValue)}</td>

	<td>{$node->tpl(btnEdit)}</td>

</tr>
