<div class="view-flat">

	{$node|tpl:viewAsSubmission}
	{$node|tpl:reply}

	{foreach from=$node.childrenDirect item=n key=i}
		<div class="node-level" style="padding-left: 50px;">
			{$n|tpl:viewAsSubmission}
		</div>
	{/foreach}

</div>
