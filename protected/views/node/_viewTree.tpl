<div class="view-tree">

	{$node|tpl:viewAsSubmission}
	{$node|tpl:reply}

	{$node|tpl:currentVisitors}

	{assign var="ref_level" value=$node.vector|@strlen/8}

	{foreach from=$node.childrenTree item=n key=i}
		{assign var="level" value=$n.vector|@strlen/8 - $ref_level}
		<div class="node-level" style="padding-left: {50+log($level, 1.15)*15}px;">
			{$n|tpl:viewAsSubmission}
		</div>
	{/foreach}

</div>
