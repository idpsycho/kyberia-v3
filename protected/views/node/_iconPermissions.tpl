<a href="{$node.link}/info" class="node-permissions">

	{if $node.node_system_access=='private'}
		<i class="icon-lock" title="private - visible only to accessors and masters"></i>
	{elseif $node.node_system_access=='moderated'}
		<i class="icon-list-alt" title="moderated - only accessors and masters can add new submissions"></i>
	{elseif $node.node_external_access eq 'no'}
		<i class="icon-chevron-up" title="visible to kyberia"></i>
	{else}
		<i class="icon-globe" title="visible to internet"></i>

	{/if}

</a>