<div class="view-nodeshell">

	{$node->tpl(viewAsSubmission)}
	{$node->tpl(reply)}

	{$node->tpl(currentVisitors)}

	<table class="nodeshells">
		<tbody>
			{foreach from=$node.childrenDirect item=n key=i}
				{$n->tpl(viewAsNodeshell)}
			{/foreach}
		</tbody>
	</table>

</div>
