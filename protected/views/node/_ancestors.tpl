
<div class="node-ancestors">
	{assign var=ancestors value=$node.ancestors|@array_reverse}

	in {if $ancestors|@count==0}the black hole{/if}
	{foreach from=$ancestors item=a key=i}
		{if $i>0} <span class="separator">/</span> {/if}
		<a href="{$a.link}" class="{if $a.id_template!=4}big-ancestor{/if}">{$a.name|truncate:25}</a>
	{/foreach}
</div>
