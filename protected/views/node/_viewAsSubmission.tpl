<div class="node view-as-submission">
	{perfcheck}

	<div class="header">

		<img class="avatar" src="{$node.userAvatar}">

		{$node.user->tpl(link)}

		{$node->tpl(link)}

		<div class="controls">
			{$node->tpl(kValue)}
			{$node->tpl(btnK)}

			{$node->tpl(btnEdit)}
		</div>

		<span class="date">{$node.created}</span>

	</div>

	{assign var=hidden value=($node.access == 'private')}
	<div class="content {if $hidden}hidden{/if}">
		{if $hidden}
			private
		{else}
			{assign var=content value=$node.content}

			{if $tiamat}
				{assign var=content value=$tiamat.content}
			{/if}

			{if $source or $node.external_link|@startsWith:'template'}
				{assign var=content value=$content|@htmlspecialchars|@nl2br}
			{elseif $node.nl2br}
				{assign var=content value=$content|@nl2br}
			{/if}

			{if $trim}
				{assign var=content value=$content|@mb_substr:0:$trim|@htmlspecialchars}
			{/if}

			{$content}
		{/if}
	</div>

</div>
{perfcheck}
