<div class="booked-users">
	{assign var=booked value=$node.bookedUsers}

	<span class="node-booked-count">{$booked|@count} booked</span>

	{foreach from=$booked item=na}
		{assign var=unread value=$na.unread_children}
		<a href="{$na.user.link}" title="{$na.user.login}{if $unread}- {$unread} NEW{/if}">
			<img src="{$na.user.avatar}">
		</a>
	{/foreach}
</div>
