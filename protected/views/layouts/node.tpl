{extends file="layouts/2-columns.tpl"}

{block mainbar}

	{assign var="nav_user" value=($node.asUser)}
	{assign var="nav_forum" value=(
		$node.id != 1 and !$nav_user and $node.id != $node.id_template and !$node.external_link
		or $template.id==2 or $template.id==3 or $template.id==4)}
	{assign var="nav_main" value=(!$nav_user and !$nav_forum)}

	<nav class="pointers">

		{if $nav_main}
		<a href="/">						<i class="icon-chevron-up"></i> kyberia</a>
		<a class="filler"></a>
		<a href="/reactions">				<i class="icon-leaf"></i> reactions</a>
		<a href="/submissions">				<i class="icon-comment"></i> submissions</a>
		<a class="filler"></a>
		<a href="/k-new">					<i class="icon-star"></i> k NEW</a>
		<a href="/consciousness">			<i class="icon-fire"></i> CONSciousness</a>
		<a href="/trending">				<i class="icon-globe"></i> trending</a>
		<a class="filler"></a>
		<a href="/last">					<i class="icon-time"></i> last</a>
		<a href="/people">					<i class="icon-user"></i> people</a>
		{/if}

		{if $nav_user}
		<a href="{$node.link}">				<i class="icon-user"></i> profile</a>
		<a class="filler"></a>
		<a href="{$node.link}/submissions">	<i class="icon-comment"></i> submissions</a>
		<a href="{$node.link}/booked">		<i class="icon-chevron-up"></i> booked</a>
		<a href="{$node.link}/movement">	<i class="icon-road"></i> movement</a>
		<a href="{$node.link}/given-k">		<i class="icon-heart"></i> given-k</a>
		<a class="filler"></a>
		<a href="{$node.link}/top-k">		<i class="icon-star"></i> top-k</a>
		<a href="{$node.link}/reactions">	<i class="icon-leaf"></i> reactions</a>
		<a href="{$node.link}/visits">		<i class="icon-eye-open"></i> last visits</a>
		<a href="{$node.link}/visits?order=visits"><i class="icon-eye-open"></i> top visitors</a>
		{/if}

		{if $nav_forum}
		<a href="{$node.link}">				<i class="icon-home"></i> {$node.template.name}</a>
		<a class="filler"></a>
		<a href="{$node.link}/top-k">		<i class="icon-star"></i> top <b>K</b></a>
		<a class="filler"></a>
		<a href="{$node.link}/visits">		<i class="icon-eye-open"></i> last visits</a>
		<a href="{$node.link}/visits?order=visits"><i class="icon-eye-open"></i> top visitors</a>
		<a class="filler"></a>
		<a href="{$node.link}/last">		<i class="icon-time"></i> last</a>
		<a href="{$node.link}/forum">		<i class="icon-align-right"></i> forum</a>
		<a href="{$node.link}/source">		<i class="icon-wrench"></i> source</a>
		{/if}

	</nav>
	{perfcheck}

	{if ($nav_user or $nav_forum) and $node}
	<div class="node-topic">
		<div class="controls">
			{$node->tpl(bookedUsers)}
			{$node->tpl(btnBook)}
			{$node->tpl(btnFook)}
			{if $node.asUser}
				{$node->tpl(btnBlockMail)}
			{/if}
			{$node->tpl(btnEdit)}
		</div>

		{$node->tpl(iconPermissions)}
		{$node->tpl(ancestors)}
	</div>
	{/if}

	{block content}{/block}

{/block}
