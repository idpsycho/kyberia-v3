{extends "layouts/layout.tpl"}
{block "layout-content"}

<div class="layout one-column">

	<div class="column mainbar">
		<div class="inner">
			{block mainbar}{/block}

			{include file="layouts/_footer.tpl"}
		</div>
	</div>

</div>

{/block}

