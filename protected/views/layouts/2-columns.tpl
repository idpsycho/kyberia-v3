{extends "layouts/layout.tpl"}
{block "layout-content"}

<div class="layout two-columns">

	<div class="column sidebar">
		<div class="inner">
			{block sidebar}
				{include "user/_sidebar.tpl"}
			{/block}
		</div>
	</div>

	<div class="column mainbar js-min-height-fill-body">
		<div class="inner">
			{block mainbar}{/block}

			{include file="layouts/_footer.tpl"}
		</div>
	</div>

</div>

{/block}

