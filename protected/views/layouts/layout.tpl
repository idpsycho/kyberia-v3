<!DOCTYPE html>
<html lang="{$Yii->language}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{$this->pageTitle}</title>

    <script src="{$Yii->baseUrl}/assets/js/jquery-1.10.2.min.js"></script>
    <script src="{$Yii->baseUrl}/assets/js/kyberia.js"></script>

    {*<link rel="stylesheet" href="{$Yii->baseUrl}/assets/css/{lessToCss file='bootstrap/bootstrap.less'}">*}
    <link rel="stylesheet" href="{$Yii->baseUrl}/assets/css/{lessToCss file='kyberia/old.less'}">
    <link rel="stylesheet" href="{$Yii->baseUrl}/assets/css/{lessToCss file='kyberia/layout.less'}">
    <link rel="stylesheet" href="{$Yii->baseUrl}/assets/css/{lessToCss file='kyberia.less'}">
    <link rel="stylesheet" href="{$Yii->baseUrl}/assets/css/icons.css">

    <link rel="shortcut icon" href="{$Yii->baseUrl}/favicon.ico" type="image/x-icon">
    <link rel="icon" href="{$Yii->baseUrl}/favicon.ico" type="image/x-icon">

</head>

<body>
	{include file="layouts/_header.tpl"}
    {block "layout-content"}{/block}
</body>
</html>