<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>{$node.node_name|@strip_tags} :: V3</title>
	<script src="/assets/js/jquery-1.10.2.min.js"></script>
	<script src="/assets/js/kyberia.js"></script>
	<link rel="stylesheet" href="/assets/css/kyberia.css">
	<link rel="stylesheet" href="/assets/css/icons.css">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon"></head>
<body>

<script>
	// some basic site information for easier customization, etc..
	var KYBERIA_user_id = '{$user.user_id}';
</script>

