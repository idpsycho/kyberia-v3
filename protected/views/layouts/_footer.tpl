<footer>
	<div class="inner">

		<nav class="pagination">
			<div class="inner">
				<!--<a href="#" class="prev">
					<i class="icon-chevron-up"></i>
				</a>
				<br>-->
				<a href="#" class="next">
					<i class="icon-chevron-down"></i>
				</a>
			</div>
		</nav>

		<!--
		in memoriam ergond [/id/258] 23.7.2002
		in memoriam aqwarel [/id/1105602] 12.8.2007
		in memoriam kovo [/id/2417] 11.1.2008
		in memoriam cert [/id/1155846] 13.4.2008
		in memoriam kubo [/id/2278] 21.12.2008
		in memoriam maXim [/id/1350] 21.12.2008
		in memoriam ___RIP___ [/id/788] 15.04.2009
		in memoriam kedilek [/id/2096751] 13.10.2009
		in memoriam wiruz [/id/1681] 25.12.2009
		in memoriam lsdguru [/id/875] 20.4.2012
		in memoriam x2um [/id/2242] 27.7.2012
		in memoriam lulylu [/id/2319] 11.9.2012
		in memoriam Furby [/id/2052] 13.8.2013
		-->

		&copy; kyberia.sk v3<br>
		{pageGenerationTook}s

	</div>
</footer>
