<header>
	<div class="inner">
	{if $user.user_id}
		<div class="sidebar">
			<a href="/" class="dashboard"><i class="icon-chevron-up black"></i> kyberia</a>
			{assign var="numUnread" value=$user.numUnreadMails}
			<a href="/mail" id="ajax-mail-notification" class="mail {if $numUnread>0}unread{/if}">
				<div class="unread-senders">
					{foreach from=$user.unreadSenders item=u}
						<img src="{$u.avatar}" title="{$u.login}">
					{/foreach}
				</div>
				<i class="icon-envelope black"></i> <span class="num-unread">{$numUnread|or:''}</span> mail
			</a>
		</div>
		{perfcheck}
		<div class="mainbar">
			<label class="search">
				<div class="search-anchor">
					{assign var=search_text
						value=$smarty.get.search|or:$smarty.get.user|or:$smarty.get['k-by']}
					<input name="search" tabindex="1" type="text" placeholder="search" value="{$search_text}">
					{include file="search/_search.tpl"}
				</div>
			</label>
			<div class="controls">
				<a href="/help" class="help">help</a>
				<a href="{$user.link}/edit" class="settings">settings</a>
				<form class="logout" action="/logout" method="POST">
					<button>logout</button>
				</form>
			</div>
		</div>

	{else}
		<div class="login">
			{$user|tpl:formLogin}
		</div>
	{/if}
	{perfcheck}

	</div>
</header>

<div class="fixed-header-placeholder"></div>


{flashMessages}
