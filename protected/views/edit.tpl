{extends file="layouts/2-columns.tpl"}
{block mainbar}
<section class="edit">

{if !$user|canEdit:$node}
	you don't have permissions to edit<br>
	{if $user|canModerate_slow:$node}
		<br>
		you can reparent this node if you have rights to moderate..<br>
		<form action="{$node.link}/reparent" method="POST">
			<input type="text" name="id_parent">
			<button>set_parent</button>
		</form>
	{/if}
{else}

<style>
	form.configure	{ max-width: 600px; margin: 0 auto; text-align: center; }
	form.configure .row { margin: 2px 0; }
	form.configure .row>div { width: 40%; display: inline-block; }
	form.configure .row>div:nth-of-type(1) { text-align: right; padding-right: 10px; }
	form.configure .row>div:nth-of-type(2) { text-align: left; }
	form.configure .actions { border-top: 1px solid #6dae42; background: #000;
						padding: 10px; margin-top: 20px; }
	.user-settings	{ background: #000; padding: 10px 0; margin: 15px 0; }
	form.admin		{ text-align: center; max-width: 600px; margin: 10px auto;
						background: #000; display: block; padding: 10px 0; border: 1px dashed #6dae42; }

	section.edit textarea[name=content] { height: 100px; }
</style>

<a style="float: right; margin-top: 40px;" href="/id/{$node.id}">back</a>

{if $user.isAdmin and $node.id == $user.id}
	<!-- iba adminko -->
	<form action="/admin" method="POST" class="admin">
		<input type="submit" name="action" value="recalcVectors">
		<input type="submit" name="action" value="recalcChildCount">
		<input type="submit" name="action" value="recalcParentUser">
		<a href="/performance">performance</a>
	</form>
{/if}

<form method="POST" class="configure" enctype="multipart/form-data">

	{if $node.id == $user.id}
	<div class="user-settings">
		<div class="row">
			<div>change password</div>
			<div><input type="text" name="password"></div>
		</div>
		<div class="row">
			<div>email</div>
			<div><input type="text" name="email" value="{$user.email|@htmlspecialchars}"></div>
		</div>
	</div>
	{/if}

	<div class="row">
		<textarea name="content">{$node.content|@htmlspecialchars}</textarea>
	</div>
	<div class="row">
		<label>
			<input type="checkbox" name="no_html"> no html
		</label>
		<label>
			<input type="checkbox" name="nl2br" {if $node.nl2br}checked="checked"{/if}> nl2br
		</label>
		{if $user.canCode}
		<label>
			<input type="checkbox" name="code"
				{if $node.external_link|@startsWith:'template://'}checked="checked"{/if}
			> code
		</label>
		{/if}
		&nbsp;
		&nbsp;
		&nbsp;
		<a style="float: right;" href="{$node.link}/history">history</a>
	</div>

	<br>

	<div class="row">
		<div>id</div>
		<div>{$node.id}</div>
	</div>
	<div class="row">
		<div>name</div>
		<div>
			<input type="text" name="name" value="{($smarty.post.name|or:$node.name)|@htmlspecialchars}">

		</div>
	</div>
	{if $user.isAdmin}
	<div class="row">
		<div>external link</div>
		<div>
			<input type="text" name="external_link" value="{($node.external_link)|@htmlspecialchars}">
		</div>
	</div>
	{*<div class="row">
		<div>url name</div>
		<div>
			<input type="text" name="url_name" value="{($node.url_name)|@htmlspecialchars}">
		</div>
	</div>*}
	{/if}

	<div class="row">
		<div>access</div>
		<div><select name="access">
					<option value="public"		{if $node.access=='public'}selected{/if}	>public</option>
					<option value="moderated"	{if $node.access=='moderated'}selected{/if}	>moderated</option>
					<option value="private"		{if $node.access=='private'}selected{/if}	>private</option>
				</select>
				<label title="visible when not logged in (does not apply to private nodes)">
					<input type="checkbox" name="net_access" {if $node.net_access}checked="checked"{/if}> net
				</label>
		</div>
	</div>
	{if 1 or $user|isOwner:$node}
	<div class="row">
		<div>parent</div>
		<div><input type="text" class="small" name="id_parent" value="{$node.id_parent}"> or <a href="#">give node</a></div>
	</div>
	{/if}

	<div class="row">
		<div>template</div>
		<div><input type="text" class="small" name="id_template" value="{$node.id_template}"></div>
	</div>
	<div class="row">
		<div>image</div>
		<div><input type="file" name="image"></div>
	</div>
	<div class="row">
		<div title="masters can do everything that owner can, except to change owner">masters</div>
		<div><input type="text" name="masters" class="wide" value="{($node.namesMasters)|@htmlspecialchars}"></div>
	</div>
	<div class="row">
		<div title="silence is golden, duct tape is silver">silenced</div>
		<div><input type="text" name="silenced" class="wide" value="{($node.namesSilenced)|@htmlspecialchars}"></div>
	</div>
	{if $node.access=='private' or $node.access=='moderated'}
	<div class="row">
		<div title="allow access into private node">allow access</div>
		<div><input type="text" name="accessors" class="wide" value="{($node.namesAccessors)|@htmlspecialchars}"></div>
	</div>
	{/if}
	{perfcheck}


	<div class="actions">
		<div class="row">
			<input type="submit" value="save">
			or <a href="/id/{$node.id}">back</a>
		</div>
	</div>


</form>

{/if}

</section>
{/block}
