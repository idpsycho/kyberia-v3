{extends file="layouts/2-columns.tpl"}
{block mainbar}
<section class="mail">

{if $user.isGuest}
	you are logged out
{else}
	{literal}
	<script>
	$(function() {
		$('section.mail .check-all input').change(function() {
			var b = $(this).prop('checked');
			$('section.mail .message .checkbox').prop('checked', b);
		});
		$('section.mail').on('click', '.mail-recipient', function() {
			var to = $(this).data('to');
			if (!to) to = $(this).text();
			var $to = $('input[name=to]');

			var start = $(this).offset();
			var end = $to.offset();
			var x = (end.left - start.left)+'px';
			var y = (end.top - start.top)+'px';
			var $clone = $(this).clone().appendTo( $(this).css({position: 'relative'}) );
			$clone.css({position: 'absolute'}).animate({left: x, top: y}, function() {
				$to.val( to );
				$(this).remove();
				$('section.mail textarea').focus();
			});

			return false;
		});
	});
	</script>
	{/literal}

	{perfcheck}

	<form method="POST" action="/sendMail">

		<div class="add">
			<div class="row">
				<label>To:</label>
				<input type="text" name="to" value="{$smarty.post.to|or:$user.lastMailedUserName}"><br>
				{perfcheck}
			</div>
			<div class="row">
				<textarea name="message">{$smarty.post.message}</textarea>
			</div>
			<div class="row">
				<button>send</button>
			</div>
		</div>
	</form>

	<br>
	<br>

	<form method="POST" action="/deleteMail">

		<div class="controls">
			<input type="submit" value="delete">
			<label class="check-all">
				check all
				<input type="checkbox">
			</label>
		</div>

		<br>

		<div class="messages">
			{assign var=mails value=$user.mails}
			{perfcheck}
			{foreach from=$mails item=mail}

				{if $mail.has_read=='yes'}
					{assign var="unread" value=''}
				{else}
					{assign var="unread" value='unread'}
				{/if}
				<div class="message {$unread}">
					<a href="{$mail.from.link}" data-to="{$mail.from.login|@htmlspecialchars}" class="avatar mail-recipient" title="click to use">
						<img src="{$mail.from.avatar}">
					</a>
					<div class="header">
						from {$mail.from->tpl(link, [title=>'click to use'])}
						to {$mail.to->tpl(link, [title=>'click to use'])}
						<span class="unread">{$unread}</span>

						<input type="checkbox" name="mail_chosen[]" value="{$mail.id}" class="checkbox">
						<span class="datetime" title="{$mail.created}">{$mail.created}</a>
					</div>
					<div class="content">{$mail.message}</div>
				</div>

			{/foreach}

		</div>
	</form>
{/if}

</section>
{/block}
