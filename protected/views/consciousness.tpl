{extends file="layouts/node.tpl"}
{block content}
<section class="consciousness">

	{foreach from=$node.cons item=n}
		{$n->tpl(viewAsSubmission)}
	{/foreach}

</section>
{/block}
