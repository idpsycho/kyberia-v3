{extends file="layouts/node.tpl"}
{block content}
<section class="reactions">

	{assign var="usernode" value=$node.asUser}
	{if !$usernode}
		{assign var="usernode" value=$user}
	{/if}

	{foreach from=$usernode.userReactions item=n key=i}
		{$n->tpl(viewAsSubmission)}
	{/foreach}

</section>
{/block}
