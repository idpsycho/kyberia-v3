{extends file="layouts/node.tpl"}
{block content}
<section class="info">

	<style>
		section.info					{ padding-top: 20px; }
		section.info .row				{ margin-bottom: 20px; padding: 1px; }
		section.info .access			{ margin-bottom: 20px; padding: 1px; display: inline-block; }
		section.info .bordered			{ border: 1px solid #6dae42; }
		section.info .commanders		{ clear: both; padding: 10px; }
		section.info .commanders>div	{ display: inline-block; width: 150px; }
		section.info .commanders>div>*	{ vertical-align: middle; }
	</style>

	<div style="float: right;">
		<a href="{$node.link}/source">source</a> |
		<a href="{$node.link}/history">history</a>
	</div>

	<div class="info-permissions">
		<div class="row access commanders bordered">
			node access is <b>{$node.access}</b>
		</div>

		<h4>masters</h4>
		<div class="row masters commanders">
			{foreach from=$node.masters item=u}
				<div><img src="{$u.avatar}"> <a href="{$u.link}">{$u.login}</a></div>
			{/foreach}
		</div>

		<h4>silenced</h4>
		<div class="row silenced commanders">
			{foreach from=$node.silenced item=u}
				<div><img src="{$u.avatar}"> <a href="{$u.link}">{$u.login}</a></div>
			{/foreach}
		</div>

		{if $node.access=='moderated' or $node.access=='private'}
		<h4>access allowed</h4>
		<div class="row accessors commanders">
			{foreach from=$node.accessors item=u}
				<div><img src="{$u.avatar}"> <a href="{$u.link}">{$u.login}</a></div>
			{/foreach}
		</div>
		{/if}
	</div>

</section>
{/block}