{extends file="layouts/node.tpl"}
{block content}
<section class="k-new">

	{foreach from=$node.kNEW item=n}
		{$n|tpl:viewAsSubmission}
	{/foreach}

</section>
{/block}
