{extends file="layouts/node.tpl"}
{block content}
<section class="trending">

	{foreach from=$user.mostPopulatedNodes item=n}
		<div>
			<a href="{$n.location.link}">{$n.location.name}</a> - {$n.totalUsers}
		</div>
	{/foreach}

</section>
{/block}
