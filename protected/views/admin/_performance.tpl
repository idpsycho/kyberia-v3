<style>
	table.slowest	{ position: fixed; top: 100px; right: 50px; z-index: 8; }
	table.slowest td { padding: 0 10px; text-align: right; }
	table.slowest td.name { text-align: left; }

	.graph-fixed	{ position: fixed; left: 1%; bottom: 1%; right: 1%; height: 50px; z-index: 9; }
	.graph-relative	{ position: relative; width: 100%; height: 100%; background: rgba(255, 255, 255, 0.2); }
	.graph-bar		{ position: absolute; right: 0px; bottom: 0; height: 200px; width: 2px; background: white; }
	[name="NodeController::actionAdd"] { background: blue; }
	[name="Node::getChildrenDirect"] { background: #bb0; }
	[name="Node::getVisits"] { background: green; }
	[name="Node::getChildrenTree"] { background: red; }
	[name="Node::getAncestors"] { background: #0ff; }
	[name="User::getBookedNodes"] { background: #0a0; }
	[name^="."] { background: #f5a; color: black; }
</style>

{assign var=perf value=$user.performance}
{$min		= 0.05}
{$noTpl		= 0}
{$yScale	= 1}


<table class="slowest" style="background: black; color: white;">
	<tbody>
		<tr>
			{*<th>sum</th>*}
			<th>average</th>
			<th>n</th>
			<th>name</th>
		</tr>
		{assign var=calls value=$perf->getSlowest($min, $noTpl)}
		{foreach from=$calls item=call}
			<tr name="{$call.name}" title="{$call.callstacks}">
				{if !$call.isTickCall}
					<td>{intval($call.sumMs/$call.count)} ms</td>
				{else}
					<td>{$call.count}</td>
				{/if}
				<td>{$call.count}</td>
				<td class="name">{$call.name}</td>
			</tr>
		{/foreach}
	</tbody>
</table>

<style>
</style>

{assign var=last value=$perf->getLastMinutes($min, $noTpl)}
{assign var=now value=$perf.now}
<div class="graph-fixed">
	<div class="graph-relative">

	{foreach from=$last item=p}
		{assign var=t value=$p.created|@strtotime}
		{assign var=x value=($now - $t)/$min*3}
		{assign var=h value=($p.ms/1*$yScale)}
		<div class="graph-bar" style="right: {$x}%; height: {$h}%"
			name="{$p.name}" title="{''|cat:$p.name:' ':{$p.ms}:' ms'}">
		</div>
	{/foreach}

	</div>
</div>
