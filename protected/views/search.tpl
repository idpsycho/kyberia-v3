{extends file="layouts/node.tpl"}
{block content}
<section class="search">

	{* treba vymysliet odkial sa to ma volat, lebo z usera to nedava zmysel *}
	{foreach from=$node.searchResults item=n}
		{$n->tpl(viewAsSubmission, [trim=>'200'])}
	{/foreach}

</section>
{/block}
