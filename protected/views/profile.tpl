{extends file="layouts/node.tpl"}
{block content}
<section class="profile">

	{$node->tpl(viewAsSubmission)}
	{$node->tpl(reply, [addtext=>'add as friend'])}

	<div class="mutual-friends">
		{* you {if $user->hasFriended($node)}are{else}are not{/if} friend - *}
		{assign var=mutual value=$user->getMutualFriendsWith($node.id)}
		{assign var=count value=$mutual|@count}
		{$count} mutual friend{if $count!=1}s{/if}
		<br><br>
		{foreach from=$mutual item=friend}
			{$friend->tpl(avatar, [small=>1])}
		{/foreach}
	</div>

	{$node->tpl(viewLast)}

</section>
{/block}