{extends "layouts/center.tpl"}
{block content}
<section class="reset-password">

	<br><br>
	{if $smarty.get.key}
		<form action="/resetPassword" method="POST">
			<input type="hidden" name="email" value="{$smarty.get.email}">
			<input type="hidden" name="key" value="{$smarty.get.key}">
			<label>
				nove heslo:
				<br><br>
				<input type="text" name="new_password" placeholder="nove heslo" style="text-align: center;">
			</label>
			<br><br>
			<button>nastavit nove heslo</button>
		</form>
	{else}
		<p class="kyberia-important">
			zabudol/la/lo si heslo?
		</p>
		<br>
		<form action="/resetPassword" method="POST">
			<input type="text" name="email" placeholder="tvoj email" style="text-align: center;"></label>
			<br><br>
			<button>odoslat resetovaci link na email</button>
		</form>
	{/if}

</section>
{/block}
