{extends file="layouts/node.tpl"}
{block content}
<section class="visits">
	<table>
		<tbody>
			{foreach from=$node.visits item=visit key=i}
				<tr>
					<td>{$visit.last_visit}</td>
					<td>:: visited {$visit.visits}x</td>
					<td>by <a href="{$visit.user.link}" class="user-name">{$visit.user.login}</a></td>
					<td class="unread-children">
					{if $visit.unread_children>0}
						:: <span class="unread-children">{$visit.unread_children} NEW</span>
					{/if}
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
</section>
{perfcheck}

{/block}
