{extends file="layouts/node.tpl"}
{block content}
<section class="movement">

	<style>
		section.movement .now { opacity: 0.7; margin: 2px 0; }
		section.movement .away { color: #555; margin: 2px 0; }
		section.movement .away.days { color: #aaa; margin: 8px 0; }
	</style>

	<div class="now">{date('Y-m-d h:m:s')} - now</div>
	{assign var="last" value=time()}

	{assign var="usernode" value=$node.asUser}
	{if !$usernode}
		can only be used on users
	{else}

		{foreach from=$usernode.userMovement item=visit key=i}
			<div>
			{assign var="curr" value=strtotime($visit.last_visit)}
			{assign var="dt" value=($last-$curr)/60/60}
			{if $dt >= 1}
				{if $dt >= 24}
					<div class="away days">- away for {($dt/24)|@round} days -</div>
				{else}
					<div class="away hours">- away for {$dt|@round} hours -</div>
				{/if}
			{/if}

			{$visit.last_visit} - <a href="{$visit.node.link}">{$visit.node.name}</a>
			{*if $visit.unread_children > 0}
				:: <span class="unread-children">{$visit.unread_children} NEW</span>
			{/if*}

			{assign var="last" value=$curr}
			</div>
		{/foreach}

	{/if}

</section>
{perfcheck}

{/block}
