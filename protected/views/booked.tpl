{extends file="layouts/node.tpl"}
{block content}
<section class="booked">

	{assign var="usernode" value=$node.asUser}
	{if !$usernode}
		{assign var="usernode" value=$user}
	{/if}

	{$usernode|tpl:booked}

</section>
{/block}
