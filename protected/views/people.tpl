{extends file="layouts/node.tpl"}
{block content}
<section class="people">

	{foreach from=$user.activePeople item=u}
		{$u->tpl(people)}
	{/foreach}

</section>
{/block}
