<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_imagestrip($string)
{
    return preg_replace ("/<img(?:[^<>])* src(?:[ \t\r\n\v\f])*=(?:[ \t\r\n\v\f])*(.*?)>/","image: <a href=\\1 target=\"_blank\">\\1</a>",$string);
}

/* vim: set expandtab: */

?>
