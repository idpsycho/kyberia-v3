<?php
function smarty_modifier_number_pad($input, $pad_length, $pad_string="0", $pad_type=STR_PAD_LEFT)
{
	return str_pad($input, $pad_length, $pad_string, $pad_type);
}
?>