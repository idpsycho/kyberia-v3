<?php
/**
 * Smarty plugin
 * @package kybca_modifiers
 * @subpackage content
 */


/**
 * replaceLocalURLs modifier plugin
 *
 * Type:     modifier<br>
 * Name:     replace<br>
 * Purpose:  replace urls like http(s)?://(www.)?kyberia.sk used in href|src with local based urls
 * @param string
 * @return string
 */
function smarty_modifier_replaceLocalURLs($str_search = '')
{
    return preg_replace("{(src|href)=([\\\]*['\"]?)?http(s)?://(www.)?kyberia.(sk|eu|org)}i", "\\1=\\2", $str_search);
}

/* vim: set expandtab: */

?>
