<?php
function smarty_modifier_str_pad($input, $pad_length, $pad_string=" ", $pad_type=STR_PAD_RIGHT)
{
	return str_pad($input, $pad_length, $pad_string, $pad_type);
}
?>