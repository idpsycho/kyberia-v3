<?php
function smarty_modifier_html_substr($string, $length, $addstring = "") {
  if (mb_strlen($string) > $length) {
    if (!empty($string) && $length > 0) {
      $isText = true;
      $ret = "";
      $i = 0;

      $currentChar = "";
      $lastSpacePosition = -1;
      $lastChar = "";

      $tagsArray = array();
      $currentTag = "";
      $tagLevel = 0;

      $noTagLength = mb_strlen(strip_tags($string));

      // Parser loop
      for ($j = 0; $j < mb_strlen($string); $j++) {
        $currentChar = mb_substr($string, $j, 1);
        $ret .= $currentChar;

        // Lesser than event
        if ($currentChar == "<")
          $isText = false;

        // Character handler
        if ($isText) {
          // Memorize last space position
          if ($currentChar == " ")
          $lastSpacePosition = $j;
          else
          $lastChar = $currentChar;

          $i++;
        }
        else {
          $currentTag .= $currentChar;
        }

        // Greater than event
        if ($currentChar == ">") {
          $isText = true;

          // Opening tag handler
          if ((mb_strpos($currentTag, "<") !== false) &&
              (mb_strpos($currentTag, "/>") === false) &&
              (mb_strpos($currentTag, "</") === false)) {

            // Tag has attribute(s)
            if (mb_strpos($currentTag, " ") !== false) {
              $currentTag = mb_substr($currentTag, 1, mb_strpos($currentTag, " ") - 1);
            }
            else {
              // Tag doesn't have attribute(s)
              $currentTag = mb_substr($currentTag, 1, -1);
            }

            array_push($tagsArray, $currentTag);
          }
          elseif (mb_strpos($currentTag, "</") !== false) {
            array_pop($tagsArray);
          }

          $currentTag = "";
        } //*end* if ($currentChar == ">")

        if( $i >= $length)
          break;
      } //*end* for ($j = 0; $j < strlen($string); $j++)
  
      // Cut HTML string at last space position
      if ($length < $noTagLength) {
        if ($lastSpacePosition != -1)
          $ret = mb_substr($string, 0, $lastSpacePosition);
        else
          $ret = mb_substr($string, $j);
      }

      // Close broken XHTML elements
      while (count($tagsArray) != 0) {
        $aTag = array_pop($tagsArray);
        $ret .= "</" . $aTag . ">\n";
      }
    } //*end* if (!empty($string) && $length > 0)
    else {
      $ret = "";
    }
  
    // only add string if text was cut
    if (mb_strlen($string) > $length) {
      return $ret . $addstring;
    }
    else {
      return $ret;
    }
  } //*end* if (strlen($string) > $length)
  else {
    return $string;
  }
}
?>