<?php

// obj: $node/$user
function smarty_modifier_isOwner($obj, $property)
{
	return $obj->isOwner($property);
}
