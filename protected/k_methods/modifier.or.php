<?php

// obj: $node/$user
// use like this: {$smarty.post.postedValue|or:$model.alreadySet}
function smarty_modifier_or($obj, $property)
{
	return $obj ? : $property;
}
