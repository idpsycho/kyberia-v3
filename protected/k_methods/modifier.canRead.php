<?php

function smarty_modifier_canRead($obj, $property)
{
	return $obj->canRead($property);
}
