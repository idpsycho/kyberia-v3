<?php

function smarty_function_flashMessages($x, &$smarty)
{
	$s = '';
	$flashes = Yii::app()->user->getFlashes();
	if (count($flashes))
	{
		$s .= "\t<div class=\"kyberia-flashMessages\">\n";
		foreach ($flashes as $class => $msg)
		{
			if (!is_string($msg)) {$class.=' left'; $msg = 'Unknown Error: <br>'.pre_r($msg, true); }
			if (strpos($msg, 'Array')>0) $class .= ' left';
			$s .= "\t\t<div class='message $class' onclick='$(this).parent().fadeOut(\"fast\");'>\n";
			$s .= "\t\t\t$msg\n";
			$s .= "\t\t</div>\n";
			$s .= "\n";
		}
		$s .= "\t</div>\n";
	}
	return $s;
}


