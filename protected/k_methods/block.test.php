<?php
function smarty_block_test($params, $content, &$smarty)
{
	if ($params['tpl'])
	{
		//$smarty->assign('content', $content);
		return $smarty->fetch('layout/_layout-basic.tpl');
	}

	if ($content === NULL)
	{
		return "== BLOCK START ==";
	}
	else
	{
		return "$content\n".
				"== BLOCK END ==";
	}
}
