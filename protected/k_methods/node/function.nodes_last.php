<?php
function smarty_function_nodes_last($params, &$smarty)
{
	$crit = new CDbCriteria;
	$crit->limit = 100;
	$crit->order = 'id desc';

	if ($params['tiamated'])
		$arr = Tiamat::model()->findAll($crit);
	else
	if ($params['booked'])
	{
		$tags = Tag::model()->findAll('id_user=:u AND tag="book"', array(':u'=>user()->id));
		$arr = array();
		foreach ($tags as $t)
			$arr[] = $t->node;
	}
	if ($params['by_k'])
	{
		$crit->order = 'k desc';
		$crit->addCondition('created > NOW()-24*60*60');
		$arr = Node::model()->findAll($crit);
	}

	$smarty->assign('nodes_last', $arr);
}
