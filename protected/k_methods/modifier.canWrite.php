<?php

function smarty_modifier_canWrite($obj, $property)
{
	return $obj->canWrite($property);
}
