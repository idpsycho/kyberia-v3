<?php

function smarty_modifier_canEdit($obj, $property)
{
	return $obj->canEdit($property);
}
