<?php
/**
* V tomto subore su kadejake pomocne funkcie pouzivane napriec celou app
* Asi by sa to zislo upratat/preriedit - to teda zislo ;)
*/

mb_internal_encoding("UTF-8");

function swap(&$a, &$b)
{
	$tmp = $a;
	$a = $b;
	$b = $tmp;
}
function error($msg, $obj=null)
{
	if ($obj) $msg .= '<br>'.pre_r($obj, 1);
	showError($msg);
	return false;
}
function smarty($smarty=null)
{
	static $smarty_;
	if (!$smarty) return $smarty_;
	else $smarty_ = $smarty;
}
function smartyFetch($tpl, $assign)
{
	$smarty = smarty();
	foreach ($assign as $k=>$a)
		$smarty->assign($k, $a);
	return $smarty->fetch($tpl);
}
function NOW()
{
	return new CDbExpression('NOW()');
}
function showMessage($msg, $class='')
{
	return setFlash($class?:'', $msg);
}
function showError($msg)
{
	return setFlash('error', $msg);
}

function setFlash($type, $msg)
{
	$type .= ' '.uniqid();
	return Yii::app()->user->setFlash($type, $msg);
}

function redirectBack()
{
	Yii::app()->controller->redirect( Yii::app()->request->urlReferrer );
}
function redirect($url)
{
	Yii::app()->controller->redirect($url);
}

function redirectToId($id)
{
	Yii::app()->controller->redirect('/id/'.$id);
}


function file_add_contents($fname, $data)
{
	file_put_contents($fname, $data, FILE_APPEND);
}
function file_var_export($fname, $data, $append=0)
{
	file_put_contents($fname, var_export($data, 1)."\n\n", $append?FILE_APPEND:0);
}
function file_var_import($fname)
{
	$data = file_get_contents($fname);
	eval('$x = '.$data.';');
	return $x;
}

function prependBaseUrlIfNeeded($link, $bu)
{
	if (!startsWith($link, 'http')) {
		if (!startsWith($link, '/')) $link = "/$link";
		$link = $bu.$link;
	}
	return $link;
}

// only if exists, and handles directories, recursively
function unlink_($f)
{
	if (!file_exists($f)) return;
	if (is_dir($f)) rmdir_recur($f);
	else unlink($f);
}
function rmdir_recur($dir)	// must not end with '/'
{
	foreach(glob($dir . '/*') as $f)
	{
		if(is_dir($f))
			rrmdir($f);
		else
			unlink($f);
	}
	rmdir($dir);
}

function mkdir_for_file($fname)
{
	$parts = pathinfo($fname);
	$dir = $parts['dirname'];

	if (!is_dir($dir))
		mkdir($dir, 0777, true);
}

// something.txt
// something(2).txt
// something(3).txt
// ...
// something(99).txt
// something(99)c651cfbfb16.txt
// something(99)79fb6de651f.txt
// ...
function next_number_filename($fname)
{
	$i=2;
	$x='';

	$parts = pathinfo($fname);
	$dir = $parts['dirname'];
	$name = $parts['filename'];
	$ext = $parts['extension'];

	while (file_exists($fname))
	{
		$fname = "$dir/{$name}({$i}){$x}.$ext";

		if ($i>=99) $x = uniqid();	// if over 999, use randomness..
		else $i++;
	}
	return $fname;
}

// "http://bla.sk/page?q=5#"
// -> http-bla-sk-page-q-5-"
function make_valid_filename($s)
{
	$s = preg_replace('/[^a-zA-Z0-9]/', '-', $s);
	$s = preg_replace('/[-]+/', '-', $s);

	return $s;
}

function curl_get_contents($url, &$info=null, $debug=0)
{
	$h = curl_init($url);
	curl_setopt($h, CURLOPT_RETURNTRANSFER, TRUE);

	$resp = curl_exec($h);

	if (is_array($info) || $debug)
	{
		$opt = array(
			'CURLINFO_EFFECTIVE_URL'			=> CURLINFO_EFFECTIVE_URL,
			'CURLINFO_HTTP_CODE'				=> CURLINFO_HTTP_CODE,
			'CURLINFO_FILETIME'					=> CURLINFO_FILETIME,
			'CURLINFO_TOTAL_TIME'				=> CURLINFO_TOTAL_TIME,
			'CURLINFO_NAMELOOKUP_TIME'			=> CURLINFO_NAMELOOKUP_TIME,
			'CURLINFO_CONNECT_TIME'				=> CURLINFO_CONNECT_TIME,
			'CURLINFO_PRETRANSFER_TIME'			=> CURLINFO_PRETRANSFER_TIME,
			'CURLINFO_STARTTRANSFER_TIME'		=> CURLINFO_STARTTRANSFER_TIME,
			'CURLINFO_REDIRECT_COUNT'			=> CURLINFO_REDIRECT_COUNT,
			'CURLINFO_REDIRECT_TIME'			=> CURLINFO_REDIRECT_TIME,
			'CURLINFO_SIZE_UPLOAD'				=> CURLINFO_SIZE_UPLOAD,
			'CURLINFO_SIZE_DOWNLOAD'			=> CURLINFO_SIZE_DOWNLOAD,
			'CURLINFO_SPEED_DOWNLOAD'			=> CURLINFO_SPEED_DOWNLOAD,
			'CURLINFO_SPEED_UPLOAD'				=> CURLINFO_SPEED_UPLOAD,
			'CURLINFO_HEADER_SIZE'				=> CURLINFO_HEADER_SIZE,
			'CURLINFO_HEADER_OUT'				=> CURLINFO_HEADER_OUT,
			'CURLINFO_REQUEST_SIZE'				=> CURLINFO_REQUEST_SIZE,
			'CURLINFO_SSL_VERIFYRESULT'			=> CURLINFO_SSL_VERIFYRESULT,
			'CURLINFO_CONTENT_TYPE'				=> CURLINFO_CONTENT_TYPE,
			'CURLINFO_CONTENT_LENGTH_DOWNLOAD'	=> CURLINFO_CONTENT_LENGTH_DOWNLOAD,
			'CURLINFO_CONTENT_LENGTH_UPLOAD'	=> CURLINFO_CONTENT_LENGTH_UPLOAD,
		);
		if (!is_array($info)) $info = array();
		foreach ($opt as $k=>$var)
			$info[$k] = curl_getinfo($h, $var);

		if ($debug) file_var_export('wtf/curl_debug_info.txt', array($url, $info), 1);
	}

	curl_close($h);
	return $resp;
}



function post_get_contents($url, $post=null)
{
	$c = curl_init($url);
	curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($c, CURLOPT_HEADER, 0);
	curl_setopt($c, CURLOPT_FOLLOWLOCATION, 0);

	if ($post)
	{
        $encoded = http_build_query($post);
		curl_setopt($c, CURLOPT_POSTFIELDS, $encoded);
	}
	$resp = curl_exec($c);
	$err = curl_error($c);
	if ($err) file_var_export('logs/curl_err.txt', $err."\n\n", 1);
	//file_var_export("post_get_contents.txt", $resp);

	//list($header, $html) = explode("\r\n\r\n", $resp, 2);
	$html = $resp;

	return $html;
}
function brm_proxy_url()
{
	//$proxyUrl = "http://l/pub/";					// localhost psycho's pc
	//$proxyUrl = "http://85.216.129.133:8071/";	// gajova psycho's pc
	$proxyUrl = "http://37.157.193.203/proxy/";		// fuqu vps
	return $proxyUrl;
}
function file_get_contents_proxy($url)
{
	$proxy = brm_proxy_url();
	if ($proxy)	return post_get_contents($proxy, array('file'=>$url));
	else		return @file_get_contents($url);
}
function curl_getWithCookies_proxy($url, &$cookies, $post=null)
{
	//return curl_getWithCookies($url, $cookies, $post);

	$proxy = brm_proxy_url();
	$resp = post_get_contents($proxy, array('url'=>$url, 'cookies'=>$cookies, 'post'=>$post));
	file_var_export("curl-proxy.txt", $resp);
	$json = json_decode($resp);
	$cookies = $json->cookies;
	return $json->html;
}

function curl_getWithCookies($url, &$cookies, $post=null, $recursiveCall=0)
{
	$chrome = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.40 Safari/537.17';

	$c = curl_init($url);
	curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($c, CURLOPT_HEADER, 1);
	curl_setopt($c, CURLOPT_USERAGENT, $chrome);
	curl_setopt($c, CURLOPT_FOLLOWLOCATION, 0);

	if ($cookies)
		curl_setopt($c, CURLOPT_COOKIE, $cookies);
	if ($post)
	{
        $encoded = '';
        foreach($post as $n=>$v)
            $encoded .= urlencode($n).'='.urlencode($v).'&';
        $encoded = substr($encoded, 0, strlen($encoded)-1); // -&
		curl_setopt($c, CURLOPT_POSTFIELDS, $encoded);
	}
	$resp = curl_exec($c);
	$err = curl_error($c);
	if ($err) file_var_export('curl_err.txt', $err."\n\n", 1);

	list($header, $html) = explode("\r\n\r\n", $resp, 2);
	preg_match('/^Set-Cookie:\s*([^;]*)/mi', $header, $m);
    if ($m[1])  // read cookies, or leave the previous ones
	    $cookies = $m[1];
    preg_match('/Location:(.*?)\r?\n/', $header, $m);
	$location = trim($m[1]);
	curl_close($c);

	if ($location && !$recursiveCall)
		$html = curl_getWithCookies($location, $cookies, null, 'recursive call');

	$html .= '';
	return $html;
}




// extract first monile phone number: 090x/123 456
function extractTel($s, $sKontakt='')
{
	// z 80 realitiek iba 1 nezacinala s nulou (09xx)
	// a asi 2 mali pevnu linku cislo - osrat take pripady

	// 421 alebo 0	[optional]
	// 9xx			[any nums]
	// /			[optional]
	// 123456		[any nums with any spaces]
	$re = '/((?:\d\d\d ?-?|0)9(?:\d ?){2}\/? ?(?:\d ?){6})/';

	$m = Str::match($s, $re);
	$tel = formatTel($m);
	if (!$tel && $sKontakt)
	{
		// ak nic, tak akekolvek cislo v kontakte
		// 9xx123456 (9cifier), 2/12345678 (9cifier)
		$tel = Str::match($sKontakt, '/([:\d \/]{8}[\d \/]+)/');
		//pre_r(array($sKontakt, $tel)); //die();
	}
	return $tel;
}
function formatTel($tel)
{
	$tel = strval($tel);
	$tel = str_replace(' ', '', $tel);
	$tel = str_replace('/', '', $tel);

	if (strlen($tel) < 9)
		return '';

	// start with 0, and format last 9 numbers to: 0123 456 789
	$a = substr($tel, -9, 3);
	$b = substr($tel, -6, 3);
	$c = substr($tel, -3, 3);
	return "0$a $b $c";
}


function wordCount($i, $word1, $word234, $word5)
{
	if ($i==1) return $word1;
	if ($i>=2 && $i<=4) return $word234;
	return $word5;
}


function mysql_current_timestamp()
{
	return date("Y-m-d H:i:s");
}
// mysql_timestamp_is_older_than('2013-03-17 13:45:06', '1 day ago');
function mysql_timestamp_is_older_than($mysql_timestamp, $strtotime_input)
{
	$mysql = strtotime($mysql_timestamp);
	$input = strtotime($strtotime_input);
	return $mysql < $input;
}

function timer()
{
	static $t = 0;
	$ret = $t ? (microtime(1) - $t) : 0;
	$t = microtime(1);
	return number_format($ret, 2);
}

function daysAgo($t)
{
	return (time()-$t)/60/60/24;
}
function time_elapsed_string($ptime)
{
	$etime = time() - $ptime;

	if ($etime <= 0) {
		return 'a while ago';//in future '.time_elapsed_string(time() + $etime);
	}

	$a = array( 12 * 30 * 24 * 60 * 60	=>  'year',
				30 * 24 * 60 * 60		=>  'month',
				24 * 60 * 60			=>  'day',
				60 * 60					=>  'hour',
				60						=>  'minute',
				1						=>  'second'
				);

	foreach ($a as $secs => $str) {
		$d = $etime / $secs;
		if ($d >= 1) {
			$r = round($d);
			return $r . ' ' . $str . ($r > 1 ? 's' : '').' ago';
		}
	}
}

// bootstrap badge
function badge($val, $style)
{
	if (is_array($style))
		$style = $style[$val];

	if ($style == 'green')	$style = 'success';
	if ($style == 'yellow')	$style = 'warning';
	if ($style == 'red')	$style = 'important';
	if ($style == 'blue')	$style = 'info';
	if ($style == 'black')	$style = 'inverse';

	$style = $style ? "label-$style" : "";

	if (is_null($val)) return '';

	return "<span class='label $style'>$val</span>";
}



// nakoniec som to nepouzil (pomale alebo zacyklene), takze neotestovane
function mb_preg_pos($str, $re, $mb_offset=0)
{
	$offset = strlen( mb_substr($str, 0, $mb_offset) );
	if (!preg_match($re, $str, $matches, PREG_OFFSET_CAPTURE, $offset))
		return false;

	$mb_offset = mb_strlen( substr($str, 0, $matches[0][1]) );
	return $mb_offset;
}

// http://stackoverflow.com/questions/12339642/replace-characters-position-in-a-string
function mb_substr_replace($string, $replacement, $start, $length=0)
{
	return mb_substr($string, 0, $start) . $replacement . mb_substr($string, $start+$length);
}
function mb_substr_after($s, $afterThis)
{
	return mb_substr( $s, mb_strpos($s, $afterThis)+mb_strlen($afterThis) );
}

function strRemove($needle, $s, &$removed)
{
	$needle = removeDiacritics($needle);

	// if regex
	if (startsWith($needle, '/'))
	{
		$re = $needle;
		$re = regexWithDiacritics($re);
		$q = preg_match_all($re, $s, $m);

		foreach ($m[0] as $f)
			$removed .= "$f \n";

		return preg_replace($re, '', $s);
	}
	else
	{
		return str_replace_anyAccent($needle, '', $s, $removed);
	}
}

function str_replace_anyAccent($needle, $repl, $s, &$removed=null)
{
	if ($needle == '') {
		echo('str_replace_anyAccent ma prazdne needle');
		//return $repl;
	}
	$nondS = removeDiacritics($s);

	$arrPos = array();
	$pos = -1;
	while (false !== ($pos = mb_stripos($nondS, $needle, $pos+1)))
		$arrPos[] = $pos;

	$arrPos = array_reverse($arrPos);
	foreach ($arrPos as $pos)
	{
		$sublen = mb_strlen($needle);
		$rpl = $repl;
		$found = mb_substr($s, $pos, $sublen);
		if (false !== ($pp = mb_strpos($rpl, '$0')))
			$rpl = mb_substr_replace($rpl, $found, $pp, 2);

		$s = mb_substr_replace($s, $rpl, $pos, $sublen);
		$removed .= "$found \n";
	}
	return $s;
}

function mb_ucfirst($string)
{
	$strlen		= mb_strlen($string);
	$firstChar	= mb_substr($string, 0, 1);
	$then		= mb_substr($string, 1, $strlen - 1);
	return mb_strtoupper($firstChar) . $then;
}
function allCapsWordsToLower($s, &$removed=null)
{
	$re = '/\b[A-ZAÁÄCČDĎEÉĚIÍLĹĽNŇOÓÖŐRŔŘSŚŠTŤUÚÜŰŮYÝZŽ]{4,}\b/eu';

	if ('dbg out')
	{
		preg_match_all($re, $s, $m);
		$m = $m[0];
		if (count($m) > 10) $m = array_slice($m, 0, 10);
		if ($m) $removed .= 'lowered: ';
		foreach ($m as $x)
			$removed .= "$x ";
	}

	$s = preg_replace($re, 'mb_ucfirst(mb_strtolower("$0"))', $s);
	return $s;
}

function mapValues($value, $arrMap)
{
	return $arrMap[$value];
}

// working directory can change in shutdown function
$WORKING_DIRECTORY = '';
function register_shutdown_function_FIX_WORKING_DIRECTORY()
{
	global $WORKING_DIRECTORY;
	if (!$WORKING_DIRECTORY) $WORKING_DIRECTORY = getcwd();
	chdir($WORKING_DIRECTORY);
}

function dbg($s, $n=0, $allowHtml=0, $echo=1)
{
	register_shutdown_function_FIX_WORKING_DIRECTORY();

	if ($n > 0) $s = mb_substr($s, 0, $n);
	if (!$allowHtml) $s = htmlspecialchars($s);

	$mem = number_format(memory_get_usage()/1024/1024).'M';
	$mem = str_pad("($mem)", 7, " ", STR_PAD_LEFT);// " (125M)"

	$t = date("Y-m-d H:i:s")." $mem";
	$msg = "<small>{$t}</small>\t{$s}\n";

	if ($echo) echo $msg.'<br>';
	@file_put_contents("logs/dbg.txt", $msg, FILE_APPEND);
	return $msg;
}
// only to file, without echo (avoid breaking json requests)
function dbgf($s, $n=0, $allowHtml=0)
{
	dbg($s, $n, $allowHtml, 0);
}

function file_append($fname, $s)
{
	file_put_contents($fname, "$s\n", FILE_APPEND);
	echo "$s\n<br>";
}

///////////////////////////////////////////
function strhas_one($s, $what, &$match='')
{
	// if regex
	if (startsWith($what, '/'))
	{
		if (!endsWith($what, 'i')) $what .= 'i';

		// toto by tusim malo byt skor preg_match, kedze aj tak nevraciame vsetky vysledky..
		if (preg_match_all($what, $s, $m))
		{
			$match = $m[0][0];
			return $what;
		}
	}
	else
	{
		if (mb_stripos($s, $what)!==false)
		{
			$match = $what;
			return $what;
		}
	}
}
function strhas($s, $arr, &$matches='')
{
	if (!is_array($arr))
		return strhas_one($s, $arr, $matches);

	foreach ($arr as $find)
	{
		$f = strhas_one($s, $find, $matches);
		if ($f)
			return $f;
	}
}


function strposall($haystack, $needle, $offset = 0)
{
	$result = array();

	// Loop through the $haystack/string starting at offset
	for($i = $offset; $i<strlen($haystack); $i++){
		$pos = strpos($haystack,$needle,$i);
		if($pos !== FALSE){
			$offset =  $pos;
			if($offset >= $i){
				$i = $offset;

				// Add found results to return array
				$result[] = $offset;
			}
		}
	}
	return $result;
}


function shorten($s, $maxLen)
{
	if (mb_strlen($s) > $maxLen)
		$s = mb_substr($s, 0, $maxLen-3)."...";
	return $s;
}

function mb_strshorter($s, $shorterBy)
{
	$len = mb_strlen($s);
	return mb_substr($s, 0, $len - $shorterBy);
}
function mb_strhasShorter($s, $f, $shorterBy, $minLen=0)
{
	if ($minLen && mb_strlen($f) < $minLen)
		return false;

	$shorter = mb_strshorter($f, $shorterBy);
	if (mb_strhas($s, $shorter))
		return true;
}

function in_array_get($needle, $hay)
{
	foreach ($hay as $h)
	{
		if ($h == $needle)
			return $h;
	}
}
function array_randVal($arr)
{
	$k = array_rand($arr);
	return $arr[$k];
}


function startsWith($haystack, $needle)
{
	$length = mb_strlen($needle);
	return (mb_substr($haystack, 0, $length) === $needle);
}
function endsWith($haystack, $needle)
{
	$length = mb_strlen($needle);
	if ($length == 0) {
		return true;
	}

	return (mb_substr($haystack, -$length) === $needle);
}
function ensureEnding(&$s, $ending)
{
	if (!endsWith($s, $ending))
		$s .= $ending;
	return $s;
}
/*
function withoutFirstLine($s)
{
	$pos = strpos($s, "\n");
	if ($pos===false) return null;
	return substr($s, $pos+1);
}*/
function firstLine($s)
{
	preg_match("/[^\r\n]*/", $s, $m);
	return $m[0];
}
function withoutFirstLine($s)
{
	preg_match("/\n.*/si", $s, $m);
	return $m[0];
}
function withoutStart($s, $startsWith)
{
	if (!startsWith($s, $startsWith))
		return $s;

	$n = mb_strlen($startsWith);
	return mb_substr($s, $n);
}
function withoutEnd($s, $endsWith)
{
	if (!endsWith($s, $endsWith))
		return $s;

	$n = mb_strlen($endsWith);
	return mb_substr($s, 0, -$n);
}

function findFiles($dir, $wildcard=null)
{
	if (is_null($wildcard))
		$wildcard = '*';

	$dir = withoutEnd($dir, '/');
	$arr = array();
	if (file_exists($dir))
	{
		foreach (glob("$dir/$wildcard") as $path)
			$arr[] = $path;
	}
	return $arr;
}
function dirFiles($dir)	// toto treba zmazat, ked sa prepise kod na to hore
{
	$arr = array();

	if (is_dir($dir))
	if ($h = opendir($dir))
	{
		while (false !== ($n = readdir($h)))
		{
			if ($n=='.' || $n=='..') continue;
			$arr[] = $n;
		}

		closedir($h);
	}
	return $arr;
}

// toto vyhodit ked to nebude treba..
function rmdirRecursive($dirPath)
{
	if (!is_dir($dirPath))
		throw new InvalidArgumentException("$dirPath must be a directory");

	if (substr($dirPath, strlen($dirPath) - 1, 1) != '/')
		$dirPath .= '/';

	$files = glob($dirPath . '*', GLOB_MARK);
	foreach ($files as $file)
	{
		if (is_dir($file))	self::deleteDir($file);
		else				unlink($file);
	}
	rmdir($dirPath);
}


/**
 * Tazka klasika, srdce kazdeho pamatnika zaplesa
 */
function die_r($expression, $return = false)
{
	$x = pre_r($expression, $return);
	die();
	return $x;
}

function pre_r($expression, $return = false)
{
	if ($return)
	{
	  if (is_string($expression)) return '<pre>' . print_r(str_replace(array('<','>'), array('&lt;','&gt;'), $expression), true) . '</pre>';
		return '<pre>' . print_r($expression, true) . '</pre>';
	}
	else
	{
		echo '<pre>';
		if (is_string($expression)) print_r(str_replace(array('<','>'), array('&lt;','&gt;'), $expression), false);
		else print_r($expression, false);
		echo '</pre>';
	}
}

function onceEveryS($s=1)
{
	static $tLast = 0;
	$t = microtime(1);
	if ($tLast + $s <= $t) {
		$tLast = $t;
		return true;
	}
}

/**
 * Vrati base URL bez lomitka na konci alebo spravi z linku absolutny link
 * Priklad pouzitia: bu().'/images/drist.png' *alebo* bu('images/drist.png')
 */
function buimg($url=null)
{
	return bu().'/images';
}

function bu($url=null)
{
	static $baseUrl;
	if ($baseUrl === null) $baseUrl = Yii::app()->getRequest()->getBaseUrl(true); // true = chceme absolutnu url zacinajucu s http://...
	return $url === null ? $baseUrl : $baseUrl.'/'.ltrim($url,'/');
}


/**
* @return User model (NOT WebUser - because what would be the point, right?)
*/
function user()
{
	return Yii::app()->user->model;
}


/**
* Konvertuje "route" na URL, cize "controller/action", array('id'=>5), na http://blabla/controller/action/5
*
* @param mixed $route Bud 'cesta' alebo array('cesta', array(parametre)) alebo array('cesta', 'parameter1'=>'hodnota', 'parameter2'=>'hodnota')
* @param mixed $params
*/
function cu($route, $params = array()) { return createUrl($route, $params); }

function createUrl($route, $params = array())
{
	if (is_array($route))
	{
		if (is_array($route[1])) // format array('cesta', array(parametre))
			return Yii::app()->createAbsoluteUrl($route[0], $route[1]);
		else
		{ // format array('cesta', 'parameter1'=>'hodnota', 'parameter2'=>'hodnota')
			$r = array_shift($route);
			return Yii::app()->createAbsoluteUrl($r, $route);
		}
	}
	else return Yii::app()->createAbsoluteUrl($route, $params); // format 'cesta', array(parametre)
}

function url_byt($byt)
{
	return createUrl('byt/view', array(
		'id' => $byt->id,
		'url' => seo_url($byt->rooms.'-izbovy-byt-'.$byt->rozloha.'-m2-'.$byt->ulica.'-bratislava'),
	));
}


function url_izba($izba)
{
	$url = $izba->cela_izba ? 'izba-' : 'zdielana-izba-';
	if ($izba->nepriechodna) $url .= 'nepriechodna-';
	$url .= $izba->ulica.'-bratislava';

	return createUrl('izba/view', array(
		'id' => $izba->id,
		'url' => seo_url($url),
	));
}


/**
 * Sanitizer; zatial taka lite verzia (treba vobec inu?)
 */
function sanitize($string)
{
	return str_replace(array('&','<','>','"',"'"), array('&amp;','&lt;','&gt;','&quot;','&#39;'), $string);
}


function sanitizeFilename($filename)
{
	return strtr($filename, "<>[]{}|/\\:*?\"", "_____________");
}



function browser_is_bot()
{
	return (preg_match('/(googlebot|bingbot|baiduspider|msnbot|slurp|teoma|php|curl)/i', $_SERVER['HTTP_USER_AGENT'])
		|| strlen($_SERVER['HTTP_USER_AGENT']) == 0);
}



/**
 * Odstrani z HTMLka potencialne XSS betaroviny, inak tagy necha tak..
 * Na dlhych textoch to je (obviously) nie-moc-rychle (~80ms na 50kilobajtovom HTMLku), pouzivat najlepsie v kombinacii s cache
 * The credit goes to http://stackoverflow.com/questions/1336776/xss-filtering-function-in-php#answer-1741568
 */
function filter_xss($data)
{
	// Fix &entity\n;
	$data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
	$data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
	$data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
	$data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

	// Remove any attribute starting with "on" or xmlns
	$data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

	// Remove javascript: and vbscript: protocols
	$data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
	$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
	$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

	// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
	//$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
	//$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
	//$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

	// Remove namespaced elements (we do not need them)
	$data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

	$data = str_replace(
		array('<applet','<base','<gsound','<link','<embed','<frame','<frameset',/*'<iframe',*/'<ilayer','<layer','<link','<meta','<object','<script','<style','<title','<xml'),
		array('<!--',   '<!--', '<!--',   '<!--', '<!--',  '<!--',  '<!--',	 /*'<!--',*/   '<!--',  '<!--',   '<!--', '<!--', '<!--',   '<!--',   '<!--',  '<!--',  '<!--'),
		$data
	);

	$data = str_replace(
		array('</applet>','</base>','</gsound>','</link>','</embed>','</frame>','</frameset>',/*'</iframe>',*/'</ilayer>','</layer>','</link>','</meta>','</object>','</script>','</style>','</title>','</xml>'),
		array('</-->',	'</-->',  '</-->',	'</-->',  '</-->',   '</-->',   '</-->',	  /*'</-->',*/	'</-->',	'</-->',   '</-->',  '</-->',  '</-->',	'</-->',	'</-->',   '</-->',   '</-->'),
		$data
	);

	return $data;
}



/*
 * odstrani divne znamienka a interpunkcie a what not zo stringu
 remove diacritics, diakritiku
 */
function removeDiacritics($str) { return remove_accents($str); }
function regexWithDiacritics($re, $onlySK=1) { return regex_with_accents($re, $onlySK); }

function remove_accents($str)
{
	return str_replace (
	array(
		'à','á','â','ã','ä','å','æ', 'ç','ć','č','ď','đ','ð','è','é','ê','ë','ę','ě','ì','í','î','ï','ĺ','ľ','ł','ň','ñ','ò','ó','ô','õ','ö','ő','ø','ŕ','ř','ś','š','ß', 'ť','þ', 'ù','ú','û','ü','ű','ů','ý','ÿ','ž',
		'À','Á','Â','Ã','Ä','Å','Æ', 'Ç','Ć','Č','Ď','Ð',	'È','É','Ê','Ë','Ę','Ě','Ì','Í','Î','Ï','Ĺ','Ľ','Ł','Ň','Ñ','Ò','Ó','Ô','Õ','Ö','Ő','Ø','Ŕ','Ř','Ś','Š',	 'Ť','Þ', 'Ù','Ú','Û','Ü','Ű','Ů','Ý','Ÿ','Ž'
	),
	array(
		'a','a','a','a','a','a','ae','c','c','c','d','d','d','e','e','e','e','e','e','i','i','i','i','l','l','l','n','n','o','o','o','o','o','o','o','r','r','s','s','ss','t','th','u','u','u','u','u','u','y','y','z',
		'A','A','A','A','A','A','AE','C','C','C','D','D',	'E','E','E','E','E','E','I','I','I','I','L','L','L','N','N','O','O','O','O','O','O','O','R','R','S','S',	 'T','TH','U','U','U','U','U','U','Y','Y','Z'
	), $str );
}

// sk accents: áäčďéěíĺľňóôöőŕřśšťúüűůýž
function regex_with_accents($re, $onlySK=1)
{
	if (!$onlySK)
	{
		$accents = Array(
			'a'		=> '[aàáâãäå]',
			'c'		=> '[cçćč]',
			'd'		=> '[dďđð]',
			'e'		=> '[eèéêëęě]',
			'i'		=> '[iìíîï]',
			'l'		=> '[lĺľł]',
			'n'		=> '[nňñ]',
			'o'		=> '[oòóôõöőø]',
			'r'		=> '[rŕř]',
			's'		=> '[sśš]',
			't'		=> '[tť]',
			'u'		=> '[uùúûüűů]',
			'y'		=> '[yýÿ]',
			'z'		=> '[zž]',
	  	);
	}
	else
	{
		$accents = Array(
			'a'		=> '[aáä]',
			'c'		=> '[cč]',
			'd'		=> '[dď]',
			'e'		=> '[eéě]',
			'i'		=> '[ií]',
			'l'		=> '[lĺľ]',
			'n'		=> '[nň]',
			'o'		=> '[oóôöő]',
			'r'		=> '[rŕř]',
			's'		=> '[sśš]',
			't'		=> '[tť]',
			'u'		=> '[uúüűů]',
			'y'		=> '[yý]',
			'z'		=> '[zž]',
	  	);
	}

  	$re = removeDiacritics($re);
	$re = mb_strtolower($re);

	if (strpos($re, '['))
	{
		// taketo zapeklite situacie to este nepodporuje
	   	if (strpos($re, '\[') || strpos($re, '\]'))
	   		return $re;

		// '[a-z]' sprav VELKE
   		$re = preg_replace('/\[[^\[\]]+\]/e', 'mb_strtoupper("$0")', $re);
   		//pre_r($re);
   		//die();
   	}


	// dont replace modifiers
	$modif = preg_match('/\/[a-z]+$/', $re, $m);
	$m = $m[0];
	if ($m)
		$re = mb_substr($re, 0, -strlen($m));	// strip modifiers


	// replaceni male pismena za [aáä]
	// velke su [A-Z], tie nechame tak
	$re = str_replace(array_keys($accents), $accents, $re);
	$re = mb_strtolower($re);	// naspat na male


	if ($m)
	{
		if (!strhas($m, 'i')) $m .= 'i';	// case insensitive modifier
		if (!strhas($m, 'u')) $m .= 'u';	// unicode modifier
		$re .= $m;	// add modifiers
	}
	else
		$re .= 'ui';

	return $re;
}


/*
 * vygeneruje url bez divnych znakov
 * napr: vajcia.com/žvásť!)("dríst™ -> vajcia.com/zvast-drist
 */
function seo_url($s)
{
	$s = strtolower(remove_accents($s));
	$s = preg_replace('/[\']+/', '', $s);	// remove apostrof's character
	$s = preg_replace('/[^a-z0-9]+/', '-', $s);
	$s = trim($s, '-');
	return $s ? $s : '-';
}



function getDayNames($long = false)
{
	if ($long) return array('Nedeľa', 'Pondelok', 'Utorok', 'Streda', 'Štvrtok', 'Piatok', 'Sobota');
	else return array('ne','po','ut','st','št','pi','so');
}



/*
 * odosiela multipart spravy - html aj plaintext pre hnilych klientov
 *
 * linky pri plaintext mailoch su zamenene za samotne "href" hodnoty
 * html tagy a uderene charaktery su pri nich odstranene a "<br>" je zmenene na "\n"
 */
function send_mail($to, $subject, $body, $plaintext = NULL, $from = NULL)
{
	// ak neni zadany from, zvoli sa defaultny:
	if (!$from)
	{
		$from = Yii::app()->params['adminEmail'];
	}

	if (YII_DEBUG)
	{
		file_put_contents("protected/mail_debug.txt", " -> $to\n$subject\n\n$body\n\n\n", FILE_APPEND);
		return true; // debugovaci mod, nic neodosielame
	}

	// zostavi html mail:
	$html  = "<html>\n";
	$html .= "<body style=\"font-family: Verdana, Geneva, sans-serif; font-size: 14px; color:#333;\">\n";
	$html .= $body;
	$html .= "</body>\n";
	$html .= "</html>\n";

	// ak neni zadany plaintext, zostavi sa z povodneho html mailu:
	if ( ! $plaintext)
	{
		// odstrani "\n" a potom nahradi "<br>" za "\n" pre plaintext:
		$body = preg_replace('/<br\\s*?\/??>/i', "\n", str_replace("\n", '', $body));

		// odstrani tagy (okrem <a>)
		$plaintext = strip_tags($body, '<a>');

		// matchne "<a>" tag a jeho "href" hodnotu:
		$pattern = '%<a\s[^>]*href=["/\'](.*)["/\']\s*>(?:.*)</a>%im';
		preg_match_all($pattern, $plaintext, $matches, PREG_SET_ORDER);

		// vymeni nazov linku za "href" hodnotu a na koniec prida medzeru (vraj to pomaha):
		foreach ($matches as $link)
		{
			$plaintext = str_replace($link[0], $link[1] . ' ', $plaintext);
		}

		// odstrani interpunkciu a prehodi html entities na text
		$plaintext = remove_accents(html_entity_decode($plaintext));

		// prida newline na konci plaintext mailu (bez toho to blbne):
		$plaintext .= "\n";
	}

	# -=-=-=- MIME BOUNDARY

	$mime_boundary = "----awsm----" . md5(time());

	# -=-=-=- MAIL HEADERS

	$headers  = "From: $from\n";
	$headers .= "Reply-To: $from\n";
	//$headers .= "Return-Path: <yablko@seznam.cz>\n";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-Type: multipart/alternative; charset=UTF-8; boundary=\"$mime_boundary\"\n";

	# -=-=-=- TEXT EMAIL PART

	$message  = "--$mime_boundary\n";
	$message .= "Content-Type: text/plain; charset=UTF-8\n";
	$message .= "Content-Transfer-Encoding: 8bit\n\n";

	$message .= $plaintext;

	# -=-=-=- HTML EMAIL PART

	$message .= "--$mime_boundary\n";
	$message .= "Content-Type: text/html; charset=UTF-8\n";
	$message .= "Content-Transfer-Encoding: 8bit\n\n";

	$message .= $html;

	# -=-=-=- FINAL BOUNDARY

	$message .= "--$mime_boundary--\n\n";

	# -=-=-=- SEND MAIL

	$subject="=?UTF-8?B?".base64_encode($subject)."?=\n";

	$mail_sent = @mail( $to, $subject, $message, $headers );

	return $mail_sent;
}


/**
 * Otvori obrazok (jpg alebo png) a vrati ho spolu s jeho velkostou and such (false, ak sa nedal otvorit)
 */
function open_image($file)
{
	$info = new stdclass;
	list($info->width, $info->height, $info->type, $info->attr) = @getimagesize($file);

	if ($info->type == IMAGETYPE_JPEG)
	{
		$image = @imagecreatefromjpeg($file);
		if (!$image)
		{
			@imagedestroy($image);
			return false;
		}
	}
	else
	if ($info->type == IMAGETYPE_PNG)
	{
		$image = @imagecreatefrompng($file);
		if (!$image)
		{
			@imagedestroy($image);
			return false;
		}
	}
	else
	if ($info->type == IMAGETYPE_GIF)
	{
		$image = @imagecreatefromgif($file);
		if (!$image)
		{
			@imagedestroy($image);
			return false;
		}
	}
	else return false;

	$info->image = $image;

	return $info;
}


function normalize_jpg($src, $dst=null, $maxW=1024, $maxH=768, $quality=70, $maxKB=null)
{
	if (is_null($dst)) $dst = $src.'.jpg';

	$i = open_image($src);	// image, width, height, type (IMAGETYPE_JPEG/PNG/GIF)
	if (!$i) return false;

	$too_big = !is_null($maxKB) && filesize($src)>$maxKB*1024;
	$xScale = $maxW/$i->width;
	$yScale = $maxH/$i->height;
	if ($xScale < 1 || $yScale < 1 || $too_big)
	{
		if ($xScale < 1 || $yScale < 1)
		{
			if ($xScale < $yScale) // scale the other axis by the bigger one
				$maxH = $maxW * $i->height/$i->width;
			else
				$maxW = $maxH * $i->width/$i->height;
		}
		else
		{
			$maxW = $i->width;
			$maxH = $i->height;
		}

		$new = @imagecreatetruecolor($maxW, $maxH);
		@imagecopyresampled($new, $i->image, 0, 0, 0, 0, $maxW, $maxH, $i->width, $i->height);
		@imagejpeg($new, $dst, $quality);
		@imagedestroy($new);
	}
	else
	{
		copy($src, $dst);
	}

	@imagedestroy($i->image);
	return $dst;
}


/**
 * Vytvori stvorcovy thumbnail obrazku na rovnakom mieste ako sa nachadza povodny subor
 * Obrazok sa roztiahne/oreze tak, aby vyplnil celu plochu
 */
function create_thumbnail_square($filename, $size = 150, $thname='')
{
	if ($size < 16) return false;

	$img = open_image($filename);
	if (!$img) return false;

	// todo - ak sa velkost zhoduje, nic neresizovat, iba skopirovat

	if ($img->type == IMAGETYPE_GIF)
	{
		return ($img->width == 50);
	}

	if (!$thname)
		$thname = substr($filename, 0, strrpos($filename,'.'))."_th".$size.($img->type == IMAGETYPE_JPEG ? ".jpg" : ".png");

	$newimg = @imagecreatetruecolor($size, $size);
	imagealphablending($newimg, true);
	$transparent = imagecolorallocatealpha( $newimg, 0, 0, 0, 127 );
	imagefill( $newimg, 0, 0, $transparent );

	if ($img->width > $img->height)
		@imagecopyresampled($newimg,$img->image,0,0,($img->width - $img->height)/2,0, $size,$size,$img->height,$img->height);
	else
		@imagecopyresampled($newimg,$img->image,0,0,0,($img->height - $img->width)/2, $size,$size,$img->width,$img->width);

	if ($img->type == IMAGETYPE_JPEG)
		$res = @imagejpeg($newimg, $thname, 95); // zodpoveda to ~77 vo Photoshope
	else
	{
		imagealphablending($newimg, false);
		imagesavealpha($newimg, true);
		$res = @imagepng($newimg, $thname, 9, PNG_NO_FILTER);
	}

	showMessage('hm');

	@imagedestroy($img->image);
	@imagedestroy($newimg);

	if (!$res) return false;
	return true;
}



/**
* Vytvori thumbnail obrazku na presne rozmery (co precnieva sa odreze)
*
* @param mixed $filename
* @param mixed $width
* @param mixed $height
*/
function create_thumbnail_cropped ($filename, $width = 80, $height = 60)
{
	$img = open_image($filename);
	if (!$img) return false;

	$thname = substr($filename, 0, strrpos($filename,'.'))."_th.jpg";

	$newimg = @imagecreatetruecolor($width,$height);
	imagealphablending($newimg, true);
	$transparent = imagecolorallocatealpha( $newimg, 0, 0, 0, 127 );
	imagefill( $newimg, 0, 0, $transparent );

	if ($img->width/$img->height >= $width/$height) // zmensovany obrazok je sirsi ako chceme
	{
		$ratio = $width/$height;
		$newwidth = $img->height*$ratio;

		@imagecopyresampled($newimg,$img->image,
			0, 0,  // dst x1, y1
			$img->width/2 - $newwidth/2, 0, // src x1, y1
			$width, $height, // dst w, h
			$newwidth, $img->height // src w, h
		);
	}
	else // vyssi
	{
		$ratio = $width/$height;
		$newheight = $img->width/$ratio;

		@imagecopyresampled($newimg,$img->image,
			0, 0,  // dst x1, y1
			0, $img->height/2 - $newheight/2, // src x1, y1
			$width, $height, // dst w, h
			$img->width, $newheight // src w, h
		);
	}

	$res = @imagejpeg($newimg, $thname, 95); // zodpoveda to ~77 vo Photoshope

	@imagedestroy($img->image);
	@imagedestroy($newimg);

	if (!$res) return false;
	return true;
}



/**
* Vytvori thumbnail obrazku, snaziac sa vpratat do max rozmerov (ak je mensi, vrati false)
*
* @param mixed $filename
* @param mixed $width
* @param mixed $height
*/
function create_thumbnail_max ($filename, $width = 1280, $height = 800)
{
	$img = open_image($filename);
	if (!$img) return false;

	$thname = substr($filename, 0, strrpos($filename,'.'))."_resized.jpg";

	if ($img->width < $width && $img->height < $height) // obrazok netreba zmensovat
	{
		@imagedestroy($img->image);
		return false;
	}

	if ($width/$height > $img->width/$img->height) // sirsi
	{
		$newHeight = $height;
		$newWidth = round($height * ($img->width/$img->height));
	}
	else // vyssi
	{
		$newWidth = $width;
		$newHeight = round($width / ($img->width/$img->height));
	}

	$newimg = @imagecreatetruecolor($newWidth,$newHeight);
	/*imagealphablending($newimg, true);
	$transparent = imagecolorallocatealpha( $newimg, 0, 0, 0, 127 );
	imagefill( $newimg, 0, 0, $transparent );*/

	@imagecopyresampled($newimg,$img->image,
		0, 0,  // dst x1, y1
		0, 0, // src x1, y1
		$newWidth, $newHeight, // dst w, h
		$img->width, $img->height // src w, h
	);

	$res = @imagejpeg($newimg, $thname, 95); // zodpoveda to ~77 vo Photoshope

	@imagedestroy($img->image);
	@imagedestroy($newimg);

	if (!$res) return false;
	return true;
}



/**
* Vrati nazov thumbnailu na zaklade mena suboru
*/
function getThumbnail($filename, $width = 150)
{
	if ($filename == 'images/no_image.png') return $filename;

	return (substr($filename,0,-4)."_th$width".substr($filename,-4));
}


/******************************************************
*
* DB related funkcie
*
******************************************************/

/**
* Zjednoduseny sposob SELECTovania (oproti mega dlhemu PDO stylu)... Pouzitie:
* db_select("SELECT * FROM drist WHERE id = ? AND name = ?", 23, "somethin'")
*
* @param string $query
* @return CDbDataReader
*/
function db_select($query /*, params */)
{
	$cmd = Yii::app()->db->createCommand($query);

	$args = func_get_args();
	array_shift($args);
	$count = count($args);

	for ($i = 0; $i < $count; $i++) $cmd->bindValue(($i+1), $args[$i]);

	return $cmd->query();
}


/**
* Zjednoduseny sposob SELECTovania (oproti mega dlhemu PDO stylu). Vrati vsetky riadky v asociativnom poli.
* Pouzitie: db_select("SELECT * FROM drist WHERE id = ? AND name = ?", 23, "somethin'")
*
* @param string $query
* @return array
*/
function db_select_all($query /*, params */)
{
	$cmd = Yii::app()->db->createCommand($query);

	$args = func_get_args();
	array_shift($args);
	$count = count($args);

	for ($i = 0; $i < $count; $i++) $cmd->bindValue(($i+1), $args[$i]);

	$cmd->setFetchMode(PDO::FETCH_OBJ);
	return $cmd->queryAll();
}


/**
* Zjednoduseny sposob SELECTovania (oproti mega dlhemu PDO stylu). Vrati prvy riadok ako asociativne pole.
* Pouzitie: db_select("SELECT * FROM drist WHERE id = ? AND name = ?", 23, "somethin'")
*
* @param string $query
* @return array
*/
function db_select_one($query /*, params */)
{
	$cmd = Yii::app()->db->createCommand($query);

	$args = func_get_args();
	array_shift($args);
	$count = count($args);

	for ($i = 0; $i < $count; $i++) $cmd->bindValue(($i+1), $args[$i]);

	$cmd->setFetchMode(PDO::FETCH_OBJ);
	return $cmd->queryRow();
}


/**
* Zjednoduseny sposob SELECTovania (oproti mega dlhemu PDO stylu). Vrati rovno prvy stlpec prveho vysledku.
* Pouzitie: db_select("SELECT ano FROM drist WHERE id = ? AND name = ?", 23, "somethin'")
*
* @param string $query
* @return string
*/
function db_select_first($query /*, params */)
{
	$cmd = Yii::app()->db->createCommand($query);

	$args = func_get_args();
	array_shift($args);
	$count = count($args);

	for ($i = 0; $i < $count; $i++) $cmd->bindValue(($i+1), $args[$i]);

	$row = $cmd->queryRow();

	return ($row ? reset($row) : false);
}


/**
* Zjednoduseny sposob queryovania (pre vsetky prikazy okrem SELECTu). Vrati pocet ovplyvnenych riadkov.
* Pouzitie: db_query("UPDATE drist SET name = ? WHERE id = ?", "somethin'", 23)
*
* @param string $query
* @return array
*/
function db_query($query /*, params */)
{
	$cmd = Yii::app()->db->createCommand($query);

	$args = func_get_args();
	array_shift($args);
	$count = count($args);

	for ($i = 0; $i < $count; $i++) $cmd->bindValue(($i+1), $args[$i]);

	return $cmd->execute();
}


function db_last_insert_id()
{
	return Yii::app()->db->lastInsertID;
}

// output to file if the table is too big
// (i had problems with 64M table even thou mem was set to 128M)
function db_export($tables='', $fname=null)
{
	$config = include('protected/config/main.php');
	$dbcfg = $config['components']['db'];

	$dbconn = array();
	parse_str(str_replace(';','&',$dbcfg['connectionString']), $dbconn);

	$host = $dbconn['mysql:host'];
	$socket = $dbconn['unix_socket'];
	if ($socket) $host .= ":$socket";
	$user = $dbcfg['username'];
	$pass = $dbcfg['password'];
	$db = $dbconn['dbname'];

	return sql_export($host, $user, $pass, $db, $tables, $fname);
}
function sql_connect($host, $user, $pass, $db)
{
	mysql_connect($host, $user, $pass) or die('Error connecting to MySQL server: ' . mysql_error());
	mysql_select_db($db) or die('Error selecting MySQL database: ' . mysql_error());
	mysql_query("SET NAMES 'utf8';");
}
function sql_export($host, $user, $pass, $db, $tables='', $fname=null)
{
	sql_connect($host, $user, $pass, $db);

	//get all of the tables
	if(empty($tables) || $tables == '*')
	{
		$tables = array();
		$result = mysql_query('SHOW TABLES');
		while($row = mysql_fetch_row($result))
		{
			$tables[] = $row[0];
		}
	}
	else
	{
		$tables = is_array($tables) ? $tables : explode(',',$tables);
	}

	$rows_saved = 0;
	$data = '';
	if ($fname) file_put_contents($fname, '');	// clear

	//cycle through
	foreach($tables as $table)
	{
		$result = mysql_query('SELECT * FROM '.$table);
		$num_fields = mysql_num_fields($result);

		$data.= "\nDROP TABLE IF EXISTS `$table`;";
		$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
		$data.= "\n".$row2[1].";\n\n";

		$first = 1;
		$new_insert = 1;
		while($row = mysql_fetch_row($result))
		{
			if ($new_insert)
				$data.= ($first?'':';')."\nINSERT INTO `$table` VALUES\n";
			else
				$data.= ",\n";

			$first = 0;
			$data.="\t(";
			for($j=0; $j<$num_fields; $j++)
			{
				$x = $row[$j];
				if (is_null($x)) $x = 'NULL';
				else
				{
					$x = "'".addslashes($x)."'";
					$x = preg_replace("/\n/",'\n', $x);
					$x = preg_replace("/\r/",'\r', $x);
				}
				$data.= $x;
				if ($j<($num_fields-1)) { $data.= ', '; }
			}

			$data.= ")";

			$new_insert = 0;
			if ($fname && strlen($data) > 1024*1024)	// every 1MB
			{
				$new_insert = 1;
				file_add_contents($fname, $data);
				$data = '';
			}

			$rows_saved++;
		}
		$data.=";\n\n\n";
	}

	if ($fname)
	{
		file_add_contents($fname, $data);
		return $rows_saved;
	}
	return $data;
}






/**
 * Skonvertuje objekt odvodeny od CActiveRecord (t.j. barsjaky model) do pola s hodnotami
 */
function ar2array($active_record)
{
	if (!$active_record) return "empty";

	if (is_array($active_record))
	{
		$arr = array();
		foreach ($active_record as $ar) $arr[] = $ar->getAttributes();
		return $arr;
	}
	else
	{
		return $active_record->getAttributes();
	}
}



/**
 * Vypise hodnoty co su v objekte odvodenom od CActiveRecord (t.j. barsjakom modeli)
 */
function ar_r($active_record, $return = false)
{
	if ($return) return pre_r(ar2array($active_record), true);
	else pre_r(ar2array($active_record));
}






/********************************************************************************************
 *
 * Z Wordpressu *ehm* pozicany odriadkovavac, ktory veruze nie je uplne najblbsi a rata aj s takymi tagmi ako su <pre>, <script>, <style> atd.
 *
 ********************************************************************************************/

/**
 * Pomocna funkcia, neries
 */
function _autop_newline_preservation_helper( $matches )
{
	return str_replace("\n", "<WPPreserveNewline />", $matches[0]);
}

/**
 * Pomocna funkcia, neries
 */
function clean_pre($matches)
{
	if ( is_array($matches) )
		$text = $matches[1] . $matches[2] . "</pre>";
	else
		$text = $matches;

	$text = str_replace('<br />', '', $text);
	$text = str_replace('<p>', "\n", $text);
	$text = str_replace('</p>', '', $text);

	return $text;
}


function parseYoutubeLinkGetId($url)
{
	$reId = '/^([\w-]{11})$/';
	$reUrl = '/youtube\.com\/watch\?v=([\w-]{11})/';

	preg_match($reId, $url, $m);
	if ($m[1]) return $m[1];

	preg_match($reUrl, $url, $m);
	if ($m[1]) return $m[1];
}


/**
 * Odriadkovavac! Na vzdanie pocty Wordpressu zostalo meno funkcie :)
 * Okrem odriadkovania navyse aj popodciarkuje linky
 */
function wpautop($pee, $br = 1)
{
	if ( trim($pee) === '' ) return '';

	$pee = $pee . "\n"; // just to make things a little easier, pad the end

	$pee = make_clickable($pee);

	$pee = preg_replace('|<br />\s*<br />|', "\n\n", $pee);
	// Space things out a little
	$allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|select|option|form|map|area|blockquote|address|math|style|input|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';
	$pee = preg_replace('!(<' . $allblocks . '[^>]*>)!', "\n$1", $pee);
	$pee = preg_replace('!(</' . $allblocks . '>)!', "$1\n\n", $pee);
	$pee = str_replace(array("\r\n", "\r"), "\n", $pee); // cross-platform newlines
	if ( strpos($pee, '<object') !== false ) {
		$pee = preg_replace('|\s*<param([^>]*)>\s*|', "<param$1>", $pee); // no pee inside object/embed
		$pee = preg_replace('|\s*</embed>\s*|', '</embed>', $pee);
	}
	$pee = preg_replace("/\n\n+/", "\n\n", $pee); // take care of duplicates
	// make paragraphs, including one at the end
	$pees = preg_split('/\n\s*\n/', $pee, -1, PREG_SPLIT_NO_EMPTY);
	$pee = '';
	foreach ( $pees as $tinkle )
		$pee .= '<p>' . trim($tinkle, "\n") . "</p>\n";
	$pee = preg_replace('|<p>\s*</p>|', '', $pee); // under certain strange conditions it could create a P of entirely whitespace
	$pee = preg_replace('!<p>([^<]+)</(div|address|form)>!', "<p>$1</p></$2>", $pee);
	$pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee); // don't pee all over a tag
	$pee = preg_replace("|<p>(<li.+?)</p>|", "$1", $pee); // problem with nested lists
	$pee = preg_replace('|<p><blockquote([^>]*)>|i', "<blockquote$1><p>", $pee);
	$pee = str_replace('</blockquote></p>', '</p></blockquote>', $pee);
	$pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)!', "$1", $pee);
	$pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee);
	if ($br) {
		$pee = preg_replace_callback('/<(script|style).*?<\/\\1>/s', '_autop_newline_preservation_helper', $pee);
		$pee = preg_replace('|(?<!<br />)\s*\n|', "<br />\n", $pee); // optionally make line breaks
		$pee = str_replace('<WPPreserveNewline />', "\n", $pee);
	}
	$pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*<br />!', "$1", $pee);
	$pee = preg_replace('!<br />(\s*</?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)!', '$1', $pee);
	if (strpos($pee, '<pre') !== false)
		$pee = preg_replace_callback('!(<pre[^>]*>)(.*?)</pre>!is', 'clean_pre', $pee );
	$pee = preg_replace( "|\n</p>$|", '</p>', $pee );

	return str_replace('<br />', '<br>', $pee); // xhtml -> fuj
}




/**
* Spravi linky v texte klikatelnymi (obali ich <a> elementami)
*/
function make_clickable($text) {
	# this functions deserves credit to the fine folks at phpbb.com

	$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);

	// pad it with a space so we can match things at the start of the 1st line.
	$ret = ' ' . $text;

	// matches an "xxxx://yyyy" URL at the start of a line, or after a space.
	// xxxx can only be alpha characters.
	// yyyy is anything up to the first space, newline, comma, double quote or <
	$ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);

	// matches a "www|ftp.xxxx.yyyy[/zzzz]" kinda lazy URL thing
	// Must contain at least 2 dots. xxxx contains either alphanum, or "-"
	// zzzz is optional.. will contain everything up to the first space, newline,
	// comma, double quote or <.
	$ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);

	// matches an email@domain type address at the start of a line, or after a space.
	// Note: Only the followed chars are valid; alphanums, "-", "_" and or ".".
	$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);

	// Remove our padding..
	$ret = substr($ret, 1);
	return $ret;
}
