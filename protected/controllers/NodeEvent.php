<?php

class NodeEvent
{
	static function uploadImage($id_node)
	{
		$fname = "kyb-data/images/nodes/$id_node.gif";
		$tmp_name = $_FILES['image']['tmp_name'];
		$type = $_FILES['image']['type'];
		NodeEvent::_saveAttachedImage($fname, $tmp_name, $type);
	}

	static protected function _saveAttachedImage($fname, $tmp_name, $type)
	{
		if (!$tmp_name) return;
		if ($type != 'image/jpeg' && $type != 'image/png' && $type != 'image/jpg' && $type != 'image/gif')
			return error('bad file type, must be one of these: png/jpg/gif');

		$size = getimagesize($tmp_name);
		if ($size[0]*$size[1]*4 > 80*1024*1024) return error("image too big");

		if (file_exists($fname)) unlink($fname);
		@move_uploaded_file($tmp_name, $fname);
		if (!create_thumbnail_square($fname, 50, $fname))
			error('nepodarilo sa ulozit obrazok (ak uploadujes gif, musi byt siroky presne 50 pixelov)');
	}

	static function add($attrs)
	{
		// create node
		$node = new Node;
		$node->attributes = $attrs;
		$node->lastchild_created = NOW();	// fix weird database
		$node->k = 0;
		$node->views = 0;
		$node->children_count = 0;
		$node->save();


		// set vector
		NodeEvent::reparent($node, $attrs['id_parent'], 'DONT SAVE');
		$node->save();


		// update parent
		if (!$node->parent) return;
		$parent = $node->parent;
		$parent->lastchild_created = NOW();
		$parent->children_count++;
		$parent->save();

		$node->access = $parent->access;
		$node->net_access = $parent->net_access;
		$node->save();

		NodeAccessEvent::incrementChildCount($parent);

		// update ancestors
		$ancestor = $parent;
		while ($ancestor)
		{
			$ancestor->lastdescendant_created = NOW();
			$ancestor->save();
			$ancestor = $ancestor->parent;
		}

		return $node;

	}

	static function delete($node)
	{
		// tmp solution..
		$node->id_parent = 123456;
		$node->content = '';
		$node->title = '';
		$node->save(false);
	}

	static function parentChanged(&$node, $dont_save)
	{
		// recalc vector
		$node->vector = NodeManager::calcVector($node);
		if (!$dont_save)
			$node->save();

		// update subtree with new vector
		foreach ($node->children as $child)
			NodeEvent::parentChanged($child);
	}

	static function tagged($node, $tagName)
	{
		if ($tagName == 'k')
		{
			$k = Tag::model()->count('tag = :t AND id_node = :n', array(':t'=>$tagName, ':n'=>$node->id));
			$node->k = $k;
			$node->save();
		}
	}

	static function reparent(&$node, $id_newParent, $dontSave=0)
	{
		if ($node->parent->asUser) TagController::actionUnfriend($node->parent, user());

		$newParent = NodeManager::byId($id_newParent);

		if ($newParent->asUser) TagController::actionFriend($newParent, user());

		$node->id_parent = $id_newParent;
		$node->vector = NodeManager::calcVector($node);
		$node->id_parent_user = $newParent->id_user;
		NodeEvent::parentChanged($node, $dontSave);
	}

	static function edit($node, $attrs)
	{
		$oldParent = $node->id_parent;
		$oldContent = $node->content;

		$parentChanged = (isset($attrs['id_parent']) && $oldParent != $attrs['id_parent']);
		if ($parentChanged)
		{
			NodeEvent::reparent($node, $attrs['id_parent'], 'DONT SAVE');
		}

		$node->attributes = $attrs;
		if (isset($attrs['content']) && ($attrs['content'] != $oldContent))
			$node->updated = NOW();

		if (!$node->save())
			error( $node->getErrors() );
	}

	static function entered($node)
	{
		$user = user();
		if ($user->isGuest) return;

		NodeAccessEvent::enteredNode($node, $user);
		UserEvent::enteredNode($node, $user);
	}

	static function entered_afterRender($node)
	{
		$user = user();
		if ($user->isGuest) return;
		if ($node->id == 24) {
			MailEvent::userOpenedMails($user);
		}
	}

};
