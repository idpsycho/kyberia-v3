<?php


class MailController
{

	static function actionSendMail($node, $user)
	{
		MailController::actionSendMail_from($node, $user);
	}

	/* - actually, asi by bolo lepsie, keby sa maily z ubika posielaju tak ze sa zanho admin prihlasi
	static function actionSendByUbik($node, $user)
	{
		if (!$user->isAdmin) return error('taketo veci moze robit len adminko');

		$ubik = UserManager::ubik();
		MailController::actionSendMail_from($node, $ubik);
	}
	*/

	static function actionSendMail_from($node, $user)
	{
		$message = $_POST['message'];
		if ($user->isGuest)		return error('nie si prihlaseny');
		if (!$message)			return error('prazdna sprava? pouzi poke.. oh wait, toto nie je facebook');

		$arrTo = explode(';', $_POST['to']);
		if (count($arrTo) > 10 && !$user->isAdmin)
			return error('nemozes poslat postu viac ako 10 userom naraz');

		$alreadyMailed = array();
		foreach ($arrTo as $to)
		{
			if (!$to || $alreadyMailed[$to]) continue;

			$alreadyMailed[$to] = 1;

			$userTo = UserManager::find( $to );
			if (!$userTo) {
				error('nikoho s nickom alebo idckom "'.htmlspecialchars($to).'" nepoznam');
				continue;
			}
			if (UserRelation::find_($user, $userTo, UserRelation::$BLOCK_MAIL)) {
				error('uzivatel "'.htmlspecialchars($to).'" si od teba nezela dostavat maily :\'(');
				continue;
			}
			if (UserRelation::find_($userTo, $user, UserRelation::$BLOCK_MAIL)) {
				error('na uzivatela "'.htmlspecialchars($to).'" mas nastavene blokovanie mailov<br>ak mu chces napisat, musis si ho odblokovat');
				continue;
			}

			$a = array(
				'id_user'					=> $user->id,
				'id_from'					=> $user->id,
				'id_to'						=> $userTo->id,
				'message'					=> $message,
				'created'					=> NOW(),
				'has_read'					=> 'no',
			);

			MailEvent::send($a);
		}

		return true;
	}

	static function actionDeleteMail($node, $user)
	{
		if ($user->isGuest)				return error('nie si prihlaseny');

		$mail_chosen = $_POST['mail_chosen'];
		foreach ($mail_chosen as $id_mail)
		{
			MailEvent::delete($id_mail);
		}

		return true;
	}

}
