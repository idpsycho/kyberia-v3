<?php


class TagController
{
	static function actionK($node, $user)
	{
		if ($user->isGuest)				return error('nie si prihlaseny');
		if ($user->hasGivenK($node))	return error('node si uz k udelil, uz to nehul');

		TagEvent::addGivenk($node);
		TagEvent::setNodeAccessK($node, $user);
		TagEvent::increaseK($node);
	}

	static function actionBook($node, $user)
	{
		if ($user->isGuest)				return error('nie si prihlaseny');
		if ($user->hasBooked($node))	return; // ignore silently

		TagEvent::book($node, $user);
	}
	static function actionUnbook($node, $user)
	{
		if ($user->isGuest)				return error('nie si prihlaseny');
		if (!$user->hasBooked($node))	return; // ignore silently

		TagEvent::unbook($node, $user);
	}


	static function actionFriend($node, $user)
	{
		if ($user->isGuest)				return error('nie si prihlaseny');
		if ($user->hasFriended($node))	return; // ignore silently

		TagEvent::friend($node, $user);
	}
	static function actionUnfriend($node, $user)
	{
		if ($user->isGuest)				return error('nie si prihlaseny');
		if (!$user->hasFriended($node))	return; // ignore silently

		TagEvent::unfriend($node, $user);
	}


	static function actionFook($node, $user)
	{
		if ($user->isGuest)				return error('nie si prihlaseny');
		if ($user->hasFooked($node))	return; // ignore silently

		TagEvent::fook($node, $user);
	}
	static function actionUnfook($node, $user)
	{
		if ($user->isGuest)				return error('nie si prihlaseny');
		if (!$user->hasFooked($node))	return; // ignore silently

		TagEvent::unfook($node, $user);
	}


	static function actionBlockMail($node, $user)
	{
		if ($user->isGuest)				return error('nie si prihlaseny');
		if ($user->hasBlockedMail($node))return; // ignore silently

		TagEvent::blockMail($node, $user);
	}
	static function actionUnblockMail($node, $user)
	{
		if ($user->isGuest)				return error('nie si prihlaseny');
		if (!$user->hasBlockedMail($node))return; // ignore silently

		TagEvent::unblockMail($node, $user);
	}

}
