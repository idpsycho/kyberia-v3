<?php


class AdminController
{

	static function actionAdmin($node, $user)
	{
		if (!$user->isAdmin)	return error('ale ved ty nie si adminko');

		if ($_POST['action'] == 'recalcVectors')	AdminController::recalcVectors();
		if ($_POST['action'] == 'recalcChildCount')	AdminController::recalcChildCount();
		if ($_POST['action'] == 'recalcParentUser')	AdminController::recalcParentUser();

		redirectBack();
		return true;
	}

	static function recalcParentUser()
	{
		$ID = 'node_id';

		$ids = Node::model()->findAll(array('select'=>"$ID"));
		foreach ($ids as $m)
		{
			$id = $m->id;
			$node = Node::model()->findByPk($id);
			$node->id_parent_user = $node->parent->id_user;
			if (!$node->save(false))
				return;
		}
	}

	static function recalcVectors()
	{
		$ID = 'node_id';
		$ids = Node::model()->findAll(array('select'=>"$ID", 'order'=>"$ID"));
		$tStart = time();

		file_put_contents('logs/vectors.txt', count($ids)." nodes to recalculate\n");
		foreach ($ids as $m)
		{
			$id = $m->id;
			$node = Node::model()->findByPk($id);
			$node->vector = NodeManager::calcVector_iterate_ancestors($node);
			$dt = time() - $tStart;
			file_add_contents('logs/vectors.txt', "$dt\t{$node->id}\t{$node->vector}\n");
			if (!$node->save())
				return;
		}
	}

	static function recalcChildCount()
	{
		$ids = Node::model()->findAll(array('select'=>'id'));

		foreach ($ids as $m)
		{
			$node = Node::model()->findByPk($m->id);
			$node->children_count = Node::model()->count('id_parent = :p', array(':p'=>$m->id));
			if (!$node->save())
				return;
		}
	}

}


