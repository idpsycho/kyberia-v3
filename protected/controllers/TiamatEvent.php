<?php

class TiamatEvent
{

	static function add($node, $user)
	{
		$t = new Tiamat;
		$t->attributes = $node->attributes;
		$t->update_performed = NOW();
		$t->node_destructor = $user->id;
		$t->save();
	}

};