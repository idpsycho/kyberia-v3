<?php

class TagEvent
{

	static function addGivenk($node)
	{
		$g = GivenK::findOrCreate($node);
		$g->cas = NOW();
		$g->save();
	}
	static function setNodeAccessK($node, $user)
	{
		$na = NodeAccess::findOrCreate($node, $user);
		$na->given_k = 'yes';
		$na->save();
	}
	static function increaseK($node)
	{
		$node->k++;
		$node->save();
	}



	static function book($node, $user)
	{
		$ur = UserRelation::findOrCreate($node, $user, UserRelation::$BOOK);
		$ur->save();

		// toto sa pouziva v inych selectoch, ale hej, je to duplicitne..
		$na = NodeAccess::findOrCreate($node, $user);
		$na->bookmarked = 'yes';
		$na->save();
	}
	static function unbook($node, $user)
	{
		$ur = UserRelation::find_($node, $user, UserRelation::$BOOK);
		if ($ur) $ur->delete();

		// toto sa pouziva v inych selectoch, ale hej, je to duplicitne..
		$na = NodeAccess::find_($node, $user);
		if ($na) {
			$na->bookmarked = 'no';
			$na->save();
		}
	}



	static function friend($node, $user)
	{
		$ur = UserRelation::findOrCreate($node, $user, UserRelation::$FRIEND);
		$ur->save();
	}
	static function unfriend($node, $user)
	{
		$ur = UserRelation::find_($node, $user, UserRelation::$FRIEND);
		if ($ur) $ur->delete();
	}



	static function fook($node, $user)
	{
		$na = UserRelation::findOrCreate($node, $user, UserRelation::$FOOK);
		$na->save();
	}
	static function unfook($node, $user)
	{
		$na = UserRelation::find_($node, $user, UserRelation::$FOOK);
		if ($na) $na->delete();
	}
	static function blockMail($node, $user)
	{
		$na = UserRelation::findOrCreate($node, $user, UserRelation::$BLOCK_MAIL);
		$na->save();
	}
	static function unblockMail($node, $user)
	{
		$na = UserRelation::find_($node, $user, UserRelation::$BLOCK_MAIL);
		if ($na) $na->delete();
	}


};