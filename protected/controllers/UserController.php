<?php

class UserController
{
	static function actionEdit($user)
	{
		$a = array(
			'login'					=> $_POST['name'],	// name lebo to je podla nody
			'email'					=> $_POST['email'],
			'password'				=> $_POST['password'],
		);

		if (!$a['login'])			return error('nechces mat meno?');
		if (!$a['email'])			return error('chybny mail');
		if ($a['password'] && strlen($a['password']) < 6)
									return error('heslo musi mat aspon 6 znakov');

		$existing = UserManager::byName($a['login']);
		if ($existing && $existing->id != $user->id)
			return error('uzivatel s menom '.htmlspecialchars($a['login']).' uz existuje');

		UserEvent::edit($user, $a);
	}
	static function actionLogin($node, $user)
	{
		$login = $_POST['login'];
		$pwd = $_POST['password'];
		$ui = new UserIdentity($login, $pwd);

		if (!$ui->authenticate())
		{
			redirect('/resetPassword');
			showError('nespravny login alebo heslo');
		}
		else
		{
			Yii::app()->user->login($ui, 30*60);
			UserEvent::loggedIn( user() );
		}
	}

	static function actionLogout($node, $user)
	{
		if (!$user->user_id)	return error('ved nie si prihlaseny..');

		Yii::app()->user->logout();
		UserEvent::loggedOut($user);
	}

	static function makeKey($id, $action)
	{
		$encrypt = '6tWG)#@U R)3rj0pwij(P*YP#'.$id.'_!*&#%">'.$action.'HTPO(*)^(*!&penis';
		return md5($encrypt);
	}
	static function actionResetPassword($node, $user)
	{
		sleep(1);

		$email = $_POST['email'];
		if (!$email) return error('nezadal si mail');

		$user = User::model()->find('email = :m', array(':m'=>$email));
		if (!$user)		return error('user s takym emailom neexistuje');

		if ($_POST['new_password'])
		{
			$keySubmitted = $_POST['key'];
			$key = UserController::makeKey($email.$user->last_action, 'resetPassword');
			if ($keySubmitted != $key)
				return error('zly key');

			UserEvent::setNewPassword($user, $_POST['new_password']);
			showMessage('<br>Heslo zmenene.<br><br>Teraz sa mozes prihlasit s novym heslom.<br><br>');
			redirect('/');
		}
		else
		{
			UserEvent::sendPasswordResettingLink($email, $user->last_action);
		}
	}


	static function actionRegister($node, $user)
	{
		$n = array(
			'content'							=> $_POST['registration'],
			'id_user'							=> User::idOf('ubik'),
			'id_parent'							=> Node::idOf('registrations'),
			'id_template'						=> Node::idOf('profile'),
			'name'								=> "user registration for ".$_POST['login'],
		);
		$u = array(
			'login'								=> $_POST['login'],
			'email'								=> $_POST['email'],
			'password'							=> $_POST['password'],
			'rights'							=> 'verify_email',
		);

		if ($user->id)							return error('na zaregistrovanie alterega sa treba najprv odhlasit ;)');
		if (strlen($n['content']) < 10)			return error('moc si o sebe nenapisal...');
		if (!$u['login'])						return error('zabudol si napisat login');
		if (User::byName($u['login']))			return error('taky login uz existuje..');
		if (!$u['password'])					return error('a heslo?');
		if (strlen($u['password']) < 6)			return error('heslo by malo mat aspon 6 znakov');
		if ($u['password'] != $_POST['password2'])	return error('hesla nesedia. uz to nehul.');

		UserEvent::register($n, $u);

		redirect('/registered');
	}


}
