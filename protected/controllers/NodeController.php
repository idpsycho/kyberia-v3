<?php

/*
	1. ACTION - validacia a nakoniec zavolanie eventu, ktory uz len vykona zmeny
	2. EVENT - nastavenie a ulozenie modelov
*/


class NodeController
{
	static function actionAdd($node, $user)
	{
		if ($user->isGuest) return;

		if ($_POST['id_put_into'])
			return NodeController::actionPut($node, $user);

		$a = array(
			'id_user'					=> $user->id,
			'id_parent'					=> $node->id,
			'id_parent_user'			=> $node->id_user,
			'id_template'				=> 4,//Template::idOf($_POST['template'], 4),
			'content'					=> $_POST['content'],
			'name'						=> $_POST['name'] ? : $node->newChildName,
			'nl2br'						=> 1,
			'created'					=> NOW(),
			'net_access'				=> 'yes',
			'access'					=> 'public',
		);

		if ($_POST['code'] && !$user->canCode) return error('nemas prava na code');
		if (!$a['id_parent'])			return error('nevybral si nodu kam to mam dat..');
		if (!$a['id_user'])				return error('nie si prihlaseny');
		if (!$a['name'])				return error('chyba v nazve :(');
		if (!$a['content'])				return error('chces pridat prazdnu nodu?');
		if (!$user->canWrite_slow($node)) return error('nemas prava');

		if ($_POST['no_html'])
			$a['content'] = htmlspecialchars( $a['content'] );

		if (NodeController::catchDoublePost($a))
			return error('prave som ta zachranil od double-postu (:');

		$a['content'] = NodeController::_htmlPurifier( $a['content'] );
		$a['name'] = NodeController::_htmlPurifier( $a['name'] );

		NodeEvent::add($a);

		return true;
	}

	static function catchDoublePost($attrs)
	{
		$ID_USER = 'node_creator';
		$ID = 'node_id';
		$last = Node::model()->find(array(
			'condition' => "$ID_USER = :u",
			'params' => array(':u'=>$attrs['id_user']),
			'order' => "$ID desc",
		));

		if (!$last) return false;
		if ($last->content != $attrs['content']) return false;
		$ago = time() - strtotime($last->created);
		if ($ago > 10) return false;

		return true;
	}
	static function actionEdit($node, $user)	// content, name, id_parent, nl2br?
	{
		$a = array(
			'name'						=> $_POST['name'],
			'access'					=> $_POST['access'],
			'net_access'				=> $_POST['net_access'] ? 1 : 0,
			'content'					=> $_POST['content'],
			'id_parent'					=> $_POST['id_parent'],
			'id_template'				=> NodeManager::idOf( $_POST['id_template'] ),
			'external_link'				=> $_POST['external_link'],
			'nl2br'						=> $_POST['nl2br'] ? 1 : 0,
		);

		$tpl = $a['external_link'];

		if (!$node)						return error('nevybral si nodu');
		if (!$a['content'] && !$tpl)	return error('prazdny content (daj tam aspon bodku)');
		if (!$a['name'])				return error('prazdny nazov sa neda dat');
		if (!$a['id_parent'] && $node->id_parent) return error('nenastavil si parenta');
		if (!$a['id_template'])			return error('zla templata');
		if (!$user->canEdit($node))		return error('nemas prava');

		if ($_POST['no_html'])
			$a['content'] = htmlspecialchars( $a['content'] );
		if (!$_POST['code'])
			$a['content'] = NodeController::_htmlPurifier( $a['content'] );

		$a['name'] = NodeController::_htmlPurifier( $a['name'] );

		if ($node->content != $a['content'])
			TiamatEvent::add($node, $user);

		NodeEvent::edit($node, $a);

		if (isset($_FILES['image']))
			NodeEvent::uploadImage($node->id);

		PermissionController::actionEdit($node, $user);
		if ($node->id == $user->id)
			UserController::actionEdit($user);

		redirectToId($node->id);
		return true;
	}

	static function actionReparent($node, $user)
	{
		$a = array(
			'id_parent'					=> $_POST['id_parent'],
		);

		if (!$node)						return error('nevybral si nodu');
		if (!$a['id_parent'])			return error('nenastavil si parenta');
		if (!$user->canModerate_slow($node))
										return error('nemas prava');

		NodeEvent::reparent($node, $a['id_parent']);
	}

	static function actionPut($node, $user)
	{
		$id = $_POST['id_put_into'];
		if (!preg_match('/(?:^|\/)([0-9]+)/', $id, $m))	return error('bad id_put_into');

		$id_parent = $m[1];
		$a = array(
			'id_hardlinked'				=> $node->id,
			'id_parent'					=> $id_parent,
			'id_user'					=> $user->id,
			'created'					=> NOW(),
		);


		if (!$a['id_parent'])			return error('nevybral si nodu kam to mam dat..');
		if (!$a['id_hardlinked'])		return error('nevybral si nodu ktoru mam putnut..');
		if (!$a['id_user'])				return error('nie si prihlaseny');
		if (!$user->canWrite_slow(Node::byId($a['id_parent'])))
										return error('nemas prava');

		NodeEvent::add($a);
	}

	static function actionDelete($node, $user)
	{
		if (!$node)						return error('nevybral si nodu');
		if (!$user->canModerate_slow($node))
										return error('nemas prava');

		NodeEvent::delete($node);
		return true;
	}


	static function _htmlPurifier($content)
	{
		require_once('protected/vendors/htmlpurifier/HTMLPurifier.standalone.php');
		require_once('protected/vendors/htmlpurifier/standalone/HTMLPurifier/Filter/YouTube.php');

		$config = HTMLPurifier_Config::createDefault();
		$config->set('Filter', 'YouTube', true);
		$config->set('Attr', 'AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
		$config->set('Core', 'Encoding', 'UTF-8');

		$purifier = new HTMLPurifier($config);
		// mensi hack na \n lebo purifier to osekava
		$content = str_replace(array("\r\n", "\r", "\n"), "<br class=\"nl2br\" />", $content);
		$content = $purifier->purify($content);
		// mensi hack na \n lebo purifier to osekava
		$content = str_replace("<br class=\"nl2br\" />", "\n", $content);
		$content = preg_replace("{(((\s)|(\n)|(^))+)(http://|ftp://|https://)([a-zA-Z0-9\_\-\?\~\_][^,\s]*)}i","\\2<a target='_blank' href=\"\\6\\7\">\\6\\7</a>",$content);

		$replaceLocalURLs = function($str_search) {
			return preg_replace("{(src|href)=([\\\]*['\"]?)?http(s)?://(www.)?kyberia.(sk|eu|org)}i", "\\1=\\2", $str_search);
		};
		$content = $replaceLocalURLs($content);
		return $content;
	}

};

