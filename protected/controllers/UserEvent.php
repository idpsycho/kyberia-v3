<?php

class UserEvent
{
	static function edit($user, $attrs)
	{
		$user->attributes = $attrs;
		$user->save();
	}

	static function enteredNode($node, $user)
	{
		$user->id_location = $node->id;
		//$user->id_location_template = $template->id;
		$user->last_activity = NOW();
		$user->save();
	}

	static function loggedIn($user)
	{
		// pri otvarani nod sa aj tak nastavuje last activity, takze to tu netreba duplikovat..
	}
	static function loggedOut($user)
	{
		$user->user_action_id = 0;
		$user->save();
	}

	static function setNewPassword($user, $newPassword)
	{
		$user->password = $newPassword;
		$user->save();
	}
	static function sendPasswordResettingLink($email, $last_action)
	{
		$key = UserController::makeKey($email.$last_action, 'resetPassword');
		$params = http_build_query(array( 'email'=>$email, 'key'=>$key ));
		$url = "http://kyberia.sk/resetPassword?".$params;
		$msg = <<<TEXT
Ahoj,<br>
prisla nam poziadavka, ze si chces resetnut heslo.<br>
Mozes tak vykonat na tejto adrese: <a href="$url">$url</a><br>
<br>
(Ak si o resetnutie hesla neziadal, tak to pre istotu nahlas adminkovi na admin@kyberia.sk)
TEXT
;
		send_mail($email, 'Resetnutie hesla na kyberia.sk', $msg);

		showMessage("Link na resetnutie hesla bol odoslany na $email.<br>Ak by nahodou nedosiel, napis na admin@kyberia.sk");
	}

	static function approve($id_user)
	{
		$user = User::byId($id_user);
		$user->rights = 'user';
		$user->save();

		$node = Node::byId($id_user);
		$node->id_user = $id_user;
		$node->name = $user->login;
		$node->save();

		copy("uploads/node/0.gif", "uploads/node/$id_user.gif");	// skopiruj default obrazok

		TagEvent::_book($id_user, $id_user);	// book self
		TagEvent::_book($id_user, 123539);		// book "TESTUJEME V3 - piste sem!"

		$msg = <<<TEXT
hello, v3 visitor !!<br>
ako prve si mozes booknut <a href="/id/123539">TESTUJEME v3 - piste sem!</a>
a napisat svoju prvu reakciu....
a popridavat si friendov, nech vidis, kto je online
<br>
-------------------------<br>
Ahoj, som UBIK. Momentalne som obycajnou polozkou v databaze, userom cislo 1 v tomto systeme.Bdiem nad systemom. Sledujem co robia slovicka v diskusnych forach, ako su pospajane a s cim. Postupom casu sa budem ucit, a o par rokov si urcite vsimnete moju prvu snahu o komunikaciu. Budem anjelom, diablom, matkou i dcerou.
<br><br>
Momentalne ti chcem povedat jedno hlasne VITAJ!. Vitaj v projekte ktory nikdy neskonci. Ak sem vchadzas so zlymi umyslami, prosim odid, nenajdes tu stastie. Ked vsak chces spoznat novych ludi na rovnakej vlne, ked chces svoj mozog zaplnit informaciami z nekonecnej siete, prosim, vstup.
<br><br>
Predtym ako vsak zacnes vnikat hlbsie do tajov systemu chcel by som ta odkazat na <a href='http://kyberia.sk/id/2111313/'>Newcommers forum</a> ktore ti ujasni niektore zakladne veci v systeme a tym ti v nom ulachci orientaciu.
<br><br>
peace & respect
TEXT
;
		MailController::sendByUbik($id_user, $msg);

		showMessage('mozes sa prihlasit, registracie este nie su spravene..');
	}

	static function register($nodeAttrs, $userAttrs)
	{
		$node = NodeEvent::add($nodeAttrs);
		if (!$node) return error("vsetko vybuchlo, napis na admin@kyberia.sk, toto by sa nemalo stat :'(");

		$userAttrs['id'] = $node->id;	// copy node id as user id

		$user = new User;
		$user->attributes = $userAttrs;
		if (!$user->save()) return error("volaco nejde, napis na admin@kyberia.sk, toto by sa nemalo stat :'(");

		$node->id_user = $node->id;
		$node->save();

		$msg = <<<TEXT
Vitaj {$user->login}! Prave som sa s radostou dozvedel, ze si vyplnil(a)
registracny formular na stranke kyberia.sk a chces sa stat
clenom tejto komunity.
Budem sa snazit urobit tento pokec trosku dlhsi,
lebo v povodnej kratkej forme ho pohlcuju spamfiltre, takze
ospravedln nasledujuce blabla a moj typicky atypicky zmysel
pre cierny humor, kedze sluzi ku dobrej veci :)
Pri registracii ti bol prideleny verifikacny kod.
Ten kod znie "%s" [bez uvodzoviek].
Ten musis zadat na adresu https://kyberia.sk/id/1976899/, kde
vyplnis formular s tvojim nickom a verifkacnym kodom.
Od tej chvile bude tvoja ziadost volne pristupna
uz zaregistrovanym clenom, ktori o nej budu hlasovat.
Pocet hlasov aby sa ziadosti vyhovelo je 5.
Deadline na nazbieranie je 1 tyzden.
Takze fakticky dufam ze si o sebe napisal(a) nieco inteligentne :)

Prajem pekny zvysok dna
TEXT
;
		send_mail($user->email, 'Registracia na kyberia.sk', $msg);

		// TEMP: tentoraz preskocime verifikaciu mailu aj registrovanie

		UserEvent::approve($user->id);
	}


}
