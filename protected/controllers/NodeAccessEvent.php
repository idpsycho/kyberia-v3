<?php

class NodeAccessEvent
{
	static function incrementChildCount($parent)
	{
		// optimize
		$ID_NODE = 'node_id';
		$visits = NodeAccess::model()->findAll("$ID_NODE = :n", array(':n'=>$parent->id));

		foreach ($visits as $v) {
			if (!$v->unread_children) $v->unread_children = 0;
			$v->unread_children++;
			$v->save();
		}
	}

	static function enteredNode($node, $user)
	{
		$ID_NODE = 'node_id';
		$ID_USER='user_id';
		$visits = NodeAccess::model()->find("$ID_NODE = :n AND $ID_USER = :u",
								array(':n'=>$node->id, ':u'=>$user->id));

		if (!$visits)
		{
			$visits = new NodeAccess;
			$visits->visits = 0;
			$visits->id_node = $node->id;
			$visits->id_user = $user->id;
		}
		$visits->last_visit = NOW();
		$visits->unread_children = 0;
		$visits->visits++;
		$visits->save();
	}

}
