<?php


class PermissionEvent
{
	static function addIfNotExists(&$existing, $id_node, $user_idOrName, $permission)
	{
		if (!$user_idOrName) return;
		$id_user = UserManager::idOf($user_idOrName);
		if (!$id_user) error("user with id or login '".htmlspecialchars($user_idOrName)."' doesnt exist");

		$ID_NODE = 'node_id';
		$ID_USER = 'user_id';
		$na = NodeAccess::model()->find("$ID_NODE = :n AND $ID_USER = :u",
							array(':n'=>$id_node, ':u'=>$id_user));

		foreach ($existing as $p)
		{
			if ($p->id_user != $id_user) continue;

			$p->keep = true;
			break;
		}

		if ($na)
		{
			$na->keep = true;
			if ($na->permission != $permission)
			{
				$na->permission = $permission;
				$na->save();
			}
			return true;
		}
		else
		{
			$p = new NodeAccess;
			$p->id_user = $id_user;
			$p->id_node = $id_node;
			$p->permission = $permission;
			$p->save();
		}
	}

	static function update($id_node, $masters, $silenced, $accessors)
	{
		$ID_NODE = 'node_id';
		$PERMISSION = 'node_permission';
		$existing = NodeAccess::model()->findAll("$ID_NODE = :n AND $PERMISSION IS NOT NULL",
							array(':n'=>$id_node));

		foreach ($masters as $user_idOrName)
			PermissionEvent::addIfNotExists($existing, $id_node, $user_idOrName, 'master');

		foreach ($silenced as $user_idOrName)
			PermissionEvent::addIfNotExists($existing, $id_node, $user_idOrName, 'silence');

		foreach ($accessors as $user_idOrName)
			PermissionEvent::addIfNotExists($existing, $id_node, $user_idOrName, 'access');

		foreach ($existing as $p)
			if (!$p->keep)
			{
				$p->permission = null;
				$p->save();
			}

	}
}
