<?php

class SiteController extends Controller
{
	public $layout='//layouts/mini';
	public $tRequestStart;

	public function actionMain()
	{
		if (Yii::app()->user->isGuest)
		{
			$main = NodeManager::byId(1);
			$this->pageTitle = 'Welcome to Kyberia V3';
			$this->render('main', array(
				'user'=>user(),
				'node'=>$main,
			));
		}
		else {
			$this->pageTitle = 'Dashboard :: V3';
			$this->actionDashboard();
		}
	}

	public function actionDashboard()
	{
		$main = NodeManager::byId(1);

		NodeEvent::entered($main);

		$this->renderNodeWithTpl($main, '/site/dashboard');
		/*$this->render('dashboard', array(
			'node' => $main,
			'user' => user(),
		));*/
	}

	public function actionTemplate($template)
	{
		if (Yii::app()->request->isPostRequest)
			return $this->processPostRequest();

		$tplIds = array(
			'mail'			=> 24,
			'people'		=> 27,
			'nodeshell'		=> 2,
			'forum'			=> 3,
			'submission'	=> 4,
			'profile'		=> 7,
			'source'		=> 14,
			'k-new'			=> 15,
			'dashboard'		=> 19,
			'last'			=> 23,
			'mail'			=> 24,
			'search'		=> 25,
			'people'		=> 27,
			'registration'	=> 31,
			'consciousness'	=> 788016,
			'edit'			=> 1961033,
			'reactions'		=> 3579407,
			'submissions'	=> 1573668,
			'visits'		=> 1961037,
			'last'			=> 3755160,
			'permissions'	=> 2224463,
			'visits'		=> 748131,
			'movement'		=> 1911535,
			'given-k'		=> 1752584,
			'booked'		=> 1970182,
		);
		$id = $tplIds[$template];

		if ($id) $node = NodeManager::byId($id);
		if (!$node) $node = NodeManager::byId(1);

		$this->renderNodeWithTpl($node, $template);
	}

	function renderNodeWithTpl($node, $tpl, $params=null)
	{
		$ajax = startsWith($tpl, 'ajax') || Yii::app()->request->isAjaxRequest;

		if ($node && !$ajax) NodeEvent::entered($node);

		$params['user'] = user();
		$params['node'] = $node;
		$this->pageTitle = htmlspecialchars($tpl).' :: V3';

		Performance::start("TPL: $tpl");
		$this->render('/'.$tpl, $params);
		Performance::end("TPL: $tpl");

		if ($node && !$ajax) NodeEvent::entered_afterRender($node);

		Performance::finishedRequest($this);
	}

	function init()
	{
		Performance::startedRequest();
	}

	function processPostRequest()
	{
		$node_by_id = $this->getNode('only by id');
		$action = $_GET['template'] ? : ($_GET['url_name'] ? : $_POST['action']);
		list($controller, $action) = $this->findAction( $action, $node_by_id );

		if (!$controller || !$action) throw new CHttpException(403, 'take nieco neviem spravit :(');

		Performance::start("$controller::$action");
		$success = $controller::$action( $node_by_id, user() );
		Performance::end("$controller::$action");

		redirectBack();

		$node = $node_by_id ? : $this->getNode();
		if (!$node)
			die('tu nic neni, posledna akcia ta asi mala niekam presmerovat.. vynadaj adminovi..');
	}

	public function actionId()
	{
		if (Yii::app()->request->isPostRequest)
			return $this->processPostRequest();

		$node = NodeManager::byId( $_GET['id_node'] );
		if (!$node)
			return $this->renderNodeWithTpl($node, 'error', array('error'=>'not found'));

		if (!user()->canRead($node))
			return $this->renderNodeWithTpl($node, 'no-permission');

		$tpl = $this->determineTpl($node);
		$this->renderNodeWithTpl($node, $tpl);
	}

	function getNode($only_by_id=0)
	{
		if (isset($_GET['url_name']) && !$only_by_id)
		{
			$url_name = $_GET['url_name'];
			$node = NodeManager::byName($_GET['url_name']);
			if (!$node && $must_exist) throw new CHttpException(404, "noda zaregistrovana ako '$url_name' neexistuje");
		}
		if (isset($_GET['id_node']))
		{
			$id_node = $_GET['id_node'];
			$node = NodeManager::byId($id_node);
			if (!$node) throw new CHttpException(404, "noda $id_node neexistuje, asi si dal o jednu cifru naviac");
		}
		return $node;
	}
	function determineTpl($node)
	{
		if (isset($_GET['template']))	// url_name, ale pouziva sa to aj ako action
			return $_GET['template'];

		$tplById = array(
			1		=> 'site/main',
			2		=> 'nodeshell',
			3		=> 'forum',
			4		=> 'submission',
			7		=> 'profile',
			14		=> 'source',
			15		=> 'k-new',
			19		=> 'dashboard',
			23		=> 'last',
			24		=> 'mail',
			25		=> 'search',
			27		=> 'people',
			31		=> 'registration',
			788016	=> 'consciousness',
			1961033	=> 'edit',
			3579407	=> 'reactions',
			1573668	=> 'submissions',
			1961037	=> 'visits',
			3755160	=> 'last',
			2224463	=> 'permissions',
			748131	=> 'visits',
			1911535	=> 'movement',
			1752584	=> 'given-k',
			1970182	=> 'booked',
		);
		if ($node)
			$tpl = $tplById[$node->template_id];

		return $tpl ? : 'submission';
	}
	public function findAction($action, $node_by_id)
	{
		if (!$action && $node_by_id) $action = 'edit';

		$events = array(
			'NodeController'	=> array('add', 'edit', 'delete', 'put', 'reparent'),
			'TagController'		=> array('k', 'unk', 'book', 'unbook', 'fook', 'unfook', 'blockMail', 'unblockMail'),
			'UserController'	=> array('register', 'settings', 'login', 'logout', 'lockout', 'friend', 'unfriend', 'resetPassword'),
			'MailController'	=> array('sendMail', 'deleteMail', 'ubikMail', 'ubikMailToAll'),
			'AdminController'	=> array('admin'),
		);

		foreach ($events as $controller=>$actions)
			foreach ($actions as $a)
				if ($action == $a)
					return array($controller, 'action'.ucfirst($action));
	}

	public function actionError()
	{
		$error = Yii::app()->errorHandler->error['message'];
		die($error);
	}

}
