<?php

class MailEvent
{
	static function send($attrs)
	{
		$orig = new Mail;
		$orig->attributes = $attrs;
		$orig->save();

		$attrs['id_user'] = $attrs['id_to'];
		$attrs['id_orig_mail'] = $orig->id;

		$copy = new Mail;
		$copy->attributes = $attrs;
		$copy->save();
	}

	static function delete($id)
	{
		$ID = 'mail_id';
		$ID_USER = 'mail_user';
		$ID_ORIG_MAIL = 'mail_duplicate_id';
		$HAS_READ = 'mail_read';

		$mail = Mail::model()->find("$ID = :id AND $ID_USER = :u",
					array(':id'=>$id, ':u'=>user()->id));
		if (!$mail)
			return error('mazes neexistujuci mail');

		if (is_null($mail->id_orig_mail))
		{
			$copy = Mail::model()->find("$ID_ORIG_MAIL = :id AND $HAS_READ = 'no'", array(':id'=>$id));
			if ($copy)
				$copy->delete();
		}

		$mail->delete();
	}

	static function userOpenedMails($user)
	{
		$ID_TO = 'mail_to';
		$HAS_READ = 'mail_read';

		$id_user = $user->id;
		$n = Mail::model()->updateAll(
			array('mail_read'=>'yes'),
			"$ID_TO = $id_user AND $HAS_READ = 'no'");
	}

};
