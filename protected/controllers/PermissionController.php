<?php


class PermissionController
{
	static function actionEdit($node, $user)
	{
		if (!$node)						return error('nezadal si ze o ktoru nodu sa jedna..');
		if ($user->isGuest)				return error('nie si prihlaseny');
		if (!$user->canWrite($node))	return error('nemas prava');

		$id_node = $node->id;
		$masters = explode(';', $_POST['masters']);
		$silenced = explode(';', $_POST['silenced']);
		$accessors = explode(';', $_POST['accessors']);

		PermissionEvent::update($id_node, $masters, $silenced, $accessors);
	}
}
