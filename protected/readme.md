# kyberia v3 source code structure

## kyberia-specific
- controllers - actions: give k, add node, make friend
- k_methods - getters for smarty templates
- k_templates - templates, layout..

## top-level handling
- components - universal libraries
- models - basic yii models (new functionality goes to k_methods and k_events)
- controllers - main site controller, error handler, routing id/, api/, and maybe some more
- views - basic non-template based viewers (possibly unused in the end)

## config and unused translations
- config - framework options
- messages - translation


## backend functionality explanation
1. user views some node - SiteController::actionId loads node and necessary template, and renders using smarty
- templates are in /k_templates and methods used are in /k_methods or inside models in /models
2. user action (k, book, add node) - SiteController->findAction finds the right controller
3. controller validates action and performs proper action - eg. NodeController::actionAdd
4. model is saved - there are some handlers in yii models, like beforeSave or afterSave
5. beforeSave is catched - for example if we addNode, then we need to update vector in afterSave
6. VectorController::eventUpdateSubtree - this handles all the internal actions

so:
- actions are posted to SiteController, which routes them further
- validation happens inside MODELcontroller->actionACTIONNAME
- catching database updates happens with yii events like beforeSave
- on events, all internal actions are done in MODELcontroller->eventEVENTNAME

if you know how to make the source better structured, i'm listening