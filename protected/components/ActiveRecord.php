<?php

/*
	compatibility layer while being locked in old table..

	function getAttributes_MAP() { return __CLASS__::$attributes_MAP; }
	static $attributes_MAP = array(
		'id'				=> 'user_id',
	);

	PS: dont forget to inherit from ActiveRecord (without C)
*/

class ActiveRecord extends CActiveRecord
{

	function save($runValidation = true, $attributes = NULL)
	{
		if (!parent::save($runValidation, $attributes))
		{
			//error();
			throw new Exception(var_export($this->getErrors(), 1));
			return false;
		}
		return true;
	}
	function setAttributes($values, $safeOnly = true)
	{
		$map = $this->attributes_MAP;
		foreach ($map as $new=>$old)
		{
			if ($old==$new) continue;
			if (isset($values[$new]))
			{
				$values[$old] = $values[$new];
				unset($values[$new]);
			}
		}

		parent::setAttributes($values, $safeOnly);
	}

}