<?php


function smarty_function_lessToCss($params, Smarty_Internal_Template &$smarty)
{
    require_once(sprintf('%s/vendors/lessc.inc.php', Yii::app()->basePath));

    $lessFilePath = sprintf('%s/../assets/less/%s', Yii::app()->basePath, $params['file']);
    $lessFileNameExpl = explode('/', $params['file']);
    $cssFileName = str_replace('.less', '.css', array_pop($lessFileNameExpl));
    $cssFilePath  = sprintf('%s/../assets/css/%s', Yii::app()->basePath, $cssFileName);

    $lessc = new lessc;
    if ($params['force'])
    	$lessc->compileFile($lessFilePath, $cssFilePath);
    else
    	$lessc->checkedCompile($lessFilePath, $cssFilePath);
    return sprintf('%s?%s', $cssFileName, filectime($cssFilePath));
}