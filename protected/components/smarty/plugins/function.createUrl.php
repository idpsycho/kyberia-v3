<?php
/**
 * Allows to generate language prefixed url using createUrl.
 *
 * Syntax:
 * {createUrl route="test"}
 * {link text="test" url="controller/action?param=value"}
 * {link text="test" url="/absolute/url"}
 * {link text="test" url="http://host/absolute/url"}
 *
 * @see CHtml::link().
 *
 * @param array $params
 * @param Smarty_Internal_Template $smarty
 * @return string createUrl output
 * @throws CException
 */
function smarty_function_createUrl($params, Smarty_Internal_Template &$smarty)
{
    // check required params
    if (empty($params['route'])) {
        throw new CException("Function 'route' parameter must be specified.");
    }

    /** @var $controller CController */
    $controller = $smarty->getTemplateVars('this');
    if (!is_object($controller) || !($controller instanceof CController)) {
        $controller = Yii::app()->controller;

        if (!is_object($controller) || !($controller instanceof CController)) {
            throw new CException("Can't get controller object from template. Error.");
        }
    }

    $_route = $params['route'];
    $_params = empty($params['params']) ? array() : $params['params'];
    $_ampersand = empty($params['ampersand']) ? NULL : $params['ampersand'];

    if (is_array($_route)) {
        $_route = implode('/', $_route);
    }

    $output = $controller->createUrl($_route, $_params, $_ampersand);

    if (!empty($params['assign'])) {
        $smarty->assign($params['assign'], $output);
    }
    else {
        return $output;
    }
}