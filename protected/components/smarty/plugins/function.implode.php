<?php

function smarty_function_implode($params, Smarty_Internal_Template &$smarty)
{
    if (!array_key_exists('pieces', $params)) {
        trigger_error("implode: missing 'pieces' parameter", E_USER_WARNING);
        return FALSE;
    }
    elseif (!is_array($params['pieces'])) {
        trigger_error("implode: 'pieces' parameter must be array", E_USER_WARNING);
        return FALSE;
    }

    if (!isset($params['glue'])) {
        $params['glue'] = '';
    }

    $output = implode($params['glue'], $params['pieces']);

    if (!empty($params['assign'])) {
        $smarty->assign($params['assign'], $output);
    }
    else {
        return $output;
    }
}