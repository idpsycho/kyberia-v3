<?php

class UrlManager extends CUrlManager
{
    public $showScriptName = FALSE;
    public $appendParams = FALSE;
    public $useStrictParsing = TRUE;
    public $urlSuffix = '/';

    public function createUrl($route, $params = array(), $ampersand = '&')
    {
        $route = preg_replace_callback('/[A-Z]/', function($matches) {
            return '-' . lcfirst($matches[0]);
        }, $route);

        $route = preg_replace_callback('/([a-zA-Z-]+)/', function($matches) {
            return trim($matches[0], '-');
        }, $route);

        return parent::createUrl($route, $params, $ampersand);
    }

    public function parseUrl($request)
    {
        $route = parent::parseUrl($request);

        return lcfirst(str_replace(' ', '', ucwords(str_replace(array('-', '_'), ' ', $route))));
    }
}