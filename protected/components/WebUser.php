<?php

/**
* @property integer id
* @property string name
*/
class WebUser extends CWebUser
{
	protected $_model = null;

	public function getModel()
	{
		if (!$this->_model)
		{
			if ($this->id) $this->_model = User::model()->findByPk($this->id);
			else $this->_model = User::model();
		}

		return $this->_model;
	}
}
?>
