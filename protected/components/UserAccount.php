<?php

/*
	COPY THIS to start of User.php:

	protected $account_ = null;
	public function getAccount() { return $this->account_ ? : $this->account_ = new UserAccount($this); }
	function beforeSave() { return $this->getAccount()->beforeSave() && parent::beforeSave(); }
	function afterFind() { return $this->getAccount()->afterFind(); }

	USAGE:
	...

*/

class UserAccount
{
	public $user = null;
	protected $origHash = '';
	function __construct($user_)
	{
		$this->user = $user_;
	}
	protected function getHash($password)
	{
		return md5("aerhaergq4tw5yvtw4tvqweegf(*#HF!" . $password);
	}
	public function matchesPassword($password)
	{
		return ($this->getHash($password) == $this->origHash);
	}
	public function getConfirmationHash()
	{
		return substr(md5("sdfdsú÷×ß÷˘^ß".$this->user_id."lulz".$this->email."÷÷÷$ˇ^ľš"), 0, 16);
	}
	public function getConfirmationLink()
	{
		return bu()."/site/verify?x=".$this->getConfirmationHash($this->user_id, $this->email);
	}

	function afterFind()
	{
		$this->origHash = $this->user->password;
		$this->user->password = '';
		return true;
	}
	function beforeSave()
	{
		if (!$this->user) die('wtf');
		if (!$this->user->password)
			$this->user->password = $this->origHash;
		else
			$this->user->password = $this->getHash($this->user->password);

		return true;
	}

}
