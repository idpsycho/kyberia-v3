<?php

class UserIdentity extends CUserIdentity
{
	private $_id;

	public function getId()
	{
		return $this->_id;
	}

	public function authenticate()
	{
		$id = $this->username;
		$pwd = $this->password;

		if (($id*1).'' == $id)
			$user = UserManager::byId($id);
		else
			$user = UserManager::byName($id);

		if ($user && $user->getAccount()->matchesPassword($this->password))
		{
			$this->_id = $user->user_id;
			$this->errorCode = self::ERROR_NONE;
		}
		else
		{
			$this->errorCode = 5;	 //who cares
		}

		return !$this->errorCode;
	}

}
