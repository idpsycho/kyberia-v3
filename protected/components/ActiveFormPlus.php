<?php

class ActiveFormPlus extends CActiveForm
{
	public function labelEx($model, $attribute, $htmlOptions=array())
	{
		if (!$htmlOptions['class']) $htmlOptions['class'] = 'primary';
		return parent::labelEx($model, $attribute, $htmlOptions);
	}
}