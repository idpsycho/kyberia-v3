<?php
class LessToCss
{
	static function check($less)
	{
		require_once "protected/vendors/lessc.inc.php";
		$lessc = new lessc;
		$css = substr($less, 0, strlen($less)-5) . '.css';
		$lessc->checkedCompile($less, $css);
		return $css.'?'.filectime($css);
	}
}


