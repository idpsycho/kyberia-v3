<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public $metadata = array(); // og-image, description and such

	public $contentClass = "page";

	public function renderFile($viewFile,$data=null,$return=false)
	{
		if (!Yii::app()->request->isAjaxRequest && YII_DEBUG)
		{
			$path = str_replace(Yii::app()->basePath, '', $viewFile);
			$class = get_class($this);

			$a = "<!-- ### VIEW: $path in CONTROLLER: $class -->\n";
			if (!$return) echo $a;

			$b = parent::renderFile($viewFile, $data, $return);

			$c = "<!-- ### END OF VIEW: $path -->\n";
			if (!$return) echo $c;

			$keypoint = Performance::htmlKeypoint();
			if (!$return) echo $keypoint;

			if ($return) return $a.$b.$c.$keypoint;
		}

		return parent::renderFile($viewFile, $data, $return);
	}
}