<?php

/**
 * This is the model class for table "neuron".
 *
 * The followings are the available columns in table 'neuron':
 * @property integer $dst
 * @property integer $src
 * @property integer $synapse
 * @property string $link
 * @property string $last_impulse
 * @property string $synapse_created
 * @property string $dst_vector
 * @property integer $synapse_creator
 * @property integer $dst_level3
 */
class Neuron extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'neurons';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('last_impulse', 'required'),
			array('dst, src, synapse, synapse_creator, dst_level3', 'numerical', 'integerOnly'=>true),
			array('link', 'length', 'max'=>8),
			array('dst_vector', 'length', 'max'=>232),
			array('synapse_created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('dst, src, synapse, link, last_impulse, synapse_created, dst_vector, synapse_creator, dst_level3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dst' => 'Dst',
			'src' => 'Src',
			'synapse' => 'Synapse',
			'link' => 'Link',
			'last_impulse' => 'Last Impulse',
			'synapse_created' => 'Synapse Created',
			'dst_vector' => 'Dst Vector',
			'synapse_creator' => 'Synapse Creator',
			'dst_level3' => 'Dst Level3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dst',$this->dst);
		$criteria->compare('src',$this->src);
		$criteria->compare('synapse',$this->synapse);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('last_impulse',$this->last_impulse,true);
		$criteria->compare('synapse_created',$this->synapse_created,true);
		$criteria->compare('dst_vector',$this->dst_vector,true);
		$criteria->compare('synapse_creator',$this->synapse_creator);
		$criteria->compare('dst_level3',$this->dst_level3);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Neuron the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
