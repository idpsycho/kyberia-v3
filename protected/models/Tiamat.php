<?php

/**
 * This is the model class for table "tiamat".
 *
 * The followings are the available columns in table 'tiamat':
 * @property integer $node_id
 * @property string $node_name
 * @property integer $node_parent
 * @property string $node_vector
 * @property string $node_external_access
 * @property string $node_system_access
 * @property integer $node_children_count
 * @property integer $node_creator
 * @property string $node_created
 * @property string $lastchild_created
 * @property integer $k
 * @property integer $node_views
 * @property integer $node_destructor
 * @property string $node_content
 * @property integer $node_descendant_count
 * @property string $lastdescendant_created
 * @property integer $template_id
 * @property string $update_performed
 * @property string $external_link
 */
class Tiamat extends CActiveRecord
{
	function getId_node() { return $this->node_id; }
	function getLink() {
		// 2005-09-21 01:41:54 -> 2005-09-21_01.41.54
		$id = $this->id_node;
		$rev = $this->update_performed;
		$rev = str_replace(' ', '_', $rev);
		return "/id/$id/history?revision=$rev";
	}
	function getAsNode() {
		$node = new Node;
		$node->attributes = $this->attributes;
		return $node;
	}

	function getNext() {
		$ID_NODE = 'node_id';
		return Tiamat::model()->find(array(
			'condition'		=> "$ID_NODE = :n AND update_performed > :r",
			'params'		=> array(':n'=>$this->id_node, ':r'=>$this->update_performed),
			'order'			=> "update_performed",
		));
	}
	function getPrev() {
		$ID_NODE = 'node_id';
		return Tiamat::model()->find(array(
			'condition'		=> "$ID_NODE = :n AND update_performed < :r",
			'params'		=> array(':n'=>$this->id_node, ':r'=>$this->update_performed),
			'order'			=> "update_performed DESC",
		));
	}

	static function getByRevision($id_node, $rev)
	{
		$rev = str_replace('_', ' ', $rev);
		$ID_NODE = 'node_Id';
		$crit = array(
			'condition' => "$ID_NODE = :n",
			'params'	=> array(':n'=>$id_node),
			'order'		=> 'update_performed DESC',	// will get the latest revision if no revision is set
		);
		if ($rev) {
			$crit['condition'] .= ' AND update_performed = :r';
			$crit['params'][':r'] = $rev;
		};
		return Tiamat::model()->find($crit);
	}

    public function primaryKey()
    {
    	$ID_DESTRUCTOR = 'node_destructor';
        return "$ID_DESTRUCTOR";
    }


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tiamat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lastchild_created', 'required'),
			array('node_id, node_parent, node_children_count, node_creator, k, node_views, node_destructor, node_descendant_count, template_id', 'numerical', 'integerOnly'=>true),
			array('node_name', 'length', 'max'=>132),
			array('node_vector', 'length', 'max'=>232),
			array('node_external_access', 'length', 'max'=>3),
			array('node_system_access', 'length', 'max'=>9),
			array('external_link', 'length', 'max'=>123),
			array('node_created, node_content, lastdescendant_created, update_performed', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('node_id, node_name, node_parent, node_vector, node_external_access, node_system_access, node_children_count, node_creator, node_created, lastchild_created, k, node_views, node_destructor, node_content, node_descendant_count, lastdescendant_created, template_id, update_performed, external_link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		$ID_DESTRUCTOR = 'node_destructor';
		return array(
			'destructor'		=> array(self::BELONGS_TO, 'User', $ID_DESTRUCTOR),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'node_id' => 'Node',
			'node_name' => 'Node Name',
			'node_parent' => 'Node Parent',
			'node_vector' => 'Node Vector',
			'node_external_access' => 'Node External Access',
			'node_system_access' => 'Node System Access',
			'node_children_count' => 'Node Children Count',
			'node_creator' => 'Node Creator',
			'node_created' => 'Node Created',
			'lastchild_created' => 'Lastchild Created',
			'k' => 'K',
			'node_views' => 'Node Views',
			'node_destructor' => 'Node Destructor',
			'node_content' => 'Node Content',
			'node_descendant_count' => 'Node Descendant Count',
			'lastdescendant_created' => 'Lastdescendant Created',
			'template_id' => 'Template',
			'update_performed' => 'Update Performed',
			'external_link' => 'External Link',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('node_id',$this->node_id);
		$criteria->compare('node_name',$this->node_name,true);
		$criteria->compare('node_parent',$this->node_parent);
		$criteria->compare('node_vector',$this->node_vector,true);
		$criteria->compare('node_external_access',$this->node_external_access,true);
		$criteria->compare('node_system_access',$this->node_system_access,true);
		$criteria->compare('node_children_count',$this->node_children_count);
		$criteria->compare('node_creator',$this->node_creator);
		$criteria->compare('node_created',$this->node_created,true);
		$criteria->compare('lastchild_created',$this->lastchild_created,true);
		$criteria->compare('k',$this->k);
		$criteria->compare('node_views',$this->node_views);
		$criteria->compare('node_destructor',$this->node_destructor);
		$criteria->compare('node_content',$this->node_content,true);
		$criteria->compare('node_descendant_count',$this->node_descendant_count);
		$criteria->compare('lastdescendant_created',$this->lastdescendant_created,true);
		$criteria->compare('template_id',$this->template_id);
		$criteria->compare('update_performed',$this->update_performed,true);
		$criteria->compare('external_link',$this->external_link,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tiamat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
