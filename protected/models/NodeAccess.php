<?php

/**
 * This is the model class for table "node_access".
 *
 * The followings are the available columns in table 'node_access':
 * @property integer $node_id
 * @property integer $user_id
 * @property string $node_bookmark
 * @property string $node_permission
 * @property integer $node_user_subchild_count
 * @property string $last_visit
 * @property integer $visits
 * @property integer $bookmark_category
 * @property string $given_k
 */
class NodeAccess extends ActiveRecord
{
	public $keep;	// this is used when updating permission list

	/////////////////////////////
	// compatibility layer
	function getId_node()				{ return $this->node_id; }
	function setId_node($x)				{ $this->node_id = $x; }
	function getId_user()				{ return $this->user_id; }
	function setId_user($x)				{ $this->user_id = $x; }
	function getPermission()			{ return $this->node_permission; }
	function setPermission($x)			{ $this->node_permission = $x; }
	function getBookmarked()			{ return $this->node_bookmark; }
	function setBookmarked($x)			{ $this->node_bookmark = $x; }
	function getUnread_children()		{ return $this->node_user_subchild_count; }
	function setUnread_children($x)		{ $this->node_user_subchild_count = $x; }

	function getAttributes_MAP() { return NodeAccess::$attributes_MAP; }
	static $attributes_MAP = array(
		'id_node'				=> 'node_id',
		'id_user'				=> 'user_id',
		'bookmarked'			=> 'node_bookmark',
		'permission'			=> 'node_permission',
		'unread_children'		=> 'node_user_subchild_count',
		'last_visit'			=> 'last_visit',
		'visits'				=> 'visits',
		'bookmark_category'		=> 'bookmark_category',
		'given_k'				=> 'given_k',
	);
	//////////////////////////////////

	static function find_($node, $user)
	{
		$ID_NODE = 'node_id';
		$ID_USER = 'user_id';
		return NodeAccess::model()->find("$ID_NODE = :n AND $ID_USER = :u", array(
			':n'=>$node->id, ':u'=>$user->id
		));
	}

	static function findOrCreate($node, $user)
	{
		$na = NodeAccess::find_($node, $user);
		if (!$na) {
			$na = new NodeAccess;
			$na->id_node = $node->id;
			$na->id_user = $user->id;
		}
		return $na;
	}

	/////////////////////////////////////////////

	function getHasNewDescs()
	{
		return strtotime($this->last_visit) < strtotime($this->node->lastdescendant_created);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'node_access';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('node_id, user_id, node_user_subchild_count, visits, bookmark_category', 'numerical', 'integerOnly'=>true),
			array('node_bookmark, given_k', 'length', 'max'=>3),
			array('node_permission', 'length', 'max'=>7),
			array('last_visit', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('node_id, user_id, node_bookmark, node_permission, node_user_subchild_count, last_visit, visits, bookmark_category, given_k', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		$ID_NODE = 'node_id';
		$ID_USER = 'user_id';
		return array(
			'node'	=> array(self::BELONGS_TO, 'Node', "$ID_NODE"),
			'user'	=> array(self::BELONGS_TO, 'User', "$ID_USER"),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'node_id' => 'Node',
			'user_id' => 'User',
			'node_bookmark' => 'Node Bookmark',
			'node_permission' => 'Node Permission',
			'node_user_subchild_count' => 'Node User Subchild Count',
			'last_visit' => 'Last Visit',
			'visits' => 'Visits',
			'bookmark_category' => 'Bookmark Category',
			'given_k' => 'Given K',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('node_id',$this->node_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('node_bookmark',$this->node_bookmark,true);
		$criteria->compare('node_permission',$this->node_permission,true);
		$criteria->compare('node_user_subchild_count',$this->node_user_subchild_count);
		$criteria->compare('last_visit',$this->last_visit,true);
		$criteria->compare('visits',$this->visits);
		$criteria->compare('bookmark_category',$this->bookmark_category);
		$criteria->compare('given_k',$this->given_k,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NodeAccess the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}