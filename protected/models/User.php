<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $user_id
 * @property string $login
 * @property string $password
 * @property string $password_old
 * @property string $email
 * @property string $last_action
 * @property string $user_action
 * @property integer $user_action_id
 * @property integer $user_mail
 * @property integer $user_mail_id
 * @property integer $listing_amount
 * @property string $session_string
 * @property string $user_k
 * @property string $user_location_vector
 * @property integer $ldap_id
 * @property string $listing_order
 * @property integer $header_id
 * @property string $cube_vector
 * @property integer $k_wallet
 * @property string $hash
 * @property string $acc_lockout
 * @property string $moods
 * @property string $invisible
 * @property integer $password_change_period
 * @property integer $login_retry
 * @property string $date_last_login
 * @property string $date_password_changed
 * @property string $date_login_failed
 * @property integer $mail_notify
 * @property integer $bookstyle
 * @property string $user_setting_metadata
 */
class User extends ActiveRecord
{
	public $totalUsers;

	/////////////////////////////
	// compatibility layer
	function getId()				{ return $this->user_id; }
	function getId_location()		{ return $this->user_action_id; }
	function setId_location($x)		{ $this->user_action_id = $x; }
	function getLast_activity()		{ return $this->last_action; }
	function setLast_activity($x)	{ $this->last_action = $x; }

	function getAttributes_MAP() { return User::$attributes_MAP; }
	static $attributes_MAP = array(
		'id'				=> 'user_id',
		'login'				=> 'login',
		'password'			=> 'password',
		//'password_old'		=> 'password_old',
		'email'				=> 'email',
		'last_activity'		=> 'last_action',
		//'user_action'		=> 'user_action',
		'id_location'		=> 'user_action_id',
		//'user_mail'			=> 'user_mail',
		//'user_mail_id'		=> 'user_mail_id',
		//'listing_amount'	=> 'listing_amount',
		//'session_string'	=> 'session_string',
		//'user_k'			=> 'user_k',
		'location_vector'	=> 'user_location_vector',
		//'ldap_id'			=> 'ldap_id',
		//'listing_order'		=> 'listing_order',
		//'header_id'			=> 'header_id',
		//'cube_vector'		=> 'cube_vector',
		//'k_wallet'			=> 'k_wallet',
		//'hash'				=> 'hash',
		//'acc_lockout'		=> 'acc_lockout',
		//'moods'				=> 'moods',
		//'invisible'			=> 'invisible',
		//'password_change_period'=> 'password_change_period',
		//'login_retry'		=> 'login_retry',
		//'date_last_login'	=> 'date_last_login',
		//'date_password_changed'=> 'date_password_changed',
		//'date_login_failed'	=> 'date_login_failed',
		//'mail_notify'		=> 'mail_notify',
		//'bookstyle'			=> 'bookstyle',
		//'user_setting_metadata'=> 'user_setting_metadata',
	);
	///////////////////////////////////

	/////////////////////////
	// toto riesi zmenu hesla a tusim prihlasovanie a tak..
	protected $account_ = null;
	public function getAccount() { return $this->account_ ? : $this->account_ = new UserAccount($this); }
	function beforeSave() { return $this->getAccount()->beforeSave() && parent::beforeSave(); }
	function afterFind() { return $this->getAccount()->afterFind(); }
	///////////////////////////////


	function beforeFind() { Performance::tick(__METHOD__); return true; }


	function canWrite_slow($node) { return true; }
	function canWrite($node) { return true; }
	function canEdit($node)	{ return true; }
	function canRead($node)	{ return true; }

	function getAvatar()	{ $id = $this->id ? : 0; return "/kyb-data/images/nodes/$id.gif"; }
	function getLink()		{ return "/id/{$this->id}"; }
	function getIsGuest()	{ return !$this->id; }
	function getIsAdmin()	{ return Yii::app()->params['admins'][$this->id]; }
	function getCanCode()	{ return $this->isAdmin || Yii::app()->params['coders'][$this->id]; }

	function hasGivenK($node) {
		$ID_USER = 'user_id';
		$ID_NODE = 'node_id';
		return NodeAccess::model()->count("given_k = 'yes' AND $ID_USER = :u AND $ID_NODE = :n", array(
			':u'=>$this->id,
			':n'=>$node->id,
		));
	}

	/*	$ID_USER = 'user_id';
		$ID_NODE = 'node_id';
		$BOOKMARKED = 'node_bookmark';
		return NodeAccess::model()->count("$BOOKMARKED = 'yes' AND $ID_USER = :u AND $ID_NODE = :n", array(
			':u'=>$this->id,
			':n'=>$node->id,
		));
	*/
	function hasFriended($node) {
		return UserRelation::find_($node, $this, UserRelation::$FRIEND);
	}
	function hasBooked($node) {
		return UserRelation::find_($node, $this, UserRelation::$BOOK);
	}
	function hasFooked($node) {
		return UserRelation::find_($node, $this, UserRelation::$FOOK);
	}
	function hasBlockedMail($node) {
		return UserRelation::find_($node, $this, UserRelation::$BLOCK_MAIL);
	}

	function tpl($prop, $params=null)
	{
		if (!is_array($params)) $params = array();
		$params['user'] = user();
		$params['u'] = $this;
		Yii::app()->controller->renderPartial("/user/_$prop", $params);
	}



	////////////////////////////////////////////

	function getBookedNodes()
	{
		$ID_USER = 'user_id';
		$NAME = 'node_name';
		$BOOKMARKED = 'node_bookmark';

		Performance::start(__METHOD__);
		$booked = NodeAccess::model()->with('node')->findAll(array(
			'condition'=>"t.$ID_USER = :u AND $BOOKMARKED = 'yes'",
			'params'=>array(':u'=>$this->id),
			'order'=>"node.$NAME",
		));
		Performance::end(__METHOD__);
		return $booked;
		$nodes = array();
		foreach ($booked as $b)
			$nodes[] = $b->node;
		return $nodes;
	}

	function getLastActiveAgo()
	{
		$dt = time() - strtotime($this->last_activity);
		$min = floor($dt/60);
		$sec = $dt-$min*60;
		return "{$min}m {$sec}s";
	}

	function getLastMailedUserName()
	{
		$lastMailed = Mail::model()->find(array(
			'condition'=>'mail_from = :u',
			'params'=>array(':u'=>$this->id),
			'order'=>'mail_id desc',
		));
		return UserManager::byId($lastMailed->mail_to)->login;
	}

	function getNumUnreadMails() {
		$ID_TO = 'mail_to';
		$ID_USER = 'mail_user';
		$HAS_READ = 'mail_read';
		return Mail::model()->count(array(
			'select'=>"$ID_TO",
			'condition'=>"$ID_TO = :u AND $ID_USER = :u AND $HAS_READ != 1",
			'params'=>array(':u'=>$this->id),
		));
	}

	function getUnreadSenders() {
		$ID_TO = 'mail_to';
		$ID_FROM = 'mail_from';
		$ID_USER = 'mail_user';
		$HAS_READ = 'mail_read';
		$mails = Mail::model()->findAll(array(
			'condition'=>"$ID_TO = :u AND $ID_USER = :u AND $HAS_READ != 1",
			'params'=>array(':u'=>$this->id),
			'group'=>"$ID_FROM",
			'limit'=>4,
		));
		$senders = array();
		foreach ($mails as $m)
			$senders[] = $m->from;
		return $senders;
	}

	function getUserSubmissions() {
		$ID_USER = 't.node_creator';
		$ID = 't.node_id';

		$crit = array(
			'condition'=>"$ID_USER = :u",
			'params'=>array(':u'=>$this->id),
			'order'=>"$ID desc",
			'limit'=>100,
		);

		Node::get_FILTER($crit);
		return Node::model()->with('user')->findAll($crit);
	}
	function getUserReactions() {
		$ID_USER = 't.node_creator';
		$ID_PARENT_USER = 't.parent_node_creator';
		$ID = 't.node_id';

		$crit = array(
			'condition'=>"$ID_PARENT_USER = :u",
			'params'=>array(':u'=>$this->id),
			'order'=>"$ID desc",
			'limit'=>100,
		);

		Node::get_FILTER($crit);
		return Node::model()->with('user')->findAll($crit);
	}
	function getUserMovement() {
		return NodeAccess::model()->with('node')->findAll(array(
			'condition'=>'t.user_id = :u AND last_visit IS NOT NULL',
			'params'=>array(':u'=>$this->id),
			'order'=>'last_visit desc',
			'limit'=>100,
		));
	}
	function getActivePeople() {
		$ID_LOCATION = 'user_action_id';
		$LAST_ACTIVITY = 'last_action';

		return User::model()->with('location')->findAll(array(
			'condition'=>"$ID_LOCATION IS NOT NULL AND $LAST_ACTIVITY > NOW() - 30*60",
			'order'=>"$LAST_ACTIVITY desc",
			'limit'=>1000,
		));
	}

	function getFriendsOnline() { return UserRelation::myFriendsOnline(); }

	// najprv len taketo hacky riesenie
	static function getMail_FILTER(&$crit) {
		$ID_FROM = 'mail_from';
		$ID_TO = 'mail_to';
		$MESSAGE = 'mail_text';

		if ($_GET['user']) {
			$id_user = UserManager::idOf( $_GET['user'] );
			$crit['condition'] = "(".$crit['condition'].") AND ($ID_FROM = :filter OR $ID_TO = :filter)";
			$crit['params'][':filter'] = $id_user;
		}

		if ($_GET['search']) {
			$crit['condition'] = "(".$crit['condition'].") AND ($MESSAGE LIKE :filter)";
			$crit['params'][':filter'] = '%'.$_GET['search'].'%';
		}
	}

	function getMails() {
		$ID = 'mail_id';
		$ID_USER = 'mail_user';
		$ID_FROM = 'mail_from';
		$ID_TO = 'mail_to';
		$MESSAGE = 'mail_text';

		$crit = array(
			'condition'=>"$ID_USER = :u",
			'params'=>array(':u'=>$this->id),
			'limit'=>100,
			'order'=>"$ID desc",
		);

		User::getMail_FILTER($crit);

		return Mail::model()->with('from', 'to')->findAll($crit);
	}
	function getTopK() {
		$ID_USER = 'node_creator';

		$crit = array(
			'condition'=>"t.$ID_USER = :u AND k > 0",
			'params'=>array(':u'=>$this->id),
			'order'=>'k desc',
			'limit'=>100,
		);

		Node::get_FILTER($crit);
		return Node::model()->with('user')->findAll($crit);
	}
	function getGivenK() {
		$ID_USER = 'user_id';
		$givenk = NodeAccess::model()->with('node', 'node.user')->findAll(array(
			'condition'=>"given_k = 'yes' AND t.$ID_USER = :u",
			'params'=>array(':u'=>$this->id),
			'order'=>'last_visit desc',
			'limit'=>100,
			));

		return $givenk;
	}

	function getMostPopulatedNodes() {
		$ID_LOCATION = 'user_action_id';
		return User::model()->with('location')->findAll(array(
			'select'=>"$ID_LOCATION, count(1) as totalUsers",
			'condition'=>"$ID_LOCATION > 0",
			'group'=>"$ID_LOCATION",
			'order'=>'totalUsers desc',
			'limit'=>100,
		));
	}


	/*function getNumHasFriended() {
		return UserRelation::numHasFriended($this->id);
	}
	function getNumHasFriended() {
		return UserRelation::numHasFriended($this->id);
	}*/
	function getMutualFriendsWith($id_other) {
		if ($this->isGuest) return array();
		return UserRelation::mutualFriends($this->id, $id_other);
	}





	function getPerformance()
	{
		return new Performance;	// aby som sa dostal k performance hodnotam..
	}








	/////////////////////////////////////

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('last_action, user_k, date_password_changed, date_login_failed', 'required'),
			array('user_id, user_action_id, user_mail, user_mail_id, listing_amount, header_id, k_wallet, password_change_period, login_retry, mail_notify, bookstyle', 'numerical', 'integerOnly'=>true),
			array('login', 'length', 'max'=>66),
			array('password, password_old', 'length', 'max'=>230),
			array('email', 'length', 'max'=>50),
			array('user_action', 'length', 'max'=>75),
			array('session_string', 'length', 'max'=>64),
			array('user_k', 'length', 'max'=>10),
			array('user_location_vector, moods', 'length', 'max'=>123),
			array('listing_order', 'length', 'max'=>4),
			array('cube_vector, hash', 'length', 'max'=>23),
			array('invisible', 'length', 'max'=>3),
			array('acc_lockout, date_last_login', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, login, password, password_old, email, last_action, user_action, user_action_id, user_mail, user_mail_id, listing_amount, session_string, user_k, user_location_vector, ldap_id, listing_order, header_id, cube_vector, k_wallet, hash, acc_lockout, moods, invisible, password_change_period, login_retry, date_last_login, date_password_changed, date_login_failed, mail_notify, bookstyle, user_setting_metadata', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		$ID_LOCATION = 'user_action_id';
		return array(
			'node'		=> array(self::HAS_ONE, 'Node', 'node_id'),
			'location'	=> array(self::BELONGS_TO, 'Node', "$ID_LOCATION"),
			//'template'	=> array(self::BELONGS_TO, 'Node', 'id_location_template'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'login' => 'Login',
			'password' => 'Password',
			'password_old' => 'Password Old',
			'email' => 'Email',
			'last_action' => 'Last Action',
			'user_action' => 'User Action',
			'user_action_id' => 'User Action',
			'user_mail' => 'User Mail',
			'user_mail_id' => 'User Mail',
			'listing_amount' => 'Listing Amount',
			'session_string' => 'Session String',
			'user_k' => 'User K',
			'user_location_vector' => 'User Location Vector',
			'ldap_id' => 'Ldap',
			'listing_order' => 'Listing Order',
			'header_id' => 'Header',
			'cube_vector' => 'Cube Vector',
			'k_wallet' => 'K Wallet',
			'hash' => 'Hash',
			'acc_lockout' => 'Acc Lockout',
			'moods' => 'Moods',
			'invisible' => 'Invisible',
			'password_change_period' => 'Password Change Period',
			'login_retry' => 'Login Retry',
			'date_last_login' => 'Date Last Login',
			'date_password_changed' => 'Date Password Changed',
			'date_login_failed' => 'Date Login Failed',
			'mail_notify' => 'Mail Notify',
			'bookstyle' => 'Bookstyle',
			'user_setting_metadata' => 'User Setting Metadata',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('password_old',$this->password_old,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('last_action',$this->last_action,true);
		$criteria->compare('user_action',$this->user_action,true);
		$criteria->compare('user_action_id',$this->user_action_id);
		$criteria->compare('user_mail',$this->user_mail);
		$criteria->compare('user_mail_id',$this->user_mail_id);
		$criteria->compare('listing_amount',$this->listing_amount);
		$criteria->compare('session_string',$this->session_string,true);
		$criteria->compare('user_k',$this->user_k,true);
		$criteria->compare('user_location_vector',$this->user_location_vector,true);
		$criteria->compare('ldap_id',$this->ldap_id);
		$criteria->compare('listing_order',$this->listing_order,true);
		$criteria->compare('header_id',$this->header_id);
		$criteria->compare('cube_vector',$this->cube_vector,true);
		$criteria->compare('k_wallet',$this->k_wallet);
		$criteria->compare('hash',$this->hash,true);
		$criteria->compare('acc_lockout',$this->acc_lockout,true);
		$criteria->compare('moods',$this->moods,true);
		$criteria->compare('invisible',$this->invisible,true);
		$criteria->compare('password_change_period',$this->password_change_period);
		$criteria->compare('login_retry',$this->login_retry);
		$criteria->compare('date_last_login',$this->date_last_login,true);
		$criteria->compare('date_password_changed',$this->date_password_changed,true);
		$criteria->compare('date_login_failed',$this->date_login_failed,true);
		$criteria->compare('mail_notify',$this->mail_notify);
		$criteria->compare('bookstyle',$this->bookstyle);
		$criteria->compare('user_setting_metadata',$this->user_setting_metadata,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
