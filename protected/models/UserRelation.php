<?php

/**
 * This is the model class for table "user_relation".
 *
 * The followings are the available columns in table 'user_relation':
 * @property integer $user_relation_id
 * @property integer $user_id
 * @property integer $node_id
 * @property integer $relation_type
 */
class UserRelation extends ActiveRecord
{
	static $FRIEND = 2;
	static $BOOK = 1;
	static $FOOK = -1;
	static $BLOCK_MAIL = -3;

	/////////////////////////////
	// compatibility layer
	function getId_node()				{ return $this->node_id; }
	function setId_node($x)				{ $this->node_id = $x; }
	function getId_user()				{ return $this->user_id; }
	function setId_user($x)				{ $this->user_id = $x; }
	function getType()					{ return $this->relation_type; }
	function setType($x)				{ $this->relation_type = $x; }

	function getAttributes_MAP()		{ return UserRelation::$attributes_MAP; }
	static $attributes_MAP = array(
		'id'					=> 'user_relation_id',
		'id_user'				=> 'user_id',
		'id_node'				=> 'node_id',
		'type'					=> 'relation_type',
	);
	//////////////////////////////////


	function beforeFind() { Performance::tick(__METHOD__); return true; }



	static function find_byIds($id_node, $id_user, $type)
	{
		$ID_NODE = 'node_id';
		$ID_USER = 'user_id';
		$TYPE = 'relation_type';
		return UserRelation::model()->find("$ID_NODE = :n AND $ID_USER = :u AND $TYPE = :t", array(
			':n'=>$id_node, ':u'=>$id_user, ':t'=>$type,
		));
	}
	static function find_($node, $user, $type)
	{
		return UserRelation::find_byIds($node->id, $user->id, $type);
	}
	static function findOrCreate($node, $user, $type)
	{
		$ur = UserRelation::find_($node, $user, $type);
		if (!$ur) {
			$ur = new UserRelation;
			$ur->id_node = $node->id;
			$ur->id_user = $user->id;
			$ur->type = $type;
		}
		return $ur;
	}

	/////////////////////////////////////////////

	/*
	static function numHasFriended($id_friended)
	{
		$ID_NODE = 'node_id';
		$TYPE = 'relation_type';
		return UserRelation::model()->count("$ID_NODE = :n AND $TYPE = :t",
				array(':n'=>$id_friended, ':t'=>UserRelation::$FRIEND));
	}
	static function numFriendedBy($id_friended)
	{
		$ID_NODE = 'node_id';
		$TYPE = 'relation_type';
		return UserRelation::model()->count("$ID_NODE = :n AND $TYPE = :t",
				array(':n'=>$id_friended, ':t'=>UserRelation::$FRIEND));
	}
	*/

	// kolko ludi si pridal za friendov, ktorych som si aj ja pridal za friendov
	// cize selectnem vsetky odchadzajuce friendnutia z NEHO a MNA
	// groupnem podla cieloveho, countnem pocet, a having count = 2, a to cele countnem
	/*
	static function numMutualFriends($id_with)
	{
		$user = user();
		if ($user->isGuest) return 0;

		$ID_NODE = 'node_id';
		$ID_USER = 'user_id';
		$TYPE = 'relation_type';
		return UserRelation::model()->count(array(
			'select'	=> 'count(1) as mutual',
			'condition'	=> "($ID_USER = :u1 OR $ID_USER = :u2) AND $TYPE = :t",
			'params'	=> array(':u1'=>$id_with, ':u2'=>$user->id, ':t'=>UserRelation::$FRIEND),
			'group'		=> "$ID_NODE",
			'having'	=> 'mutual = 2',
		));
	}*/

	static function mutualFriends($id_user1, $id_user2)
	{
		$ID_NODE = 'node_id';
		$ID_USER = 'user_id';
		$TYPE = 'relation_type';

		$relations = UserRelation::model()->with('friend')->findAll(array(
			'select'	=> "count(1) as mutual, $ID_NODE",
			'condition'	=> "(t.$ID_USER = :u1 OR t.$ID_USER = :u2) AND $TYPE = :t",
			'params'	=> array(':u1'=>$id_user1, ':u2'=>$id_user2, ':t'=>UserRelation::$FRIEND),
			'group'		=> "$ID_NODE",
			'having'	=> 'mutual = 2',
		));
		//error($relations);
		$friends = array();
		foreach ($relations as $r)
			$friends[] = $r->friend;
		return $friends;
	}

	static function myFriendsOnline()
	{
		$user = user();
		if ($user->isGuest) return array();

		$ID_NODE = 'node_id';
		$ID_USER = 'user_id';
		$TYPE = 'relation_type';
		$LAST_ACTIVITY = 'last_action';

		$relations = UserRelation::model()->with('friend')->with('friend.location')->findAll(array(
			'condition'	=> "t.$ID_USER = :u AND $TYPE = :t AND $LAST_ACTIVITY > NOW() - 30*60",
			'params'	=> array(':u'=>$user->id, ':t'=>UserRelation::$FRIEND),
			'order'		=> "$LAST_ACTIVITY desc",
		));

		$friends = array();
		foreach ($relations as $r)
			$friends[] = $r->friend;
		return $friends;
	}









	/////////////////////////////////////////////

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_relation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, node_id, relation_type', 'required'),
			array('user_id, node_id, relation_type', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_relation_id, user_id, node_id, relation_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		$ID_NODE = 'node_id';
		return array(
			'friend'		=> array(self::BELONGS_TO, 'User', $ID_NODE),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_relation_id' => 'User Relation',
			'user_id' => 'User',
			'node_id' => 'Node',
			'relation_type' => 'Relation Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_relation_id',$this->user_relation_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('node_id',$this->node_id);
		$criteria->compare('relation_type',$this->relation_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserRelation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
