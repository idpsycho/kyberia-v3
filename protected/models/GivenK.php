<?php

/**
 * This is the model class for table "i".
 *
 * The followings are the available columns in table 'i':
 * @property integer $node_id
 * @property string $cas
 */
class GivenK extends CActiveRecord
{

	/////////////////////////////
	// compatibility layer
	function getId_node()			{ return $this->node_id; }
	function setId_node($x)			{ $this->node_id = $x; }
	function getCreated()			{ return $this->cas; }
	function setCreated($x)			{ $this->cas = $x; }

	function getAttributes_MAP() { return Node::$attributes_MAP; }
	static $attributes_MAP = array(
		'id_node'			=> 'node_id',
		'created'			=> 'cas',
	);
	//////////////////////////////////

	static function findOrCreate($node)
	{
		$ID_NODE = 'node_id';
		$g = GivenK::model()->find("$ID_NODE = :n", array(':n'=>$node->id));
		if (!$g) {
			$g = new GivenK;
			$g->id_node = $node->id;
		}
		return $g;
	}



    public function primaryKey()
    {
    	$ID_NODE = 'node_id';
        return "$ID_NODE";
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'i';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cas', 'required'),
			array('node_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('node_id, cas', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		$ID_NODE = 'node_id';
		return array(
			'node'	=> array(self::BELONGS_TO, 'Node', "$ID_NODE"),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'node_id' => 'Node',
			'cas' => 'Cas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('node_id',$this->node_id);
		$criteria->compare('cas',$this->cas,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return I the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
