<?php

class NodeManager
{

	static function byId($id)
	{
		$ID = 'node_id';
		return Node::model()->find("$ID=:n", array(':n'=>$id));
	}

	static function idOf($id_or_name)
	{
		if (!$id_or_name)
			return $default;

		if (is_numeric($id_or_name))
			return NodeManager::byId($id_or_name)->id;
		else
			return NodeManager::byName($id_or_name)->id;
	}

	static function newChildName($node)
	{
		// Re[23]: parent-name
		if ($node->template_id != '3')
			return NodeManager::_nextReName($node->name);

		// 09.08.2012 - 18:27:15
		return date('d.m.Y - H:i:s');
	}


	static function _nextReName($name)
	{
		$lvl = 1;
		if (preg_match('/^re(?:\[(\d+)\])?: (.*)/i', $name, $m) && $m[2])
		{
			$lvl = $m[1] ? $m[1]+1 : 2;
			$name = $m[2];
		}
		$lvl = $lvl>1 ? "[$lvl]" : '';
		return "Re$lvl: $name";
	}

	static function padForVector($id) { return str_pad($id, 8, "0", STR_PAD_LEFT); }
	static function calcVector($node)
	{
		$parent = NodeManager::byId($node->id_parent);
		return $parent->vector . NodeManager::padForVector($node->id);
	}
	static function calcVector_iterate_ancestors($node)
	{
		$vector = '';
		$seen = array();
		while ($node)
		{
			$padded_id = NodeManager::padForVector($node->id);
			$vector = $padded_id . $vector;
			$id_p = $node->id_parent;
			if (!$id_p || $seen[$id_p])
				break;

			$seen[$id_p] = 1;
			$node = NodeManager::byId($id_p);
		}
		return $vector;
	}

/*
	public $totalUsers = 0;
	public $recentK = 0;

	// static
	static function byName($name)
	{
		return Node::model()->find('name=:n', array(':n'=>$name));
	}


	function getUsersByPermission($permission) {
		$arr = Permission::model()->with('user')->findAll(
							'id_node = :n AND permission = :p',
							array(':n'=>$this->id, ':p'=>$permission));
		$users = array();
		foreach ($arr as $i=>$a)
			$users[] = $a->user;
		return $users;
	}
	function getNamesByPermission($permission) {
		$users = $this->getUsersByPermission($permission);
		$names = '';
		foreach ($users as $i=>$u)
			$names .= ($i?';':'').$u->login;
		return $names;
	}
	function getNamesMasters()	{ return $this->getNamesByPermission('master'); }
	function getNamesSilenced()	{ return $this->getNamesByPermission('silence'); }
	function getNamesAccessors(){ return $this->getNamesByPermission('access'); }

	function getMasters()	{ return $this->getUsersByPermission('master'); }
	function getSilenced()	{ return $this->getUsersByPermission('silence'); }
	function getAccessors()	{ return $this->getUsersByPermission('access'); }


	function getPermission() { return true; }
	function getCanWrite() { return false; }
	function canWrite($user) {
		// todo, check permissions...
		return true;
	}

	function getUnreadChildren()
	{
		$v = Visits::model()->find('id_user = :u AND id_node = :n', array(':u'=>user()->id, ':n'=>$this->id));
		return $v->unread_children;
	}
	function getLastUserVisit($user)
	{
		$v = Visits::model()->find('id_user = :u AND id_node = :n', array(':u'=>$user->id, ':n'=>$this->id));
		return $v->last;
	}
	function getHasNewDescs()
	{
		$lastUserVisit = $this->getLastUserVisit(user());
		return $this->lastdesc_created > $lastUserVisit;
	}
	function smarty($tpl)	{ return smartyFetch($tpl, array('node'=>$this)); }
	function tpl($prop)		{ return $this->smarty("node/_$prop.tpl"); }

	function getVisitors() {
		$ids = "1010 1027 1097 1106 1112 1122 1202 1213 125 1254 127 1350 1351 1369 1376 1377 1383 140 1487 15 150 1539 1540 1554 1614 1621 1638 1641 1673 1717 1728 1749 181 1813 1851 1876 1935 1941 1946 1955 199 2040 2111 2184 2259 2283 229 2306 2362 2370 2376 239 2403 2408 2426 2448 2500 2506 251 2538 2550 258 263 265 2687 2697 2710 2784 2812 283 2845 2856 2867 2919 2925 2947 3013 3018 305";
		$ids = explode(' ', $ids);

		$arr = array();
		for ($i=0; $i < 8; $i++)
			$arr[] = $ids[ rand()%count($ids) ];
		return $arr;
	}

	function nodeByName($name)	{ return Node::byName($name); }

	function getAsUser()	{ return User::model()->findByPk($this->id); }

	// is - value getters
	function getIsBook()	{ return Tag::find_('book', $this->id, user()->id); }
	function getIsFook()	{ return Tag::find_('fook', $this->id, user()->id); }
	function getIsK()		{ return Tag::find_('k',	$this->id, user()->id); }
	function getLink()		{ return cu('site/id', array('id_node'=>$this->id)); }
	function getIsKRecent()	{
		$tag = Tag::find_('k', $this->id, user()->id);
		return $tag && $tag->secsAgo() < 60;
	}
	function getContentOrFile() {
		$s = '';
		if ($this->content)
			$s = $this->content;
		else if ($this->external_link)
			$s = @file_get_contents('protected/k_templates/'.$this->external_link);

		if ($this->nl2br)
			$s = nl2br($s);//preg_replace("/[\n]/", "<br>\n", $s);

		return $s;
	}


	function getIsUserNode() { return User::byId($this->id); }
	function getUserAvatar() { return User::avatarOfId($this->id_user); }




	////////////////////////////////////////////////////////////
	// gii generated stuff below - only add to new columns to rules + relations
	public function rules()
	{
		return array(
			array('id_user, id_hardlinked, id_parent, id_template, net_access, nl2br, code, k, views, children_count', 'numerical', 'integerOnly'=>true),
			array('name, external_link', 'length', 'max'=>140),
			array('access', 'length', 'max'=>30),
			array('vector', 'length', 'max'=>400),
			array('content, url_name, created, updated, lastchild_created, lastdesc_created', 'safe'),
		);
	}
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'template'	=> array(self::BELONGS_TO, 'Node', 'id_template'),
			'parent'	=> array(self::BELONGS_TO, 'Node', 'id_parent'),
			'hardlinked'=> array(self::BELONGS_TO, 'Node', 'id_hardlinked'),
			'user'		=> array(self::BELONGS_TO, 'User', 'id_user'),
			'children'	=> array(self::HAS_MANY, 'Node', 'id_parent',
								'order'=>'id desc'),
			'bookeds'	=> array(self::HAS_MANY, 'Tag', 'id_node',
								'condition'=>'bookeds.tag="book"'),
		);
	}
	public function tableName() { return 'node'; }
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
*/
}
