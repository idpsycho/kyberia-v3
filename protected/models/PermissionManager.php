<?php

class PermissionManager
{
	/*
	public $keep;	// used when updating permission list

	static function toRead($node, $user)
	{
		$net = $node->net_access;
		$a = $node->access ? : 'public';
		$p = $user->permissionIn($node);
		$loggedIn = !$user->isGuest;

		if ($node->id_user == $user->id)
			return true;

		if ($a == 'public' || $a == 'moderated')
			return ($net || $loggedIn);

		if ($a == 'private')
			return ($p=='access' || $p=='master');

		return false;
	}
	static function toModerate_recursive($node, $user)
	{
		while ($node)
		{
			if (Permission::toEdit($node, $user))
				return true;

			$node = $node->parent;
		}
		return false;
	}
	static function toWrite_recursive($node, $user)
	{
		while ($node)
		{
			if (!Permission::toWrite($node, $user))
				return false;

			$node = $node->parent;
		};
		return true;
	}
	static function toWrite($node, $user)
	{
		$net = $node->net_access;
		$a = $node->access ? : 'public';
		$p = $user->permissionIn($node);

		if ($user->isGuest) return false;

		if ($node->id_user == $user->id)
			return true;

		if ($a == 'public')
			return $p != 'silence';

		if ($a == 'moderated')
			return ($p == 'access' || $p == 'master');

		if ($a == 'private')
			return ($p == 'access' || $p == 'master');

		return false;
	}
	static function toEdit($node, $user)
	{
		if ($user->isGuest) return false;

		if ($node->id_user == $user->id)
			return true;

		$p = $user->permissionIn($node);

		if ($p == 'master') return true;
		return false;
	}
	static function isOwner($node, $user)
	{
		if ($user->isGuest) return false;
		return $node->id_user == $user->id;
	}



	public function rules()
	{
		return array(
			array('id_user, id_node', 'numerical', 'integerOnly'=>true),
			array('permission', 'length', 'max'=>7),
		);
	}

	public function relations()
	{
		return array(
			'node'		=> array(self::BELONGS_TO, 'Node', 'id_node'),
			'user'		=> array(self::BELONGS_TO, 'User', 'id_user'),
		);
	}

	public function tableName() { return 'permission'; }
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
*/
}
