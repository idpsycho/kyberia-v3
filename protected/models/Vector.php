<?php

/**
 * This is the model class for table "node_vector".
 *
 * The followings are the available columns in table 'node_vector':
 * @property integer $node_vector_id
 * @property integer $node_id
 * @property integer $node_parent
 * @property string $node_vector
 * @property integer $level3
 * @property integer $is_hl
 */
class Vector extends CActiveRecord
{



	function beforeFind() { Performance::tick(__METHOD__); return true; }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'node_vector';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('node_id, node_vector', 'required'),
			array('node_id, node_parent, level3, is_hl', 'numerical', 'integerOnly'=>true),
			array('node_vector', 'length', 'max'=>400),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('node_vector_id, node_id, node_parent, node_vector, level3, is_hl', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'node'	=> array(self::BELONGS_TO, 'Node', 'node_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'node_vector_id' => 'Node Vector',
			'node_id' => 'Node',
			'node_parent' => 'Node Parent',
			'node_vector' => 'Node Vector',
			'level3' => 'Level3',
			'is_hl' => 'Is Hl',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('node_vector_id',$this->node_vector_id);
		$criteria->compare('node_id',$this->node_id);
		$criteria->compare('node_parent',$this->node_parent);
		$criteria->compare('node_vector',$this->node_vector,true);
		$criteria->compare('level3',$this->level3);
		$criteria->compare('is_hl',$this->is_hl);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NodeVector the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
