<?php

/**
 * This is the model class for table "mail".
 *
 * The followings are the available columns in table 'mail':
 * @property integer $mail_id
 * @property integer $mail_user
 * @property string $mail_conversation_id
 * @property integer $mail_from
 * @property integer $mail_to
 * @property string $mail_text
 * @property string $mail_timestamp
 * @property string $mail_read
 * @property integer $mail_duplicate_id
 */
class Mail extends ActiveRecord
{
	/////////////////////////////
	// compatibility layer
	function getId()				{ return $this->mail_id; }
	function getId_user()			{ return $this->mail_user; }
	function getId_from()			{ return $this->mail_from; }
	function getId_to()				{ return $this->mail_to; }
	function getHas_read()			{ return $this->mail_read; }
	function getCreated()			{ return $this->mail_timestamp; }
	function getMessage()			{ return $this->mail_text; }
	function getId_orig_mail()		{ return $this->mail_duplicate_id; }

	function getAttributes_MAP() { return Mail::$attributes_MAP; }
	static $attributes_MAP = array(
		'id'				=> 'mail_id',
		'id_user'			=> 'mail_user',
		'id_conversation'	=> 'mail_conversation_id',
		'id_from'			=> 'mail_from',
		'id_to'				=> 'mail_to',
		'message'			=> 'mail_text',
		'created'			=> 'mail_timestamp',
		'has_read'			=> 'mail_read',
		'id_orig_mail'		=> 'mail_duplicate_id',
	);
	//////////////////////////////////


	function beforeFind() { Performance::tick(__METHOD__); return true; }







	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mail_user, mail_from, mail_to, mail_duplicate_id', 'numerical', 'integerOnly'=>true),
			array('mail_conversation_id', 'length', 'max'=>10),
			array('mail_read', 'length', 'max'=>3),
			array('mail_text, mail_timestamp', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('mail_id, mail_user, mail_conversation_id, mail_from, mail_to, mail_text, mail_timestamp, mail_read, mail_duplicate_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		$ID_FROM = 'mail_from';
		$ID_TO = 'mail_to';
		return array(
			'from'	=> array(self::BELONGS_TO, 'User', $ID_FROM),
			'to'	=> array(self::BELONGS_TO, 'User', $ID_TO),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mail_id' => 'Mail',
			'mail_user' => 'Mail User',
			'mail_conversation_id' => 'Mail Conversation',
			'mail_from' => 'Mail From',
			'mail_to' => 'Mail To',
			'mail_text' => 'Mail Text',
			'mail_timestamp' => 'Mail Timestamp',
			'mail_read' => 'Mail Read',
			'mail_duplicate_id' => 'Mail Duplicate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mail_id',$this->mail_id);
		$criteria->compare('mail_user',$this->mail_user);
		$criteria->compare('mail_conversation_id',$this->mail_conversation_id,true);
		$criteria->compare('mail_from',$this->mail_from);
		$criteria->compare('mail_to',$this->mail_to);
		$criteria->compare('mail_text',$this->mail_text,true);
		$criteria->compare('mail_timestamp',$this->mail_timestamp,true);
		$criteria->compare('mail_read',$this->mail_read,true);
		$criteria->compare('mail_duplicate_id',$this->mail_duplicate_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
