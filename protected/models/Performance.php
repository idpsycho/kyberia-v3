<?php

/**
 * This is the model class for table "performance".
 *
 * The followings are the available columns in table 'performance':
 * @property integer $id
 * @property integer $ms
 * @property string $name
 * @property integer $id_user
 * @property string $created
 * @property string $callstack
 */
class Performance extends CActiveRecord
{
	static $DEBUG = 0;//YII_DEBUG;	// turn on here
	static $timers = array();
	static $toSave = array();
	static $counters = array();
	static $newTicks = array();
	static $tStart = 0;

	static function startedRequest()
	{
		if (!Performance::$DEBUG) return;

		if (!Performance::$tStart)
			Performance::$tStart = microtime(1);
	}

	static function start($name)
	{
		if (!Performance::$DEBUG) return;

		Performance::startedRequest();
		if (Performance::$timers[$name]) die("two timers run at once: $name");

		Performance::$timers[$name] = microtime(1);
	}
	static function end($name)
	{
		if (!Performance::$DEBUG) return;

		if (!Performance::$timers[$name]) die("ending unknown timer: $name");

		$ms = (microtime(1) - Performance::$timers[$name])*1000;
		$ms = intval($ms);
		Performance::$timers[$name] = null;
		Performance::add($name, $ms);
	}
	static function tick($name)
	{
		if (!Performance::$DEBUG) return;

		Performance::add('.'.$name, 1);
		/*
		if (!Performance::$counters[$name])
			Performance::$counters[$name] = 0;
		Performance::$counters[$name]++;
		*/
	}
	static function add($name, $ms)
	{
		if (!Performance::$DEBUG) return;

		Performance::startedRequest();

		$item = array(
			'name'=>$name,
			'ms'=>$ms,
			'id_user'=>user()->id,
			'created'=>NOW(),
			'callstack'=>Performance::_callstack(),
			'dt'=>intval((microtime(1) - Performance::$tStart)*1000),
		);
		Performance::$toSave[] = $item;

		if ($name[0]=='.')
			Performance::$newTicks[] = $item;
	}

	static function getNewTicks()
	{
		if (!Performance::$DEBUG) return;

		$new = Performance::$newTicks;
		Performance::$newTicks = array();
		return $new;
	}

	static function htmlKeypoint()
	{
		if (!Performance::$DEBUG) return;

		$newTicks = Performance::getNewTicks();
		$nTicks = count($newTicks);
		if (!$nTicks)
			return;

		$title = '';
		foreach ($newTicks as $tick)
			$title .= $tick['name']." at ".$tick['dt']." ms \n".$tick['callstack']."\n";

		return "<br style='display: none;' class='render-keypoint' data-ticks='$nTicks' data-title='$title'>\n";
				//"data-ticks='$newTicks' data-file='$viewFile' data-class='$class'>\n";
	}

	static function finishedRequest($controller)
	{
		if (!Performance::$DEBUG) return;

		foreach (Performance::$counters as $name=>$ticks)
			Performance::$toSave[] = array('name'=>'.'.$name, 'ms'=>$ticks, 'created'=>NOW());

		foreach (Performance::$toSave as $item)
		{
			$p = new Performance;
			$p->attributes = $item;
			$p->name = substr($p->name, 0, 30);
			$p->save(false);
		}

		$controller->renderPartial('/admin/_performance', array('user'=>user()));
	}

	static function _callstack()
	{
		$callstack = '';
		$reverse_trace = array_reverse(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
		//error($reverse_trace);
		foreach ($reverse_trace as $call)
		{
			if (strpos($call['file'], 'CComponent.php') !== false)
			{
				$callstack .= ">>>>\t".$call['class'].':'.$call['function']."\n";
				continue;
			}

			if (strpos($call['file'], '\\framework\\') !== false) continue;
			if (strpos($call['file'], 'Performance.php') !== false) continue;
			if (strpos($call['file'], 'Controller.php') !== false) continue;
			if (strpos($call['file'], 'index.php') !== false) continue;
			if (strpos($call['file'], 'ESmartyViewRenderer.php') !== false) continue;
			if (strpos($call['file'], 'smarty_internal_templatebase.php') !== false) continue;

			$base = basename($call['file']);
			if (endsWith($base, '.tpl.php')) $base = substr($base, 40+strlen('.file.'));	// remove smarty cache hash

			$basedir = basename( dirname($call['file']) );
			$s = $basedir.'/'.$base;

			$s = $base;
			//$s = $call['file'];

			if (!$s) continue;
			$callstack .= "\t".$s.':'.$call['line']."\n";
		}
		return $callstack;
	}

	function getIsTickCall() { return $this->name[0] == '.'; }


	////////////////////////////////
	// gettery, kedze neviem ako volat static veci zo smarty

	public $sumMs;
	public $count;
	public $callstacks;
	function getSlowest($min=10, $notTpl=false)
	{
		$arr = Performance::model()->findAll(array(
			'select'	=> 'sum(ms) as sumMs, count(1) as count, name, group_concat(callstack SEPARATOR "\n\n") as callstacks',
			'condition' => 'created > NOW() - :min*60 AND (:notTpl OR name NOT LIKE "tpl%")',
			'params'	=> array(':notTpl'=>!$notTpl, ':min'=>$min),
			'order'		=> 'sumMs DESC',
			'group'		=> 'name',
			'limit'		=> 20,
		));
		usort($arr, function($a, $b) { return $b->sumMs/$b->count - $a->sumMs/$a->count; });
		return $arr;
	}
	function getNow() { return time(); }
	function getLastMinutes($min=10, $notTpl=false)
	{
		return Performance::model()->findAll(array(
			'condition' => 'created > NOW() - :m*60 AND ms > 1 AND (:notTpl OR name NOT LIKE "tpl%")',
			'params'	=> array(':m'=>$min, 'notTpl'=>!$notTpl),
			'order'		=> 'id desc',
			'limit'		=> 2000,
		));
	}



	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'performance';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ms, name, id_user, created', 'required'),
			array('ms, id_user', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
			array('callstack', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ms, name, id_user, created, callstack', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ms' => 'Ms',
			'name' => 'Name',
			'id_user' => 'Id User',
			'created' => 'Created',
			'callstack' => 'Callstack',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ms',$this->ms);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('callstack',$this->callstack,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Performance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
