<?php

class UserManager
{
	static function byName($name)
	{
		return User::model()->find('login=:n', array(':n'=>$name));
	}
	static function byId($id)
	{
		return User::model()->findByPk($id);
	}

	static function ubik()
	{
		return UserManager::byId(332);
	}

	static function idOf($id_or_name, $default=0)
	{
		$u = UserManager::find($id_or_name, $default);
		return $u ? $u->id : 0;
	}

	static function find($id_or_name, $default=0)
	{
		if (!$id_or_name)
			return $default;

		if (is_numeric($id_or_name))
			return UserManager::byId($id_or_name);
		else
			return UserManager::byName($id_or_name);
	}

/*

	function getLink()		{ return cu('site/id', array('id_node'=>$this->id)); }






	static function add($attrs)
	{
		$user = new User();
		$user->attributes = $attrs;

		if (!$user->save())
			return showError( $user->getErrors() );

		return $user;
	}

	static function avatarOfId($id_user)
	{
		if ($id_user)
			return "/uploads/node/$id_user.gif";
		else
			return "/uploads/node/0.gif";
	}


	function getAvatar() { return User::avatarOfId($this->id); }

	function smarty($tpl)	{ return smartyFetch($tpl, array('u'=>$this)); }
	function tpl($prop)		{ return $this->smarty("user/_$prop.tpl"); }

	function getGuest() { return !$this->id; }
	function getIsGuest() { return !$this->id; }

	function permissionIn($node)	// todo: optimize
	{
		$perm = Permission::model()->find('id_user = :u AND id_node = :n',
									array(':u'=>$this->id, ':n'=>$node->id));
		return $perm->permission;
	}

	// rights: OWNER, EDIT, MODERATE, WRITE, READ, none

	// function rightsIn($node)
	// {
	// 	if (Permission::isOwner($node, $this))	return 'owner';
	// 	if (Permission::toEdit($node, $this))	return 'edit';
	// 	if (Permission::toWrite($node, $this))	return 'write';
	// 	if (Permission::toRead($node, $this))	return 'read';
	// 	return '';
	// }

	// slow functions are recursive, so not very fast..
	function canWrite_slow($node){ return Permission::toWrite_recursive($node, $this); }
	function canModerate_slow($node){ return Permission::toModerate_recursive($node, $this); }
	function canRead($node)		{ return Permission::toRead($node, $this); }
	function canWrite($node)	{ return Permission::toWrite($node, $this); }
	function canEdit($node)		{ return Permission::toEdit($node, $this); }
	function isOwner($node)		{ return Permission::isOwner($node, $this); }


	///////////////////////////////////////
	// todo rewrite this to UserAccount, and extend from it
	protected $account_ = null;
	public function getAccount() { return $this->account_ ? : $this->account_ = new UserAccount($this); }
	function beforeSave() { return $this->getAccount()->beforeSave() && parent::beforeSave(); }
	function afterFind() { return $this->getAccount()->afterFind(); }


	////////////////////////////////////////////////////////////
	// gii generated stuff below - only add to new columns to rules + relations
	public function rules()
	{
		return array(
			array('id', 'required'),
			array('id, logged_in, id_location', 'numerical', 'integerOnly'=>true),
			array('login, password, email', 'length', 'max'=>50),
			array('rights', 'length', 'max'=>20),
			array('last_activity', 'safe'),
		);
	}
	public function relations()
	{
		return array(
			'location'	=> array(self::BELONGS_TO, 'Node', 'id_location'),
			'template'	=> array(self::BELONGS_TO, 'Node', 'id_location_template'),
		);
	}
	public function tableName() { return 'user'; }
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
*/
}
