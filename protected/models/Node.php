<?php

/**
 * This is the model class for table "node".
 *
 * The followings are the available columns in table 'node':
 * @property integer $node_id
 * @property string $node_name
 * @property integer $node_parent
 * @property integer $node_type
 * @property string $node_external_access
 * @property string $node_system_access
 * @property integer $node_children_count
 * @property integer $node_creator
 * @property integer $parent_node_creator
 * @property string $node_created
 * @property string $lastchild_created
 * @property integer $k
 * @property integer $node_views
 * @property integer $node_descendant_count
 * @property string $lastdescendant_created
 * @property integer $template_id
 * @property string $node_updated
 * @property string $external_link
 * @property string $node_vector
 * @property string $node_content
 * @property integer $node_level3
 * @property integer $nl2br
 */
class Node extends ActiveRecord
{
	/////////////////////////////
	// compatibility layer
	function getId()				{ return $this->node_id; }
	function getName()				{ return $this->node_name; }
	function getId_parent()			{ return $this->node_parent; }
	function setId_parent($x)		{ $this->node_parent = $x; }
	function getId_parent_user()	{ return $this->parent_node_creator; }
	function setId_parent_user($x)	{ $this->parent_node_creator = $x; }
	function getId_user()			{ return $this->node_creator; }
	function getId_template()		{ return $this->template_id; }
	function getCreated()			{ return $this->node_created; }
	function getUpdated()			{ return $this->node_updated; }
	function setUpdated($x)			{ $this->node_updated = $x; }
	function getViews()				{ return $this->node_views; }
	function setViews($x)			{ $this->node_views = $x; }
	function getContent()			{ return $this->node_content; }
	function setContent($x)			{ $this->node_content = $x; }
	function getVector()			{ return $this->node_vector; }
	function setVector($x)			{ $this->node_vector = $x; }
	function getAccess()			{ return $this->node_system_access; }
	function setAccess($x)			{ $this->node_system_access = $x; }
	function getNet_access()		{ return $this->node_external_access; }
	function setNet_access($x)		{ $this->node_external_access = $x; }
	function getChildren_count()	{ return $this->node_children_count; }
	function setChildren_count($x)	{ $this->node_children_count = $x; }

	function getAttributes_MAP() { return Node::$attributes_MAP; }
	static $attributes_MAP = array(
		'id'				=> 'node_id',
		'name'				=> 'node_name',
		'id_parent'			=> 'node_parent',
		'type'				=> 'node_type',
		'net_access'		=> 'node_external_access',
		'access'			=> 'node_system_access',
		'children_count'	=> 'node_children_count',
		'id_user'			=> 'node_creator',
		'id_parent_user'	=> 'parent_node_creator',
		'created'			=> 'node_created',
		'lastchild_created'	=> 'lastchild_created',
		'k'					=> 'k',
		'views'				=> 'node_views',
		'descendant_count'	=> 'node_descendant_count',
		'lastdescendant_created'=> 'lastdescendant_created',
		'id_template'		=> 'template_id',
		'updated'			=> 'node_updated',
		'external_link'		=> 'external_link',
		'vector'			=> 'node_vector',
		'content'			=> 'node_content',
		'level3'			=> 'node_level3',
		'nl2br'				=> 'nl2br',
	);
	//////////////////////////////////


	function beforeFind() { Performance::tick(__METHOD__); return true; }

	////////////////////////////////////
	// getters for templates
	function getNewChildName()	{ return NodeManager::newChildName($this); }
	function getIsBooked()		{ return false; }
	function getIsK()			{ return false; }
	function getIsFooked()		{ return false; }

	function getUserAvatar()	{ return "/kyb-data/images/nodes/{$this->id_user}.gif"; }
	function getLink()			{ return '/id/'.$this->id; }
	function getAsUser()		{ return UserManager::byId($this->id); }
	///////////////////////////////////////



	// this wouldnt be safe..
	//function nodeByName($name)	{ return NodeManager::byName($name); }
	function getMain()	{ return NodeManager::byId(1); }

	function tpl($prop, $params=null)
	{
		if (!is_array($params)) $params = array();
		$params['node'] = $this;
		$params['user'] = user();
		Yii::app()->controller->renderPartial("/node/_$prop", $params);
	}

	/*protected function _exceptPrivate($forceNoPrivate=false) {
		$showPrivate = (!$forceNoPrivate && $this->access == 'private');
		return $showPrivate ? 'TRUE' : 'node_system_access != "private"';
	}*/
	function getChildrenFlat() {
		$ID = 'node_id';
		$VECTOR = 'node_vector';

		$crit = array(
			'condition'=>"$VECTOR like :v AND $ID != :id",
			'params'=>array(':v'=>$this->vector.'%', ':id'=>$this->id),
			'order'=>"$ID desc",
			'limit'=>100,
		);

		Node::get_FILTER($crit);
		Performance::start(__METHOD__);
		$nodes = Node::model()->with('user')->findAll($crit);
		Performance::end(__METHOD__);

		return $nodes;
	}
	function getChildrenTree() {
		$ID_NODE = 'node_id';
		$VECTOR = 'node_vector';

		$crit = array(
			'condition'=>"$VECTOR like :v AND $ID_NODE != :id",
			'params'=>array(':v'=>$this->vector.'%', ':id'=>$this->id),
			'order'=>"concat($VECTOR, 'z') desc",	// "z" zaruci ze parent bude vyssie nez childy
			'limit'=>100,
		);

		Node::get_FILTER($crit);
		Performance::start(__METHOD__);
		$nodes = Node::model()->with('user')->findAll($crit);
		Performance::end(__METHOD__);
		return $nodes;
	}
	function getChildrenDirect() {
		$ID = 'node_id';
		$UID = 'user_id';
		$ID_PARENT = 'node_parent';
		$NAME = 'node_name';

		$crit = array(
			'condition'=>"t.$ID_PARENT = :p AND t.$ID != :id",
			'params'=>array(':p'=>$this->id, ':id'=>$this->id),
			'limit'=>100,
			'order'=>"t.$ID desc",
		);
		$with = array('user', 'template');

		Node::get_FILTER($crit);
		Performance::start(__METHOD__);
		$nodes = Node::model()->with($with)->findAll($crit);
		Performance::end(__METHOD__);
		return $nodes;
	}
	function getKNEW() {
		$hours = 24;
		$CREATED = 'node_created';
		Performance::start(__METHOD__);
		$nodes = Node::model()->with('user')->findAll(array(
			'condition'=>"$CREATED > (NOW() - INTERVAL :h HOUR) AND k>0",
			'params'=>array(':h'=>$hours),
			'order'=> "k desc",
			'limit'=>100,
		));
		Performance::end(__METHOD__);
		return $nodes;
	}
	function getBookedUsers() {
		$hours = 24;
		$BOOKMARKED = 'node_bookmark';
		$ID_NODE = 'node_id';
		Performance::start(__METHOD__);
		$booked = NodeAccess::model()->with('user')->findAll(array(
			'condition'=>"$BOOKMARKED = 'yes' AND t.$ID_NODE = :n",
			'params'=>array(':n'=>$this->id),
			'order'=> "last_visit desc",
			'limit'=>100,
		));
		Performance::end(__METHOD__);
		return $booked;
	}
	function getAncestors()
	{
		Performance::start(__METHOD__);
		$arr = array();
		$node = $this;
		for ($i=0; $i < 20; $i++)
		{
			$node = $node->parent;
			if (!$node) break;
			$arr[] = $node;
		}
		Performance::end(__METHOD__);
		return $arr;
	}
	function getCons() {
		$hours = 1;
		$CREATED = 'cas';
		$ID_NODE = 'node_id';
		Performance::start(__METHOD__);
		$givenk = GivenK::model()->with('node')->findAll(array(
			'condition'=>"$CREATED > NOW() - INTERVAL :h HOUR",
			'params'=>array(':h'=>$hours),
			'group'=>"node.$ID_NODE",
			'order'=>'k desc',
			'limit'=>100,
		));
		$nodes = array();
		foreach ($givenk as $g)
			if ($g->node)
				$nodes[] = $g->node;
		Performance::end(__METHOD__);

		return $nodes;
	}

	function getTopK() {
		$ID = 'node_id';
		$VECTOR = 'node_vector';

		$crit = array(
			'condition'=>"$VECTOR like :v AND $ID != :id AND k > 0",
			'params'=>array(':v'=>$this->vector.'%', ':id'=>$this->id),
			'order'=>'k desc',
			'limit'=>100,
		);

		Node::get_FILTER($crit);
		Performance::start(__METHOD__);
		$nodes = Node::model()->with('user')->findAll($crit);
		Performance::end(__METHOD__);
		return $nodes;
	}


	function getVisits() {
		$order = $_GET['order'];
		if (!in_array($order, array('visits', 'last', 'unread_children')))
			$order = 'last';

		if ($order == 'last') $order = 'last_visit';
		if ($order == 'unread_children') $order = 'node_user_subchild_count';

		$ID_NODE = 'node_id';
		Performance::start(__METHOD__);
		$visits = NodeAccess::model()->with('user')->findAll(array(
			'condition'=>"$ID_NODE = :n",
			'params'=>array(':n'=>$this->id),
			'order'=> "$order desc",
			'limit'=>100,
		));
		Performance::end(__METHOD__);
		return $visits;
	}



	/////////////////////////////////////////

	function getSearchResults() {
		if ($_GET['user'])
		{
			Performance::start(__METHOD__.'-user');
			$users = User::model()->with('node')->findAll(array(
				'condition'=>"login LIKE :u",
				'params'=>array(':u'=>'%'.$_GET['user'].'%'),
				'limit'=>100,
			));
			$nodes = array();
			foreach ($users as $u)
				$nodes[] = $u->node;
			Performance::end(__METHOD__.'-user');
			return $nodes;
		}
		if ($_GET['search'])
		{
			$NAME = 'node_name';
			Performance::start(__METHOD__.'-search');
			$nodes = Node::model()->findAll(array(
				'condition'=>"$NAME LIKE :q",
				'params'=>array(':q'=>'%'.$_GET['search'].'%'),
				'limit'=>100,
			));
			Performance::end(__METHOD__.'-search');
			return $nodes;
		}
		return array();
	}

	// najprv len taketo hacky riesenie
	static function get_FILTER(&$crit) {
		$ID_USER = 'node_creator';
		$CONTENT = 'node_content';

		if ($_GET['user']) {
			$id_user = UserManager::idOf( $_GET['user'] );
			$crit['condition'] = "(".$crit['condition'].") AND ($ID_USER = :filter)";
			$crit['params'][':filter'] = $id_user;
		}

		if ($_GET['search']) {
			$crit['condition'] = "(".$crit['condition'].") AND ($CONTENT LIKE :filter)";
			$crit['params'][':filter'] = '%'.$_GET['search'].'%';
		}

		// if ($_GET['k-by']) {
		// 	$crit['condition'] = "(".$crit['condition'].") AND ($MESSAGE LIKE :filter)";
		// 	$crit['params'][':filter'] = '%'.$_GET['search'].'%';
		// }
	}

	function getUsersByPermission($permission) {
		$ID_NODE = 'node_id';
		$PERMISSION = 'node_permission';
		$arr = NodeAccess::model()->with('user')->findAll(
							"$ID_NODE = :n AND $PERMISSION = :p",
							array(':n'=>$this->id, ':p'=>$permission));
		$users = array();
		foreach ($arr as $i=>$a)
			$users[] = $a->user;
		return $users;
	}
	function getNamesByPermission($permission) {
		Performance::start(__METHOD__.'-'.$permission);
		$users = $this->getUsersByPermission($permission);
		$names = '';
		foreach ($users as $i=>$u)
			$names .= ($i?';':'').$u->login;
		Performance::end(__METHOD__.'-'.$permission);
		return $names;
	}
	function getNamesMasters()	{ return $this->getNamesByPermission('master'); }
	function getNamesSilenced()	{ return $this->getNamesByPermission('silence'); }
	function getNamesAccessors(){ return $this->getNamesByPermission('access'); }

	function getMasters()	{ return $this->getUsersByPermission('master'); }
	function getSilenced()	{ return $this->getUsersByPermission('silence'); }
	function getAccessors()	{ return $this->getUsersByPermission('access'); }



	function getTiamat($rev) {
		return Tiamat::getByRevision($this->id, $rev);
	}
	function getTiamats() {
		$ID_NODE = 'node_id';
		$ID_DESTRUCTOR = 'node_destructor';
		Performance::start(__METHOD__);
		$tiamats = Tiamat::model()->findAll(array(
			'select'	=> "$ID_NODE, $ID_DESTRUCTOR, update_performed",
			'condition'	=> "$ID_NODE = :n",
			'params'	=> array(':n'=>$this->id),
			'order'		=> 'update_performed DESC',
			'limit'		=> 100,
		));
		Performance::end(__METHOD__);
		return $tiamats;
	}




	////////////////////////////////////////////////

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'nodes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parent_node_creator, lastchild_created', 'required'),
			array('node_parent, node_type, node_children_count, node_creator, parent_node_creator, k, node_views, node_descendant_count, template_id, node_level3, nl2br', 'numerical', 'integerOnly'=>true),
			array('node_name', 'length', 'max'=>132),
			array('node_external_access', 'length', 'max'=>3),
			array('node_system_access', 'length', 'max'=>9),
			array('external_link', 'length', 'max'=>123),
			array('node_vector', 'length', 'max'=>230),
			array('node_created, lastdescendant_created, node_updated, node_content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('node_id, node_name, node_parent, node_type, node_external_access, node_system_access, node_children_count, node_creator, parent_node_creator, node_created, lastchild_created, k, node_views, node_descendant_count, lastdescendant_created, template_id, node_updated, external_link, node_vector, node_content, node_level3, nl2br', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		$ID_USER = 'node_creator';
		$ID_TEMPLATE = 'template_id';
		$ID_PARENT = 'node_parent';
		$ID_PARENT_USER = 'parent_node_creator';
		$ID = 'node_id';
		return array(
			'user'		=> array(self::BELONGS_TO, 'User', $ID_USER),
			'template'	=> array(self::BELONGS_TO, 'Node', $ID_TEMPLATE),
			'parent'	=> array(self::BELONGS_TO, 'Node', $ID_PARENT),
			'parent_user' => array(self::BELONGS_TO, 'User', $ID_PARENT_USER),
			'children'	=> array(self::HAS_MANY, 'Node', $ID_PARENT,
								'order'=>"$ID desc"),
			/*
			'hardlinked'=> array(self::BELONGS_TO, 'Node', 'id_hardlinked'),
			'bookeds'	=> array(self::HAS_MANY, 'Tag', 'id_node',
								'condition'=>'bookeds.tag="book"'),
			*/
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'node_id' => 'Node',
			'node_name' => 'Node Name',
			'node_parent' => 'Node Parent',
			'node_type' => 'Node Type',
			'node_external_access' => 'Node External Access',
			'node_system_access' => 'Node System Access',
			'node_children_count' => 'Node Children Count',
			'node_creator' => 'Node Creator',
			'parent_node_creator' => 'Parent Node Creator',
			'node_created' => 'Node Created',
			'lastchild_created' => 'Lastchild Created',
			'k' => 'K',
			'node_views' => 'Node Views',
			'node_descendant_count' => 'Node Descendant Count',
			'lastdescendant_created' => 'Lastdescendant Created',
			'template_id' => 'Template',
			'node_updated' => 'Node Updated',
			'external_link' => 'External Link',
			'node_vector' => 'Node Vector',
			'node_content' => 'Node Content',
			'node_level3' => 'Node Level3',
			'nl2br' => 'Nl2br',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('node_id',$this->node_id);
		$criteria->compare('node_name',$this->node_name,true);
		$criteria->compare('node_parent',$this->node_parent);
		$criteria->compare('node_type',$this->node_type);
		$criteria->compare('node_external_access',$this->node_external_access,true);
		$criteria->compare('node_system_access',$this->node_system_access,true);
		$criteria->compare('node_children_count',$this->node_children_count);
		$criteria->compare('node_creator',$this->node_creator);
		$criteria->compare('parent_node_creator',$this->parent_node_creator);
		$criteria->compare('node_created',$this->node_created,true);
		$criteria->compare('lastchild_created',$this->lastchild_created,true);
		$criteria->compare('k',$this->k);
		$criteria->compare('node_views',$this->node_views);
		$criteria->compare('node_descendant_count',$this->node_descendant_count);
		$criteria->compare('lastdescendant_created',$this->lastdescendant_created,true);
		$criteria->compare('template_id',$this->template_id);
		$criteria->compare('node_updated',$this->node_updated,true);
		$criteria->compare('external_link',$this->external_link,true);
		$criteria->compare('node_vector',$this->node_vector,true);
		$criteria->compare('node_content',$this->node_content,true);
		$criteria->compare('node_level3',$this->node_level3);
		$criteria->compare('nl2br',$this->nl2br);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Node the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
