<?php

return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'components' => array(
            'db' => array(
                'connectionString' => 'mysql:unix_socket=/tmp/mysql51.sock;dbname=database',
                'username' => 'username',
                'password' => 'password',
            ),
        ),
    )
);
