<?php

return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'modules'=>array(
            'gii' => array(
                'class' => 'system.gii.GiiModule',
                'password' => 'giipassword',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters' => array('127.0.0.1', '::1'),
                'generatorPaths' => array(
                    'application.gii',
                ),
            ),
        ),
        'components' => array(
            'db' => array(
                'connectionString' => 'mysql:host=localhost;dbname=database',
                'username' => 'username',
                'password' => 'password',
            ),
        ),
    )
);
