<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

return array(
    'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name' => 'Kyberia',
    'sourceLanguage' => 'en',
    'language' => 'sk',
    'timeZone' => 'Europe/Bratislava',

    // preloading 'log' component
    'preload' => array('log'),

    // autoloading model and component classes
    'import' => array(
        'application.components.*',
        'application.controllers.*',
        'application.models.*',
        //!TODO revisit
        'application.k_events.*',
        'application.k_events.node.*',
        'application.k_events.user.*',
        'application.k_events.tag.*',
    ),

    'modules' => array(
    ),

    // application components
    'components' => array(
        'clientScript' => array(
//            'coreScriptPosition' => CClientScript::POS_END,
        ),
        'db' => array(
            'emulatePrepare' => TRUE,
            'charset' => 'utf8',
            'schemaCachingDuration' => YII_DEBUG ? 5 : 3600,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'format' => array(
            'class' => 'CLocalizedFormatter',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
        'request' => array(
            'enableCookieValidation' => TRUE,
        ),
        'urlManager' => array(
            'class' => 'UrlManager',
            'urlFormat' => 'path',
            'showScriptName' => FALSE,
            'rules' => array(
                ''=>'site/main',
                'id/<id_node:\d+>'=>'site/id',
                //'id/<id_node:\d+>/<id_template:\d+>'=>'site/id',
                'id/<id_node:\d+>/<template:[\w-]+>'=>'site/id',

                'gii'=>'gii',
                'gii/<controller:\w+>'=>'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',

                '<template:[\w-]+>'=>'site/template',

                '<controller:[\w-]+>/<id:\d+>' => '<controller>/view',
                '<controller:[\w-]+>' => '<controller>/index',
                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
            ),
        ),
        'user' => array(
            'class' => 'WebUser',
            'allowAutoLogin' => TRUE,
            'loginUrl'=>array('/login'),
//            'loginUrl' => array('/admin/default/login'),
        ),
        'viewRenderer' => array(
            'class' => 'application.extensions.yiiext.renderers.smarty.ESmartyViewRenderer',
            'smartyDir' => 'application.vendors.smarty',
            'fileExtension' => '.tpl',
            'pluginsDir' => array(
                'application.components.smarty.plugins',
                'application.k_methods',
                'application.k_methods.node',
                'application.k_methods.user',
                'application.k_methods.tag',
                'application.k_methods._functions',
                'application.k_methods._modifiers',
            ),
            //'configDir' => 'application.smartyConfig',
            //'prefilters' => array(array('MyClass','filterMethod')),
            //'postfilters' => array(),
            'config' => array(
                'force_compile' => FALSE,
                //'compile_check' => FALSE,
                //'caching' => TRUE,
                //   ... any Smarty object parameter
            )
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'admin@kyberia.sk',
        'admins'=>array(
        	332		=> true,	// ubik

        	// docasni admini na testovanie (lepsie je ked clovek pouziva stranku ako bezny clovek)
        	2334	=> true,	// freezy
        	1297258	=> true,	// psycho
        ),
        'coders'=>array(
        	2334	=> true,	// freezy
        	1203863	=> true,	// repelent
        	1297258	=> true,	// psycho
        ),
    ),
);