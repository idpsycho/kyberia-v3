-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             7.0.0.4381
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table v3.performance
DROP TABLE IF EXISTS `performance`;
CREATE TABLE IF NOT EXISTS `performance` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ms` int(10) NOT NULL,
  `name` varchar(32) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `ms` (`ms`),
  KEY `created` (`created`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8;

-- Dumping data for table v3.performance: ~12 rows (approximately)
DELETE FROM `performance`;
/*!40000 ALTER TABLE `performance` DISABLE KEYS */;
INSERT INTO `performance` (`id`, `ms`, `name`, `id_user`, `created`) VALUES
	(244, 0, 'TPL: ajax-checkMail', 1297258, '2013-10-07 11:10:11'),
	(245, 7, 'User::getBookedNodes', 1297258, '2013-10-07 11:10:44'),
	(246, 1, 'Node::getBookedUsers', 1297258, '2013-10-07 11:10:44'),
	(247, 2, 'Node::getAncestors', 1297258, '2013-10-07 11:10:44'),
	(248, 0, 'Node::getTiamats', 1297258, '2013-10-07 11:10:44'),
	(249, 293, 'TPL: history', 1297258, '2013-10-07 11:10:44');
/*!40000 ALTER TABLE `performance` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
